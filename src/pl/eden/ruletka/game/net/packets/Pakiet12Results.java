package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o wylosowaniu liczby przez koło ruletki i wygranych graczy
 * @author Mateusz Macięga
 */
public class Pakiet12Results extends Pakiet {
    /** Wylosowana liczba */
    int wylosowanePole;
    /** Tablica hashująca gdzie kluczem jest nazwa gracza a wartością wygrana gracza */
    Map<String,Long[]> zwyciescyGracze;

    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet12Results(byte[] dane) {
        super(12);
        zwyciescyGracze = new HashMap<>();
        String[] wiadomosc = czytajDane(dane).split(",");
        
        wylosowanePole = Integer.parseInt(wiadomosc[0]);
        
        wiadomosc[1] = wiadomosc[1].substring(1);
        if(!wiadomosc[1].equals("")) {
            for(int i = 1; i< wiadomosc.length; i++) {
                String[] zwycieskiGracz = wiadomosc[i].split(";");
                Long[] wyniki = new Long[2];
                wyniki[0] = Long.valueOf(zwycieskiGracz[1]);
                wyniki[1] = Long.valueOf(zwycieskiGracz[2]);

                zwyciescyGracze.put(zwycieskiGracz[0], wyniki);
            }
        }
    }

    /**
     * Konstruktor sparametryzowany
     * @param zwyciescyGracze Tablica hashująca gdzie kluczem jest nazwa gracza a wartością wygrana gracza
     * @param wylosowanePole Wylosowana liczba
     */
    public Pakiet12Results(Map<String,Long[]> zwyciescyGracze, int wylosowanePole) {
        super(12);
        this.zwyciescyGracze = zwyciescyGracze;
        this.wylosowanePole = wylosowanePole;
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o wylosowaniu liczby przez koło ruletki i wygranych graczy
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet12Results.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o wylosowaniu liczby przez koło ruletki i wygranych graczy
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet12Results.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
            String wynik = "";
            
            for (Map.Entry<String, Long[]> pozycja : zwyciescyGracze.entrySet())
            {
                wynik += pozycja.getKey() + ";" + pozycja.getValue()[0] + ";" + pozycja.getValue()[1] + ",";
            }
            
            if(!wynik.equals("")) {
                wynik = wynik.substring(0, wynik.length()-1);
            }
            
            return ("12" + wylosowanePole + ",R" + wynik + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca tablicę hashująca gdzie kluczem jest nazwa gracza a wartością wygrana gracza
     * @return Tablica hashująca gdzie kluczem jest nazwa gracza a wartością wygrana gracza
     */
    public Map<String,Long[]> dajZwyciescyGracze() {
        return zwyciescyGracze;
    }
    
    /**
     * Metoda zwracająca wylosowaną liczbe
     * @return Wylosowana liczba
     */
    public int dajWylosowanePole() {
        return wylosowanePole;
    }
}
