package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne typu wiersz
 * @author Mateusz Macięga
 */
public class PoleSpecjalneWiersz extends PoleSpecjalne {
    /** Typ wyliczeniowy dla pola specjalnego Wiersz */
    public static enum Wiersze { 
        PIERWSZY_WIERSZ, DRUGI_WIERSZ, TRZECI_WIERSZ;
    }
    
    /** Które pole specjalne typu Wiersz */
    private final Wiersze wiersz;
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param wiersz Które pole specjalne typu Wiersz
     */
    public PoleSpecjalneWiersz(String nazwa, Wiersze wiersz) {
        super(nazwa, Pole.MnoznikiPola.COLUMN_BET, PoleSpecjalne.TypyPolaSpecjalnego.WIERSZ);
        this.wiersz = wiersz;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        int[] wartosciWWierszu = new int[12];
        int liczba;
        switch(wiersz)
        {
            case PIERWSZY_WIERSZ:  
                liczba = 3;
                for(int indeks = 0; indeks < wartosciWWierszu.length; indeks++)
                {
                    wartosciWWierszu[indeks] = liczba;
                    
                    if (wartosciWWierszu[indeks] == wylosowanePole)
                    {
                        return true;
                    }
                    
                    liczba = liczba + 3;
                }
                return false;  
            case DRUGI_WIERSZ:
                liczba = 2;
                for(int indeks = 0; indeks < wartosciWWierszu.length; indeks++)
                {
                    wartosciWWierszu[indeks] = liczba;
                    
                    if (wartosciWWierszu[indeks] == wylosowanePole)
                    {
                        return true;
                    }
                    
                    liczba = liczba + 3;
                }
                return false;                
            case TRZECI_WIERSZ:
                liczba = 1;
                for(int indeks = 0; indeks < wartosciWWierszu.length; indeks++)
                {
                    wartosciWWierszu[indeks] = liczba;
                    
                    if (wartosciWWierszu[indeks] == wylosowanePole)
                    {
                        return true;
                    }
                    
                    liczba = liczba + 3;
                }
                return false;  
            default:
                return false;
        }
    }
    
    /**
     * Metoda zwracająca typ wyliczeniowy dla pola specjalnego Wiersz
     * @return Typ wyliczeniowy dla pola specjalnego Wiersz
     */
    public Wiersze dajWiersz() {
        return wiersz;
    }
    
}
