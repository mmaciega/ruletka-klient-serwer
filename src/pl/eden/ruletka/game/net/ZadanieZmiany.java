package pl.eden.ruletka.game.net;

import java.nio.channels.SocketChannel;

/**
 * Klasa reprezentująca żadanie zmiany ustawień klucza selectora 
 * @author Mateusz Macięga
 */
public class ZadanieZmiany {
	public static final int REGISTER = 1;
	public static final int CHANGEOPS = 2;
	
	private final SocketChannel socket;
	private final int type;
	private final int ops;
	
        /**
         * Konstruktor sprametryzowany
         * @param socket 
         * @param type 
         * @param ops 
         */
	public ZadanieZmiany(SocketChannel socket, int type, int ops) {
		this.socket = socket;
		this.type = type;
		this.ops = ops;
	}

    /**
     * Metoda zwracająca gniazdo sieciowej
     * @return Gniazdo sieciowe
     */
    public SocketChannel dajSocket() {
        return socket;
    }

    /**
     * Metoda zwracająca typ gniazda
     * @return Typ gniazda
     */
    public int dajType() {
        return type;
    }

    /**
     * @return the ops
     */
    public int dajOps() {
        return ops;
    }
        
        
}
