package pl.eden.ruletka.game.logic;

import pl.eden.ruletka.game.gfx.fields.Pole;

/**
 * Klasa reprezentująca zakład stawiony przez gracza
 * @author Mateusz Macięga
 */
public class Zaklad {
    /** Instacja klasy pole, na które został postawiony zakład */
    private final Pole pole;
    /** Wartość zakładu */
    private int wartoscZakladu;
    
    /**
     * Konstruktor spramatryzowany 
     * @param pole Instacja klasy pole, na które został postawiony zakład
     * @param wartoscZakladu Wartość zakładu
     */
    public Zaklad(Pole pole, int wartoscZakladu) {
        this.pole = pole;
        this.wartoscZakladu = wartoscZakladu;
    }

    /**
     * Metoda zwracająca instancję klasy pole, na które został postawiony zakład
     * @return Instacja klasy pole, na które został postawiony zakład
     */
    public Pole dajPole() {
        return pole;
    }

    /**
     * Metoda zwracająca wartość zakładu
     * @return Wartość zakładu
     */
    public int dajWartoscZakladu() {
        return wartoscZakladu;
    }
    
    /**
     * Metoda sprawdzająca czy zakład jest zwycięski
     * @param wylosowanePole Wylosowana liczba
     * @return Informacja o tym czy zakład jest zwycięski
     */
    public boolean sprawdzZaklad(int wylosowanePole) {
        return dajPole().sprawdzZaklad(wylosowanePole);
    }
    
    /**
     * Metoda dodająca zwiększają wartość zakładu na pole
     * @param wartoscZakladu Wartość kolejnego zakładu na to samo pole
     */
    public void dodajWartoscZakladu(int wartoscZakladu) {
        this.wartoscZakladu += wartoscZakladu;
    }
}
