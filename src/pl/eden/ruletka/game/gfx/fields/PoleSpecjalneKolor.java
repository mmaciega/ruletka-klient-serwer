package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne typu kolor
 * @author Mateusz Macięga
 */
public class PoleSpecjalneKolor extends PoleSpecjalne {
    /** Typ wyliczeniowy dla pola specjalnego Red, Black */
    public static enum Kolory { 
        CZARNY, CZERWONY;
    }

    /** Które pole specjalne typu Kolor */
    private final Kolory kolor;

    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param kolor Kolor pola specjalnego typu Kolor
     */
    public PoleSpecjalneKolor(String nazwa, Kolory kolor) {
        super(nazwa, Pole.MnoznikiPola.RED_BLACK_BET, PoleSpecjalne.TypyPolaSpecjalnego.KOLOR);
        this.kolor = kolor;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        PoleNumeryczne.KoloryPol typ =  PoleNumeryczne.znajdzKolorPola(wylosowanePole);
        
        switch(kolor)
        {
            case CZARNY:  
               switch(typ) {
                   case CZARNE:
                       return true;
                   case CZERWONE:
                       return false;
                   default:
                       break;
               }                                
            case CZERWONY:
               switch(typ) {
                   case CZARNE:
                       return false;
                   case CZERWONE:
                       return true;
                   default:
                       break;
               } 
            default:
                return false;
        }
    }
    
    /**
     * Metoda zwracająca kolor typu specjalnego
     * @return Kolor typu specjalnego
     */
    public Kolory dajKolorPola() {
        return kolor;
    }
    
}
