package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne typu połówka
 * @author Mateusz Macięga
 */
public class PoleSpecjalnePolowka extends PoleSpecjalne {
    /** Typ wyliczeniowy dla pola specjalnego Połówka */
    public static enum Polowki { 
        PIERWSZA_POLOWKA, DRUGA_POLOWKA;
    }
    
    /** Które pole specjalne typu Połówka */
    private final Polowki polowka;
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param polowka Które pole specjalne typu Połówka
     */
    public PoleSpecjalnePolowka(String nazwa, Polowki polowka) {
        super(nazwa, Pole.MnoznikiPola.FIRST_SECOND_18_BET, PoleSpecjalne.TypyPolaSpecjalnego.POLOWKA);
        this.polowka = polowka;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        switch(polowka)
        {
            case PIERWSZA_POLOWKA:
                return wylosowanePole >= 0 && wylosowanePole <=18;
            case DRUGA_POLOWKA:
                return wylosowanePole >18 && wylosowanePole <37;
            default:
                return false;
        }
    }
    
    /**
     * Metoda zwracająca typ wyliczeniowy dla pola specjalnego Połówka
     * @return Typ wyliczeniowy dla pola specjalnego Połówka
     */
    public Polowki dajPolowke() {
        return polowka;
    }
}
