package pl.eden.ruletka.game.gfx.fields;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import pl.eden.ruletka.game.gfx.ElementGraficzny;

/**
 * Klasa reprezentująca pole w grze
 * @author Mateusz Macięga
 */
public abstract class Pole extends ElementGraficzny{
    /** Typ wyliczeniowy zawierający dostępne mnożniki pól  */
    public static enum MnoznikiPola {
        BLAD(-1), FIRST_SECOND_18_BET(1), COLUMN_BET(2), CORNER_BET(8), DOZEN_BET(2), EVEN_ODD_BET(1), FIVE_BET(6), RED_BLACK_BET(1), SIX_LINE_BET(5),
        SPLIT_BET(17), STRAIGHT_UP_BET(35), STREET_BET(11);

        /** Wartość mnożnika */
        private final int wartosc;

        /**
         * Konstruktor sparametryzowany, prywatny
         * @param wartosc Wartość mnożnika
         */
        private MnoznikiPola(int wartosc) {
            this.wartosc = wartosc;
        }

        /**
         * Metoda zwracająca wartość mnożnika dla danego typus
         * @return Warotść mnożnika
         */
        public int dajWartoscMnoznika() {
            return wartosc;
        }
    }
    
    /** Typ wyliczeniowy zawierający dostępne typy pól */
    public static enum TypyPola {
        POLE_NUMERYCZNE(0), POLE_SPECJALNE(1), POLE_GRANICZNE(2);

        /** Identyfikator typu */
        private final int idTypu;

        /**
         * Konstruktor sparametryzowany, prywatny
         * @param idTypu identyfikator typu
         */
        private TypyPola(int idTypu) {
            this.idTypu = idTypu;
        }

        /**
         * Metoda zwracająca identyfikator typu
         * @return Identyfikator typu
         */
        public int dajIdTypu() {
            return idTypu;
        }
    }
    
    /** Liczba żetonów na polu */
    private int liczbaZetonowNaPolu;
    /** Mnożnik wygranej pola */
    private final MnoznikiPola mnoznikWygranej;
    /** Typ pola */
    private final TypyPola typPola;
    /** Obrazek z postawionymi żetonami */
    private ImageIcon obrazekPolaZZetonami;
        
    /**
     * Konstruktor domyślny
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param mnoznikWygranej Mnożnik wygranej pola
     * @param typPola Typ pola
     */
    public Pole(String nazwa, MnoznikiPola mnoznikWygranej, TypyPola typPola) {
        super(nazwa);
        obrazekPolaZZetonami = dajObrazek();
        liczbaZetonowNaPolu = 0;
        this.mnoznikWygranej = mnoznikWygranej;
        this.typPola = typPola;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola 
     * @param liczbaElementow Liczba pól graniczących w tym polu
     * @param typPola Typ pola
     */
    public Pole(String nazwa, int liczbaElementow, TypyPola typPola) {
        super(nazwa);
        obrazekPolaZZetonami = dajObrazek();
        liczbaZetonowNaPolu = 0;
        this.typPola = typPola;
        
        switch(liczbaElementow) {
            case 1:
                this.mnoznikWygranej = Pole.MnoznikiPola.STRAIGHT_UP_BET;
                break;
            case 2:
                this.mnoznikWygranej = Pole.MnoznikiPola.SPLIT_BET;
                break;
            case 3:
                this.mnoznikWygranej = Pole.MnoznikiPola.STREET_BET;
                break;
            case 4:
                this.mnoznikWygranej = Pole.MnoznikiPola.CORNER_BET;
                break;
            case 6:
                this.mnoznikWygranej = Pole.MnoznikiPola.SIX_LINE_BET;
                break;
            default:
                this.mnoznikWygranej = Pole.MnoznikiPola.BLAD;
                break;
        }
    }
    
    /**
     * Metoda abstrakcyjna zwracająca informację o tym czy wylosowana liczba powoduje, że stawione zakłady są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false) 
     */
    public abstract boolean sprawdzZaklad(int wylosowanePole);
    
    /**
     * Metoda zwracająca mnożnik pola
     * @return Mnożnik pola
     */
    public int dajWartoscMnoznikaWygranej()
    {
        return mnoznikWygranej.dajWartoscMnoznika();
    }
    
    /**
     * Metoda zwracająca obrazek pola z postawionymi żetonami o ile takie istnieją
     * @return Obrazek pola
     */
    public ImageIcon dajObrazekPolaZZetonami() {
        return obrazekPolaZZetonami;
    }
    
    /**
     * Metoda zwracająca typ pola
     * @return Typ pola
     */
    public TypyPola dajTypPola() {
        return typPola;
    }
    
    /**
     * Metoda zwracająca liczbę postawionych żetonów na polu
     * @return Liczba postawionych żetonów na polu
     */
    public int dajLiczbeZetonowNaPolu() {
        return liczbaZetonowNaPolu;
    }
    
    /**
     * Metoda sprawdzająca czy pole przekazane jako argument jest takiego samego typu jak pole, z którego została wywołana metoda
     * @param pole Pole, z którym porównujemy typy
     * @return Wartość logiczna (true, false)
     */
    public boolean equals(Pole pole) {        
        if(typPola == pole.dajTypPola()) {
            switch(typPola) {
                    case POLE_NUMERYCZNE:
                        if( ((PoleNumeryczne) this).dajWartosc() == ((PoleNumeryczne) pole).dajWartosc()) {
                            return true;
                        }
                        break;
                    case POLE_GRANICZNE:
                        if(Arrays.equals( ((PoleGraniczne) this).dajPola(), ((PoleGraniczne) pole).dajPola() )) {
                            return true;
                        };
                        break;
                    case POLE_SPECJALNE:
                        if(((PoleSpecjalne) this).dajTypPolaSpecjalnego() == ((PoleSpecjalne) pole).dajTypPolaSpecjalnego()) { 
                            switch(((PoleSpecjalne) this).dajTypPolaSpecjalnego()) {
                                case KOLOR:
                                    if ( ((PoleSpecjalneKolor) this).dajKolorPola() == ((PoleSpecjalneKolor) pole).dajKolorPola()) {
                                        return true;
                                    }
                                    break;
                                case KOLUMNA:
                                    if ( ((PoleSpecjalneKolumna) this).dajNumerKolumny()== ((PoleSpecjalneKolumna) pole).dajNumerKolumny()) {
                                        return true;
                                    }
                                    break;
                                case PARZYSTOSC:
                                    if ( ((PoleSpecjalneParzystosc) this).dajParzystosc()== ((PoleSpecjalneParzystosc) pole).dajParzystosc()) {
                                        return true;
                                    }
                                    break;
                                case POLOWKA:
                                    if ( ((PoleSpecjalnePolowka) this).dajPolowke()== ((PoleSpecjalnePolowka) pole).dajPolowke()) {
                                        return true;
                                    }
                                    break;
                                case TUZIN:
                                    if ( ((PoleSpecjalneTuzin) this).dajTuzin()== ((PoleSpecjalneTuzin) pole).dajTuzin()) {
                                        return true;
                                    }
                                    break;
                                case WIERSZ:
                                    if ( ((PoleSpecjalneWiersz) this).dajWiersz()== ((PoleSpecjalneWiersz) pole).dajWiersz()) {
                                        return true;
                                    }
                                    break;
                            }
                        }
                        break;
            }
            return false;
        } else {
            return false;
        }
    }
    
    /**
     * Metoda dodająca żeton na pole (wywoływana po kliknięciu na jakieś pole stołu).
     * @param kolorZetonu Kolor nazwy gracza. Kolor żetonu będzie zgodny z kolorem nazwy gracza, który postawić dany żeton.
     * @param czyGraczaLokalnego Informacja o tym czy żeton został postawiny przez gracza lokalnego 
     */
    public void dodajZeton(Color kolorZetonu, boolean czyGraczaLokalnego)
    {
        BufferedImage obrazPolaZZetonami = new BufferedImage((int) (dajObrazek().getIconWidth()), (int) (dajObrazek().getIconHeight()), 
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2D = obrazPolaZZetonami.createGraphics();        
        
        g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);         
        
        BufferedImage zeton = null;
        try {
            zeton = ImageIO.read(getClass().getResourceAsStream("/stol2/zeton2.png"));
        } catch (IOException ex) {
            Logger.getLogger(Pole.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int width = zeton.getWidth();
        int height = zeton.getHeight();
        WritableRaster raster = zeton.getRaster();

        for (int xx = 0; xx < width; xx++) {
            for (int yy = 0; yy < height; yy++) {
                int[] pixels = raster.getPixel(xx, yy, (int[]) null);
                if(pixels[0] == 175 && pixels[1] == 175 && pixels[2] == 175) {
                    pixels[0] = kolorZetonu.getRed();
                    pixels[1] = kolorZetonu.getGreen();
                    pixels[2] = kolorZetonu.getBlue();
                    raster.setPixel(xx, yy, pixels);
                }
            }
        }
      
        g2D.setColor(Color.black);
        g2D.drawImage(dajObrazek().getImage(), 0,0, null);
        g2D.drawImage(zeton, (int) ((dajObrazek().getImage().getWidth(null)/2) - ((zeton.getWidth(null) * 0.3))/2), (int) ((dajObrazek().getImage().getHeight(null)/2) - ((zeton.getHeight(null) * 0.3))/2), (int) (zeton.getWidth(null) * 0.3), (int) (zeton.getHeight(null) * 0.3),null);
        
        if (czyGraczaLokalnego) {
            liczbaZetonowNaPolu++;
            Font czcionka = g2D.getFont().deriveFont(Font.BOLD);
            if (liczbaZetonowNaPolu < 10)
            {
                g2D.setFont(czcionka);
                g2D.drawString(String.valueOf(liczbaZetonowNaPolu),(int) ((dajObrazek().getImage().getWidth(null)/2) - ((zeton.getWidth(null) * 0.3))/2) + 6, (int) ((dajObrazek().getImage().getHeight(null)/2) - ((zeton.getHeight(null) * 0.3))/2) + 13);
            }
            else
            {
                czcionka = czcionka.deriveFont(Font.BOLD, (float) czcionka.getSize() - 2);
                g2D.setFont(czcionka);
                g2D.drawString(String.valueOf(liczbaZetonowNaPolu),(int) ((dajObrazek().getImage().getWidth(null)/2) - ((zeton.getWidth(null) * 0.3))/2) + 3, (int) ((dajObrazek().getImage().getHeight(null)/2) - ((zeton.getHeight(null) * 0.3))/2) + 12);
            }
        }
        g2D.dispose();
        obrazekPolaZZetonami = new ImageIcon((Image) obrazPolaZZetonami);
        setIcon(obrazekPolaZZetonami);
    }
    
    /**
     * Metoda odpowiedzialna za wyczyszczenie pola z wszystkich żetonów
     */
    public void wyczyscPole() {
        liczbaZetonowNaPolu=0;
        obrazekPolaZZetonami = dajObrazek();
        setToolTipText(null);
        setIcon(dajObrazek());
    }
    
    @Override
    public String toString() {
        return "T" + dajTypString() + ",F" + dajWartoscString();
    }
    
    public String dajTypString() {
        return String.valueOf(dajTypPola().dajIdTypu());
    }
    
    public String dajWartoscString() {
        String wartosc = "";
        switch(dajTypPola()) {
            case POLE_NUMERYCZNE:
                wartosc = String.valueOf(((PoleNumeryczne)this).dajWartosc());
                break;
            case POLE_GRANICZNE:
                int[] wartosciPol = ((PoleGraniczne)this).dajPola();
                for(int i = 0; i < wartosciPol.length; i++) {
                    wartosc += String.valueOf(wartosciPol[i]);
                    if( i < wartosciPol.length-1) {
                        wartosc += ".";
                    }
                }
                break;
            case POLE_SPECJALNE:
                PoleSpecjalne.TypyPolaSpecjalnego typPolaSpecjalnego = ((PoleSpecjalne)this).dajTypPolaSpecjalnego();
                wartosc = String.valueOf(typPolaSpecjalnego.ordinal()) + ".";
                
                switch(typPolaSpecjalnego) {
                    case KOLOR:
                        wartosc += String.valueOf(((PoleSpecjalneKolor)this).dajKolorPola().ordinal());
                        break;
                    case KOLUMNA:
                        wartosc += String.valueOf(((PoleSpecjalneKolumna)this).dajNumerKolumny());
                        break;
                    case PARZYSTOSC:
                        wartosc += String.valueOf(((PoleSpecjalneParzystosc)this).dajParzystosc().ordinal());
                        break;
                    case POLOWKA:
                        wartosc += String.valueOf(((PoleSpecjalnePolowka)this).dajPolowke().ordinal());
                        break;
                    case TUZIN:
                        wartosc += String.valueOf(((PoleSpecjalneTuzin)this).dajTuzin().ordinal());
                        break;
                    case WIERSZ:
                        wartosc += String.valueOf(((PoleSpecjalneWiersz)this).dajWiersz().ordinal());
                        break;
                }
                break;
                
        }
        
        return wartosc;
    }
    
    /**
     * Metoda statyczna odpowiedzialna za stworzenie instancji klasy pole z podanymi parametrami
     * @param typPola Typ pola
     * @param wartoscPola Wartość pola
     * @return Zwraca stworzoną instancję klasy pole
     */
    public static Pole stworzPole(String typPola, String wartoscPola) {
        Pole.TypyPola typ = Pole.TypyPola.values()[Integer.valueOf(typPola)];
        Pole p = null;
        String[] wartosciString = null;
        switch(typ) {
            case POLE_NUMERYCZNE:
                int wartosc = Integer.valueOf(wartoscPola);
                p = new PoleNumeryczne(wartosc, null);
                break;
            case POLE_GRANICZNE:
                wartosciString = wartoscPola.split("\\.");
                int[] wartosci = new int[wartosciString.length];
                for(int i = 0; i < wartosciString.length; i++) {
                    wartosci[i] = Integer.parseInt(wartosciString[i]);
                }
                
                p = new PoleGraniczne(null, wartosci);
                
                break;
            case POLE_SPECJALNE:
                wartosciString = wartoscPola.split("\\.");
                int indeksTypuPolaSpecjalnego = Integer.valueOf(wartosciString[0]);
                PoleSpecjalne.TypyPolaSpecjalnego typPolaSpecjalnego = PoleSpecjalne.TypyPolaSpecjalnego.values()[ indeksTypuPolaSpecjalnego ];
                
                switch(typPolaSpecjalnego) {
                    case KOLOR:
                        PoleSpecjalneKolor.Kolory kolor =  PoleSpecjalneKolor.Kolory.values()[Integer.valueOf(wartosciString[1])];
                        p = new PoleSpecjalneKolor(null, kolor);
                        break;
                    case KOLUMNA:
                        p = new PoleSpecjalneKolumna(null, Integer.valueOf(wartosciString[1]));
                        break;
                    case PARZYSTOSC:
                        PoleSpecjalneParzystosc.Parzystosc parzystosc = PoleSpecjalneParzystosc.Parzystosc.values()[Integer.valueOf(wartosciString[1])];
                        p = new PoleSpecjalneParzystosc(null, parzystosc);
                        break;
                    case POLOWKA:
                        PoleSpecjalnePolowka.Polowki polowka = PoleSpecjalnePolowka.Polowki.values()[Integer.valueOf(wartosciString[1])];
                        p = new PoleSpecjalnePolowka(null, polowka);
                        break;
                    case TUZIN:
                        PoleSpecjalneTuzin.Tuziny tuzin = PoleSpecjalneTuzin.Tuziny.values()[Integer.valueOf(wartosciString[1])];
                        p = new PoleSpecjalneTuzin(null, tuzin);
                        break;
                    case WIERSZ:
                        PoleSpecjalneWiersz.Wiersze wiersz = PoleSpecjalneWiersz.Wiersze.values()[Integer.valueOf(wartosciString[1])];
                        p = new PoleSpecjalneWiersz(null, wiersz);
                        break;
                }
        }   
        
        return p;
    }
}
