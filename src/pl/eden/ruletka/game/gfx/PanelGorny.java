package pl.eden.ruletka.game.gfx;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import pl.eden.ruletka.game.Gra;

/**
 * Klasa reprezentująca panel górny z grafiką ruletki, dostępnymi żetonami oraz kwotą pieniędzy gracza
 * @author Mateusz Macięga
 */
public class PanelGorny extends JPanel {
    /** Tło panelu */
    private Image tloObrazek;
    /** Panel z grafiką ruletki */
    private final JPanel panelZRuletka = new PanelZRuletka();
    /** Panel z belka pieniężna i żetonami */
    private final JPanel panelZBelkaIZetonami = new PanelZBelkaIZetonami();
    /** Wątek odpowiedzialny za kręcenie grafiką ruletki */
    private Thread  watek;
    
    /**
     * Konstruktor prywatny. Zastosowano wzorzec Singleton
     */
    private PanelGorny()
    {
        try 
        {
            tloObrazek = ImageIO.read(getClass().getResourceAsStream("/pasek_gora.png"));
        } 
        catch (IOException ex) { System.err.printf("Wystapil wyjatek " + ex.getMessage());}
        
        setPreferredSize(new Dimension(tloObrazek.getWidth(null), tloObrazek.getHeight(null)));
        setOpaque(false);
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0,40,0,40));
          
        add(panelZBelkaIZetonami, BorderLayout.WEST );
        add(panelZRuletka, BorderLayout.EAST );
        
        watek = new Thread((Runnable) panelZRuletka, "Watek kręcący ruletką");
    }
    
    /**
     * Metoda zwracająca instancję klasy
     * @return Instancja klasy PanelGorny
     */
    public static PanelGorny dajInstancje() {
        return PanelGornyInstancja.INSTANCJA;
    }
    
    /**
     * Metoda statyczna tworząca instację klasy
     */
    private static class PanelGornyInstancja {
        private static final PanelGorny INSTANCJA = new PanelGorny();
    }
    
    /**
     * Metoda ustawiająca nową wartość czasu przeznaczone na ruch
     * @param czasNaRuch Nowa wartość czasu przeznaczonego na ruch
     */
    public void ustawCzasNaRuch(long czasNaRuch) {
        ((PanelZRuletka)panelZRuletka).czasNaRuch.setText(czasNaRuch/60 + ":" + String.format("%02d", czasNaRuch%60));
    }
    
    /**
     * Metoda odpowiedzialna za uruchomienie wątku odpowiedzialnego za rotację grafiki ruletki 
     */
    public void zakrecRuletka() {
            if (watek.isAlive() == false) {
                watek = new Thread((Runnable) panelZRuletka, "Watek kręcący ruletką");
                watek.start();
            }
    }
    
    /**
     * Metoda zarządzają panelem okna (rysowanie tła)
     * @param g 
     */    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;    
        g2D.drawImage(tloObrazek.getScaledInstance(getWidth(), tloObrazek.getHeight(null), Image. SCALE_SMOOTH), 0, 0, null);
    }
    
    /**
     * Klasa reprezentująca prawy panel z ruletką
     */
    class PanelZRuletka extends JPanel implements Runnable  {
        /** Obrazek ruletki */
        private Image tloRuletka;
        /** Kąt obrotu ruletki */
        private int katObrocenia;
        /** Etykieta z czasem przeznaczonym na ruch */
        private JLabel czasNaRuch;
        /** Informacja o tym czy wątek odpowiedzialny za rotację grafiki ruletki jest uruchomiony */
        private volatile boolean isRunning = true;
        
        /**
         * Konstruktor domyślny
         */
        public PanelZRuletka() {               
            try 
            {
                tloRuletka = ImageIO.read(getClass().getResourceAsStream("/ruletka.png"));
            } 
            catch (IOException ex) {}
            
            setOpaque(false);
            setDoubleBuffered(true);
            setPreferredSize(new Dimension(tloRuletka.getWidth(null), tloRuletka.getHeight(null)));
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            
            //dodajemy komponenty do naszego panelu
            JLabel tekstCzasNaRuch = new JLabel("Czas na ruch");
            tekstCzasNaRuch.setFont(new Font("Verdana", Font.BOLD, 16));
            tekstCzasNaRuch.setForeground(Color.orange);
            tekstCzasNaRuch.setAlignmentX( Component.RIGHT_ALIGNMENT );
            
            czasNaRuch = new JLabel(" ");
            czasNaRuch.setFont(new Font("Verdana", Font.PLAIN, 14));
            czasNaRuch.setForeground(Color.orange);
            czasNaRuch.setAlignmentX( Component.RIGHT_ALIGNMENT );
            
            add(Box.createVerticalGlue());
            add(tekstCzasNaRuch);
            add(czasNaRuch);
            add(Box.createRigidArea(new Dimension(0,10)));
        }
            
        /**
         * Metoda odpowiedzialna za renderowanie grafiki ruletki 
         * @param g 
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            Graphics2D g2D = (Graphics2D) g; 

            BufferedImage obrazPolaZZetonami = new BufferedImage((int) (tloRuletka.getWidth(null)), (int) (tloRuletka.getHeight(null)), 
                BufferedImage.TYPE_INT_ARGB);
            Graphics2D ruletkag2D = obrazPolaZZetonami.createGraphics();        
            ruletkag2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            ruletkag2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            ruletkag2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            ruletkag2D.rotate(Math.toRadians(katObrocenia), (tloRuletka.getWidth(null)/2), (tloRuletka.getHeight(null)/2));
            ruletkag2D.drawImage(tloRuletka,0,0, null);
            ruletkag2D.dispose();
            
            g2D.drawImage(obrazPolaZZetonami, 0, -275, null);
            
            Toolkit.getDefaultToolkit().sync();

        }

        /**
         * Metoda odpowiedzialna za wątek. Obracanie ruletką.
         * @param g 
         */      
        @Override
        public void run() {
            float czasTrwania = 0;
            long czasRozpoczecia = System.currentTimeMillis();
            long roznicaCzasu;

            while(isRunning)
            {
               try {
                    katObrocenia += 1;

                    getParent().repaint();
                    roznicaCzasu = System.currentTimeMillis() - czasRozpoczecia;

                    if(roznicaCzasu <= 1000)
                    {
                        czasTrwania += 0.03f;  
                    }
                    else if(roznicaCzasu <= 2000)
                    {
                        czasTrwania += 0.3;  
                    }
                    else if(roznicaCzasu <= 3000) {
                       czasTrwania += 0.7;
                   }
                    else if(roznicaCzasu <= 3500) {
                       czasTrwania += 1.6;
                   }
                    else {
                       czasTrwania += 2;
                   }

                    Thread.sleep((int) czasTrwania);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PanelZRuletka.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                if(System.currentTimeMillis() - czasRozpoczecia >= 4300) {
                   break;
               }
            }
        }
        
        /**
         * Metoda odpowiedzialna za zakończenie pracy wątku odpowiedzialnego za obracanie grafiką ruletki
         */
        public void zabijProces() {
            isRunning = false;
        }
    }
    
    /**
     * Klasa reprezentująca lewy panel z ramką i kwotą pieniężną gracza
     */
    class PanelZBelkaIZetonami extends JPanel implements MouseListener {
        /** Lista żetonów do wyboru */
        private final java.util.List<ElementGraficzny> listaZetonow = new ArrayList<>();
        /** Panel z żetonami */
        private final JPanel panelZZetonami;
        
        /**
         * Konstruktor domyślny
         */
        public PanelZBelkaIZetonami() {
            setOpaque(false);           
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setBorder(new EmptyBorder(23,0,0,0));                       //margines od góry
            
            PieniadzeGracza pieniadze = new PieniadzeGracza();
            //pieniadze.setBorder(new LineBorder(Color.blue));
            pieniadze.setAlignmentX(Component.LEFT_ALIGNMENT);
            //pieniadze.setBorder(new EmptyBorder(0, 7, 0, 0));            //doruwnujemy do panelu z żetonami

            JPanel panelSrodkowy = new JPanel();
            panelSrodkowy.setOpaque(false);    

            JPanel panelOut = new JPanel(new GridBagLayout());
            panelZZetonami = new JPanel(new FlowLayout(FlowLayout.CENTER,7,0));
            panelZZetonami.setOpaque(false);
            
            dodajZetonDoPanelu("zeton_1.png");
            dodajZetonDoPanelu("zeton_5.png");
            dodajZetonDoPanelu("zeton_10.png");
            dodajZetonDoPanelu("zeton_25.png");
            dodajZetonDoPanelu("zeton_50.png");
            dodajZetonDoPanelu("zeton_100.png");
           
            panelOut.setOpaque(false);
            panelOut.add(panelZZetonami, new GridBagConstraints());
            panelOut.setAlignmentX(Component.LEFT_ALIGNMENT);
            
            //dodajemy komponenty do naszego panelu
            add(pieniadze);
            add(Box.createRigidArea(new Dimension(0,30)));
            //add(panelZZetonami);
            add(panelOut);
            
        }
        
        /**
         * Metoda odpowiedizalna za dodanie kolejnego żetonu do listy żetonów
         * @param nazwaPliku Nazwa pliku z graficzną preznetacją żetonu
         */
        private void dodajZetonDoPanelu(String nazwaPliku) {
            JLabel zeton;
            zeton = new ElementGraficzny(nazwaPliku);
            ((ElementGraficzny) zeton).skalujProporcjonalnie(0.8);
            zeton.addMouseListener(this);
            zeton.addMouseMotionListener(ZmianaKursora.dajInstancje());
            listaZetonow.add((ElementGraficzny) zeton);
            panelZZetonami.add(zeton);
        }

        /**
         * Metoda odpowiedzialna za obsługę zdarzenia kliknięcia w jeden z dostępnych żetonów do stawiania zakładów. Kliknięcie w żeton powoduje zwiększenie jego rozmiaru.
         * @param me 
         */
        @Override
        public void mouseClicked(MouseEvent me) {
            for(ElementGraficzny zeton : listaZetonow)
            {
                if (zeton == me.getSource()) {
                    ((ElementGraficzny) zeton).skalujProporcjonalnie(1);
                    String zetonTekst = ((ElementGraficzny) zeton).dajNazwaPliku();
                    String wartoscZetonu = zetonTekst.substring(6, zetonTekst.lastIndexOf("."));
                    
                    Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
                    gra.dajGracza().ustawWartoscWybranegoZetonu(Integer.parseInt(wartoscZetonu));
                }
                else {
                    ((ElementGraficzny) zeton).skalujProporcjonalnie(0.8);
                }
            }
            
        }

//<editor-fold defaultstate="collapsed" desc="inne zdarzenia (niezaimplementowane)">
        @Override
        public void mousePressed(MouseEvent me) {
        }
        
        @Override
        public void mouseReleased(MouseEvent me) {
        }
        
        @Override
        public void mouseEntered(MouseEvent me) {
        }
        
        @Override
        public void mouseExited(MouseEvent me) {
        }
//</editor-fold>
    }
}
