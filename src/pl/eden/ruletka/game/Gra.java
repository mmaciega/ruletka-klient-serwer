package pl.eden.ruletka.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.gfx.OknoPodsumowanieZakl;
import pl.eden.ruletka.game.gfx.PanelDolny;
import pl.eden.ruletka.game.gfx.PanelGorny;
import pl.eden.ruletka.game.gfx.PanelSrodkowy;
import pl.eden.ruletka.game.logic.ListaGraczyModel;
import pl.eden.ruletka.game.logic.MenadzerZakladow;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca rozgrywkę w Ruletkę. Okno główne gry, w którym wyświetlana jest rozgrywka.
 * @author Mateusz Macięga
 */
public class Gra extends JFrame {
    /** Stały rozmiar okna, pierwszy element to szereokość, drugi wysokość okna */
    public final static int[] POCZATKOWY_ROZMIAR_OKNA = {800, 600};
    /** Nazwa okna */
    public final static String NAZWA = "Ruletka";
    
    /** Instacje każdej części okna głównego */
    private final JPanel panelGorny = PanelGorny.dajInstancje();
    private final JPanel panelSrodkowy = PanelSrodkowy.dajInstancje();
    private final JPanel panelDolny = PanelDolny.dajInstancje();
    /** Numer portu na jakim zostanie uruchomiony serwer gry */
    private final int numerPortuSerwera = 12345;
    /** Domyślny adres IP, który zostanie wyświetlony w oknie dialogowym do łączenia z serwerem */
    private String adresIPSerwera = "127.0.0.1";

    private final Gracz gracz;
    private final ListaGraczyModel listaGraczy = new ListaGraczyModel();
    private MenadzerZakladow menadzerZakladow = new MenadzerZakladow(listaGraczy);
    
    private final KlientGry gniazdoKlienta;
    private SerwerGry gniazdoSerwera;

    private final ObslugaOkna obslugaOkna;
    
    private final JPanel oknoPodsumowanieZakl = new OknoPodsumowanieZakl();
    
    private boolean flagaWait = false;

    /**
     * Konstruktor domyślny dla klasy gra.
     */
    public Gra() {
        getContentPane().setBackground(new Color(23, 86, 43));
        setPreferredSize(new Dimension(POCZATKOWY_ROZMIAR_OKNA[0], POCZATKOWY_ROZMIAR_OKNA[1]));
        setMinimumSize(new Dimension(POCZATKOWY_ROZMIAR_OKNA[0], POCZATKOWY_ROZMIAR_OKNA[1]));
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setGlassPane(oknoPodsumowanieZakl);  

        ToolTipManager.sharedInstance().setInitialDelay(20);    //liczba milisekund po jakich pojawi się dymek z informacją
       
        if(JOptionPane.showConfirmDialog(this, "Czy chcesz uruchomić server?", "Serwer", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                gniazdoSerwera = new SerwerGry(numerPortuSerwera, this);
                gniazdoSerwera.start();
        } else {
            while(true) {
                JPanel panel = new JPanel();
                panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
                JLabel label = new JLabel("Podaj adres IP serwera:");
                JTextField ipAdres = new JTextField("127.0.0.1");
                panel.add(label);
                panel.add(ipAdres);
                String[] opcje = new String[]{"OK"};
                JOptionPane.showOptionDialog(null, panel, "Adres IP serwera",
                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                         null, opcje, opcje[0]);

                if (ipAdres.getText().matches("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$")) {
                    adresIPSerwera = ipAdres.getText();
                    break;
                } else {
                    JOptionPane.showMessageDialog(this, "Nie poprawny format adresu IP.\nAkceptowalny format jest nastepujący: xxx.xxx.xxx.xxx", "Błędny adres IP", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        String pseudonim = null;
        do {
            pseudonim = JOptionPane.showInputDialog("Podaj pseudonim (max 15 znakow)");
            if(pseudonim != null) {
                pseudonim = pseudonim.trim();
            } else {
                //zamykamy aplikację
                dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            }       
        } while (pseudonim.isEmpty() || pseudonim.length() > 15);
        gracz = new GraczMP(-1, pseudonim, null, -1);
        ((PanelDolny)panelDolny).ustawListeCzatu(listaGraczy);
        
        
        gniazdoKlienta = new KlientGry(numerPortuSerwera, adresIPSerwera, this);
        gniazdoKlienta.start();
        obslugaOkna = new ObslugaOkna(this);
        
        add(panelDolny, BorderLayout.PAGE_END);
        add(panelSrodkowy, BorderLayout.CENTER);
        add(panelGorny, BorderLayout.PAGE_START);  
    }

    /**
     * Metoda zwracająca instancję gracza lokalnego
     * @return instancja gracza lokalnego
     */
    public synchronized Gracz dajGracza() {
        return gracz;
    }

    /**
     * Metoda odpowiedzialna za dodanie gracza do listy graczy uczestniczących w rozgrywce
     * @param nowyGracz Instancja gracza, który ma zostać dodany do listy graczy
     */
    public synchronized void dodajGracza(Gracz nowyGracz) {
        dajListeGraczy().addElement(nowyGracz);
        ((PanelDolny) PanelDolny.dajInstancje()).revalidate();
    }
    
    /**
     * Metoda odpowiedzialna za modyfikację wartości pieniężnych graczy uczestniczących w rozgrywce
     * @param gracz Instacja gracza, któremu zmieniamy wartość pieniężną
     * @param pieniadze Wartość pieniężna na jaką zmieniamy
     */
    public synchronized void zmodyfikujPieniadzeGracza(Gracz gracz, long pieniadze) {
        dajListeGraczy().changeElement(gracz,pieniadze);
        ((PanelDolny) PanelDolny.dajInstancje()).revalidate();
    }
    
    /**
     * Metoda odpowiedzialna za zmiane statusu zakończenia składania zakładów przez gracza.
     * @param gracz Instacja gracza, któremu zmieniamy status zakończenia składania zakładów
     * @param b Wartość logiczną, na jaką zmieniamy status zakończenia składania zakładów
     */
    public synchronized void zmodyfikujZakonczenieZaklGracza(Gracz gracz, boolean b) {
        dajListeGraczy().changeFinishedState(gracz,b);
        ((PanelDolny) PanelDolny.dajInstancje()).revalidate();
    }
    
    /**
     * Metoda odpowiedzialna za usunięcie gracza z listy graczy uczestniczących w rozgrywce
     * @param pseudonim Nazwa gracza, którego chcemy usunąć z listy graczy
     */
    public synchronized void usunGracza(String pseudonim) {
        int indeks = 0;
        for (Gracz g : dajListeGraczy()) {
            if (g instanceof GraczMP && ((GraczMP) g).dajPseudonim().equals(pseudonim)) {
                break;
            }
            indeks++;
        }
        this.dajListeGraczy().remove(indeks);
        ((PanelDolny) PanelDolny.dajInstancje()).revalidate();
    }
    
    /**
     * Metoda zwracająca instację klasy będącej listą połączonych graczy
     * @return Instancja listy graczy
     */
    public synchronized ListaGraczyModel dajListeGraczy() {
        return listaGraczy;
    }
    
    /**
     * Metoda zwracająca indeks gracza na liście połączonych gracza z podaną nazwą gracza
     * @param pseudonim Nazwa gracza, którego indeks na liście szukamy
     * @return Pozycja gracza o podanej nazwie na liście graczy
     */
    public int dajIndeksGraczaMP(String pseudonim) {
        return listaGraczy.dajIndeksGraczaMP(pseudonim);
    }
    
    /**
     * Metoda zwracająca wartość flagi wait (flaga związana z zablokowaniem możliwości interacji z niektórymi elemntami interfejsu graficznego podczas wyświetlania okna podsumowującego losowanie)
     * @return Wartość flagi wait
     */
    public boolean czyFlagaWait() {
        return flagaWait;
    }
    
    /**
     * Metoda ustawiająca wartość flagi wait na nową wartość logiczna
     * @param flagaWait Wartość logiczna na jaką zostanie ustawiona flaga wait
     */
    public void ustawFlagaWait(boolean flagaWait) {
        this.flagaWait = flagaWait;
    }
    
    /**
     * Metoda zwracająca instancje menadzera zakładów posiadającego informacje na temat wszystkich złożonych zakładów
     * @return Instancja menadżera zakładów
     */
    public MenadzerZakladow dajMenadzerZakladow() {
        return menadzerZakladow;
    }
    
    /**
     * Metoda ustawiająca nowego menadzera zakładów
     * @param menadzerZakladow Instancja nowego menadżera zakładów
     */
    public void ustawMenadzerZakladow(MenadzerZakladow menadzerZakladow) {
        this.menadzerZakladow = menadzerZakladow;
    }
    
    /**
     * Metoda zwracająca instancję gniazda klienta gry
     * @return Instancja gniazda klienta gry
     */
    public KlientGry dajGniazdoKlienta() {
        return gniazdoKlienta;
    }
    
    /**
     * Metoda zwracająca instancję gniazda serwera gry
     * @return Instancja gniazda serwera gry 
     */
    public SerwerGry dajSerwerGry() {
        return gniazdoSerwera;
    }
}
