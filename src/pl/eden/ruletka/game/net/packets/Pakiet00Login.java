package pl.eden.ruletka.game.net.packets;

import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do logowania gracza do serwera
 * @author Mateusz Macięga
 */
public class Pakiet00Login extends Pakiet {
    /** Nazwa gracza, który dołącza do rozgrywki */
    private final String pseudonimGracza;
    /** Informacja o tym czy ten gracz zakończył już zakłady */
    private final boolean czyZakonczylZaklady;
    /** Kolor nazwy gracza */
    private final Color kolorGracza;
    /** Hasło dostępu do serwera jeśli jest wymagane */
    private final String hasloSerwera;
    /** Aktualne pieniądze gracza */
    private final long pieniadzeGracza;
    
    /**
     * Konstruktor sparametryzowany.
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt 
     */
    public Pakiet00Login(byte[] dane) {
        super(00);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.kolorGracza = new Color(Integer.parseInt(wiadomosc[1].substring(1)), Integer.parseInt(wiadomosc[2].substring(1)), Integer.parseInt(wiadomosc[3].substring(1)));
        this.hasloSerwera = wiadomosc[4].substring(1);
        this.pieniadzeGracza = Integer.parseInt(wiadomosc[5].substring(1));
        this.czyZakonczylZaklady = Integer.parseInt(wiadomosc[6].substring(1)) == 1;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który chce się zalogować do serwera
     */
    public Pakiet00Login(Gracz gracz) {
        super(00);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.kolorGracza = gracz.dajKolorGracza();
        this.pieniadzeGracza = gracz.dajPieniadze();
        this.hasloSerwera = "";
        this.czyZakonczylZaklady = gracz.czyZakonczylZaklady();
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który chce się zalogować do serwera
     * @param hasloSerwera Hasło dostępu do serwera gry
     */
    public Pakiet00Login(Gracz gracz, String hasloSerwera) {
        super(00);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.kolorGracza = gracz.dajKolorGracza();
        this.pieniadzeGracza = gracz.dajPieniadze();
        this.hasloSerwera = hasloSerwera;
        this.czyZakonczylZaklady = false;
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu logowania gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet00Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu informującego o logowaniu gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet00Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("00" + pseudonimGracza + ",R" + kolorGracza.getRed()+ ",G" + kolorGracza.getGreen() + ",B" + kolorGracza.getBlue() + ",P" + hasloSerwera + ",M" + pieniadzeGracza + ",F" + (czyZakonczylZaklady ? "1" : "0") +"K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza
     * @return Nazwa gracza 
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }

    /**
     * Metoda zwracająca kolor nazwy gracza
     * @return Kolor nazwy gracza 
     */
    public Color dajKolorGracza() {
        return kolorGracza;
    }
    
    /**
     * Metoda zwracająca hasło dostępu do serwera gry
     * @return Hasło dostępu do serwera gry
     */
    public String dajHasloSerwera() {
        return hasloSerwera;
    }
    
    /**
     * Metoda zwracająca wartość pienieżną gracza
     * @return Wartość pieniężna gracza
     */
    public long dajPieniadzeGracza() {
        return pieniadzeGracza;
    }
    
    /**
     * Metoda zwracająca informacja o tym, czy gracza zakończył składanie zakładów
     * @return Informacja o tym, czy gracza zakończył składanie zakładów
     */
    public boolean czyZakonczylZaklady() {
        return czyZakonczylZaklady;
    }
}
