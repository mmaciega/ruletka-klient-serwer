package pl.eden.ruletka.game.logic;

import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.net.packets.Pakiet14NewGame;

/**
 * Klasa odpowiedzialna za rozpoczęcie nowej rozgrywki przez gracza
 * @author Mateusz Macięga
 */
public class NowaGra {
    
    private final Timer timer;
    
    private final Gra gra;
    
    /**
     * Konstruktor spramatryzowany
     * @param gra Instancja gry
     */
    public NowaGra(Gra gra) {
        this.gra = gra;
        timer = new Timer();
        timer.schedule(new NowaGra.Zadanie(), 0);
    }
    
    /**
     * Klasa zawierająca instrukcję wyświetlające okno dialogowe z pytaniem czy grasz chce zagrać od nowa
     */
    class Zadanie extends TimerTask{
        @Override
        public void run(){            
            if(JOptionPane.showConfirmDialog(gra, "Przegrałeś!\nCzy chcesz zagrać od nowa?", "Przegrana", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                Pakiet14NewGame pakiet = new Pakiet14NewGame(gra.dajGracza());
                pakiet.piszDane(gra.dajGniazdoKlienta());
            } else {
                gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
            }
        }
    }
    
}
