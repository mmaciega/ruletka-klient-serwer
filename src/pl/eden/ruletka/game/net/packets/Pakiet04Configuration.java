package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.gfx.fields.Pole;
import pl.eden.ruletka.game.logic.ListaGraczyModel;
import pl.eden.ruletka.game.logic.MenadzerZakladow;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do przesłania parametrów serwera dotyczących rozgrywki
 * @author Mateusz Macięga
 */
public class Pakiet04Configuration extends Pakiet {
    /** Pieniądze startowe rozgrywki */
    private long pieniadzeStartowe;
    /** Czas przeznaczony na ruch */
    private long czasNaRuch;
    /** Zbiór wszystkich złożonych zakładów w danej rozgrywce */
    private MenadzerZakladow menadzerZakladow;
    
    /**
     * Konstruktor sprametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     * @param lista Lista graczy uczestniczących w rozgrywce
     */
    public Pakiet04Configuration(byte[] dane, ListaGraczyModel lista) {
        super(04);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        try {
            this.pieniadzeStartowe = Integer.parseInt(wiadomosc[0].substring(1));
            this.czasNaRuch = Long.parseLong(wiadomosc[1].substring(1));
            this.menadzerZakladow = new MenadzerZakladow(lista);

            String pseudonimGracza;
            wiadomosc[2] = wiadomosc[2].substring(1);
            if(!wiadomosc[2].equals("")) {
                for(int i=2; i<wiadomosc.length; i+=2) {
                    pseudonimGracza = wiadomosc[i];
                    String[] zaklady = wiadomosc[i+1].split(";");

                    String typPola = ""; String wartosci = ""; int wartoscZakladu; Pole pole;
                    for(int j = 0; j< zaklady.length; j++) {
                        String[] zaklad = zaklady[j].split("\\.");
                        typPola=zaklad[0];

                        wartosci = "";
                        for(int k = 1; k< zaklad.length-1; k++) {
                            wartosci+=zaklad[k] + ".";
                        }
                        
                        wartosci = wartosci.substring(0, wartosci.length()-1);
                        pole = Pole.stworzPole(typPola, wartosci);
                        wartoscZakladu = Integer.parseInt(zaklad[zaklad.length-1]);

                        menadzerZakladow.dodajZaklad(pseudonimGracza, wartoscZakladu, pole);
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Pieniądze startowe rozgrywki
     * @param czasNaRuch Czas przeznaczony na ruch
     * @param menadzerZakladow Instancja klasy zawierającej wszystkie złożone zakłady w aktualnej rozgrywce
     */
    public Pakiet04Configuration(long pieniadzeStartowe, long czasNaRuch, MenadzerZakladow menadzerZakladow) {
        super(04);
        this.pieniadzeStartowe = pieniadzeStartowe;
        this.menadzerZakladow = menadzerZakladow;
        this.czasNaRuch = czasNaRuch;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z parametrami dotyczącymi rozgrywki
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet04Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z parametrami dotyczącymi rozgrywki
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet04Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("04M" + pieniadzeStartowe + ",T" + czasNaRuch +",B" + menadzerZakladow +"K").getBytes();
    }
    
    /**
     * Metoda zwracająca pieniądze startowe rozgrywki
     * @return Pieniądze startowe
     */
    public long dajPieniadzeStartowe() {
        return pieniadzeStartowe;
    }
    
    /**
     * Metoda zwracająca instankcję klasy zawierającą wszystkie złożone zakłady w aktualnej rozgrywce
     * @return Instancja klasy z zakładami
     */
    public MenadzerZakladow dajMenadzerZakladow() {
        return menadzerZakladow;
    }
    
    /**
     * Metoda zwracająca czas przeznaczony na ruch
     * @return Czas przeznaczony na ruch
     */
    public long dajCzasNaRuch() {
        return czasNaRuch;
    }
}
