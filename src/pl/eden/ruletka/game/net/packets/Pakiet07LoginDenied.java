package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klienta o odrzuceniu próby dołączenia do rozgrywki
 * @author Mateusz Macięga
 */
public class Pakiet07LoginDenied extends Pakiet {
    /** Informacja o tym czy adres IP gracza jest zbanowany */
    private final boolean czyZbanowany;
    /** Informacja o tym czy nazwa gracza jest już zajęta */
    private final boolean czyPseudonimZajety;
    /** Informacja o tym czy podane hasło dostępu do serwera jest nieprawidłowe */
    private final boolean czyZleHaslo;
    /** Informacja o tym czy serwer gry jest już pełny */
    private final boolean czyBrakMiejsc;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet07LoginDenied(byte[] dane) {
        super(07);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.czyZbanowany = Integer.parseInt(wiadomosc[0].substring(1)) == 1;
        this.czyPseudonimZajety = Integer.parseInt(wiadomosc[1].substring(1)) == 1;
        this.czyZleHaslo = Integer.parseInt(wiadomosc[2].substring(1)) == 1;
        this.czyBrakMiejsc = Integer.parseInt(wiadomosc[3].substring(1)) == 1;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param czyZbanowany Informacja o tym czy adres IP gracza jest zbanowany
     * @param czyPseudonimZajety Informacja o tym czy nazwa gracza jest już zajęta
     * @param czyZleHaslo Informacja o tym czy podane hasło dostępu do serwera jest nieprawidłowe
     * @param czyBrakMiejsc Informacja o tym czy serwer gry jest już pełny
     */
    public Pakiet07LoginDenied(boolean czyZbanowany, boolean czyPseudonimZajety, boolean czyZleHaslo, boolean czyBrakMiejsc) {
        super(07);
        this.czyZbanowany = czyZbanowany;
        this.czyPseudonimZajety = czyPseudonimZajety;
        this.czyZleHaslo = czyZleHaslo;
        this.czyBrakMiejsc = czyBrakMiejsc;
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o odrzeceniu próby dołączenia do rozgrywki
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet07LoginDenied.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o odrzeceniu próby dołączenia do rozgrywki
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet07LoginDenied.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("07B" + (czyZbanowany ? 1 : 0) + ",U" + (czyPseudonimZajety ? 1 : 0) + ",P" + (czyZleHaslo ? 1 : 0) 
                + ",F" + (czyBrakMiejsc ? 1 : 0) +"K").getBytes();
    }
    
    /**
     * Metoda zwracająca informację o tym czy adres IP gracza jest zbanowany
     * @return Informacja o tym czy adres IP gracza jest zbanowany
     */
    public boolean czyZbanowany() {
        return czyZbanowany;
    }
    
    /**
     * Metoda zwracająca informację o tym czy nazwa gracza jest już zajęta
     * @return Informacja o tym czy nazwa gracza jest już zajęta
     */
    public boolean czyPseudonimZajety() {
        return czyPseudonimZajety;
    }
    
    /**
     * Metoda zwracająca informację o tym czy podane hasło dostępu do serwera jest nieprawidłowe 
     * @return Informacja o tym czy podane hasło dostępu do serwera jest nieprawidłowe 
     */
    public boolean czyZleHaslo() {
        return czyZleHaslo;
    }
    
    /**
     * Metoda zwracająca informację o tym czy serwer gry jest już pełny
     * @return  Informacja o tym czy serwer gry jest już pełny
     */
    public boolean czyBrakMiejsc() {
        return czyBrakMiejsc;
    }
}
