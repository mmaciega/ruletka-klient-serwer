package pl.eden.ruletka.game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.gfx.PanelSrodkowy;
import pl.eden.ruletka.game.gfx.fields.Pole;
import pl.eden.ruletka.game.net.packets.Pakiet09NewBet;

/**
 * Klasa reprezentująca menadżera zakładów
 * @author Mateusz Macięga
 */
public class MenadzerZakladow {
    /** Tablica hashująca gdzie kluczem jest instancja gracza, a wartościami jest lista zakładów danego gracza */
    private final Map<GraczMP,List<Zaklad>> zlozoneZaklady;
    /** Lista graczy uczestniczących w rozgrywce */
    private final ListaGraczyModel listaGraczy;
    
    /**
     * Konstruktor sparametryzowany 
     * @param listaGraczy Lista graczy uczestniczących w rozgrywce 
     */
    public MenadzerZakladow(ListaGraczyModel listaGraczy) {
        zlozoneZaklady = new HashMap<>();
        this.listaGraczy = listaGraczy;
    }
    
    /**
     * Metoda odpowiedzialna za usunięcie wszystkich zakładów w aktualnej rozgrywce
     */
    public void usunWszystkieZaklady() {
        zlozoneZaklady.clear();
    }
    
    /**
     * Metoda odpowiedzialna za sprawdzenie wszystkich złożonych zakładów pod kątem tego czy są one zwycięskie
     * @param wylosowanePole Wylosowana liczba
     * @return Tablice hashującą gdzie kluczem jest nazwa gracza, a wartością wartość wygranej danego gracza
     */
    public Map<String,Long[]> sprawdzZaklady(int wylosowanePole)
    {
        Map<String,Long[]> zwyciescyGracze = new HashMap<>();
        
        Iterator iterator = zlozoneZaklady.entrySet().iterator();
	while (iterator.hasNext()) {
		Map.Entry gracz = (Map.Entry) iterator.next();
                
                GraczMP g = ((GraczMP) gracz.getKey());
                long wartoscWygranej = 0L, zwrotZWygranegoPola = 0L;
                
                List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(g);
                Iterator<Zaklad> iteratorZ = zakladyGracza.iterator();
                while (iteratorZ.hasNext()) {
                    Zaklad z = (Zaklad) iteratorZ.next();
                    Pole pole = z.dajPole();
                    if(pole.sprawdzZaklad(wylosowanePole)) {
                        System.out.println("Siadl zaklad " + pole + " wartosc " + z.dajWartoscZakladu());
                        wartoscWygranej += z.dajWartoscZakladu() * pole.dajWartoscMnoznikaWygranej();
                        zwrotZWygranegoPola += z.dajWartoscZakladu();
                    }
                }
                
                Long[] wyniki = new Long[2];
                wyniki[0] = wartoscWygranej;
                wyniki[1] = zwrotZWygranegoPola;
                
                if(wartoscWygranej != 0 ) {
                    zwyciescyGracze.put(g.dajPseudonim(), wyniki);
                }                
                
                System.out.println("Gracz " + g.dajPseudonim() + " wygral " + wartoscWygranej);
	}
        
        return zwyciescyGracze;

    }
    
    /**
     * Metoda zwracająca wartość postawionych zakładów przez danego gracza na konkretnym polu
     * @param gracz Instancja gracza, dla którego sprawdzamy postawione zakłądy
     * @param pole Instancja klasy pole, które sprawdzamy pod kątem postawionych zakładów
     * @return Wartość postawionych zakładów przez danego gracza na konkretnym polu
     */
    public int wartoscZakladow(GraczMP gracz, Pole pole) {
        int indeksGracza = listaGraczy.dajIndeksGraczaMP(gracz.dajPseudonim());
        GraczMP g = (GraczMP)listaGraczy.getElementAt(indeksGracza);
        
        List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(g);  
        for(Zaklad z : zakladyGracza) {
            if(z.dajPole().equals(pole)) {
                return z.dajWartoscZakladu();
            }
        }
        
        return 0;
    }
    
    /**
     * Metoda zwracająca wartość wszystkich postawionych zakładów przez danego gracza
     * @param gracz Instancja gracza, dla którego obliczamy sumę zakładów
     * @return Wartość wszystkich postawionych zakładów przez danego gracza
     */
    public int wartoscZakladow(GraczMP gracz) {
        int indeksGracza = listaGraczy.dajIndeksGraczaMP(gracz.dajPseudonim());
        GraczMP g = (GraczMP)listaGraczy.getElementAt(indeksGracza);
        int wartoscZakladow = 0;
        
        List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(g);  
        if(zakladyGracza != null) {
            for(Zaklad z : zakladyGracza) {
                    wartoscZakladow+= z.dajWartoscZakladu();
            }

            return wartoscZakladow;
            }
        return 0;
    }
    
    /**
     * Metoda odpowiedzialna za dodanie zakładu do listy zakładów
     * @param pakiet Instancja pakietu informującego o dokonaniu nowego zakładu
     */
    public synchronized void dodajZaklad(Pakiet09NewBet pakiet) {
        Zaklad zaklad = new Zaklad(pakiet.dajPole(), pakiet.dajWartoscZetonu());
        
        int indeksGracza = listaGraczy.dajIndeksGraczaMP(pakiet.dajPseudonimGracza());
        GraczMP gracz = (GraczMP)listaGraczy.getElementAt(indeksGracza);
        
        List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(gracz);
        if (zakladyGracza == null) {
                zakladyGracza = new ArrayList();
                this.zlozoneZaklady.put(gracz, zakladyGracza);
                zakladyGracza.add(zaklad);
        } else {
            boolean czyZnaleziono = false;
            
            for (Zaklad z : zakladyGracza) {
                if (z.dajPole().equals(pakiet.dajPole())) {
                    z.dodajWartoscZakladu(zaklad.dajWartoscZakladu());
                    czyZnaleziono = true;
                    break;
                }
            }
            
            if(!czyZnaleziono) {
                zakladyGracza.add(zaklad);
            }
        }
    }
    
    /**
     * Metoda odpowiedzialna za dodanie zakładu do listy zakładów
     * @param pseudonim Nazwa gracza skłądające zakład
     * @param wartoscZetonu Wartość zakładu
     * @param pole Instancja klasy pole, na które złożono zakład
     */
    public synchronized void dodajZaklad(String pseudonim, int wartoscZetonu, Pole pole) {
        Zaklad zaklad = new Zaklad(pole, wartoscZetonu);
        
        int indeksGracza = listaGraczy.dajIndeksGraczaMP(pseudonim);
        GraczMP gracz = (GraczMP)listaGraczy.getElementAt(indeksGracza);
        
        List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(gracz);
        if (zakladyGracza == null) {
                zakladyGracza = new ArrayList();
                this.zlozoneZaklady.put(gracz, zakladyGracza);
                zakladyGracza.add(zaklad);
        } else {
            boolean czyZnaleziono = false;
            
            for (Zaklad z : zakladyGracza) {
                if (z.dajPole().equals(pole)) {
                    z.dodajWartoscZakladu(zaklad.dajWartoscZakladu());
                    czyZnaleziono = true;
                    break;
                }
            }
            
            if(!czyZnaleziono) {
                zakladyGracza.add(zaklad);
            }
        }
    }
    
    /**
     * Metoda odpowiedzialna za usunięcie wszystkich postawionych zakładów przez danego gracza
     * @param gracz Instancja gracza, któremu usuwamy wszystkie zakłady
     */
    public synchronized void usunZakladyGracza(GraczMP gracz) {
        List zakladyGracza = (List) this.zlozoneZaklady.get(gracz);

        if(zakladyGracza != null) {
            while (!zakladyGracza.isEmpty()) { 
                    zakladyGracza.remove(0);
            }
        }
        this.zlozoneZaklady.remove(gracz);
    }
    
    /**
     * Metoda sprawdzjąca czy dane pole jest zajęte przez konkretnego gracza
     * @param gracz Instancja gracza, dla którego sprawdzamy czy zajął pole
     * @param pole Instancja pola, na któym sprawdzamy czy gracza złożył zakład
     * @return Informacja o tym czy pole jest zajęte przez gracza (true, false)
     */
    public boolean czyPoleZajetePrzezGracza(Gracz gracz, Pole pole) {
        List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get((GraczMP) gracz);
        
        if(zakladyGracza != null) {
            for(Zaklad z : zakladyGracza) {
                if (((Zaklad) z).dajPole().equals(pole)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    
    @Override
    public String toString() {
        String wynik = "";
        
        Iterator iterator = zlozoneZaklady.entrySet().iterator();
	while (iterator.hasNext()) {
		Map.Entry gracz = (Map.Entry) iterator.next();
                
                wynik += ((GraczMP) gracz.getKey()).dajPseudonim() + ",";
                
                List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(gracz.getKey());
                
                Iterator<Zaklad> iteratorZ = zakladyGracza.iterator();
                while (iteratorZ.hasNext()) {
                    Zaklad z = (Zaklad) iteratorZ.next();
                    Pole pole = z.dajPole();
                    wynik+=pole.dajTypString() + "." + pole.dajWartoscString() + "." + z.dajWartoscZakladu();
                    
                    if(iteratorZ.hasNext()) {
                        wynik += ";";
                    }
                }
                
                if(iterator.hasNext()) {
                    wynik += ",";
                }
	}
 
        return wynik;
    }
   
    /**
     * Metoda odpowiedzialna za zaaktualizowanie wszystkich pól planszy
     * @param graczLokalny Instancja gracza lokalnego
     */
    public void aktualizujWszystkiePola(Gracz graczLokalny) {
        GraczMP graczMP;
        Iterator iterator = zlozoneZaklady.entrySet().iterator();
	while (iterator.hasNext()) {
            Map.Entry gracz = (Map.Entry) iterator.next();

            graczMP = (GraczMP) gracz.getKey();

            List<Zaklad> zakladyGracza = (List<Zaklad>) this.zlozoneZaklady.get(graczMP);

            if (!graczMP.dajPseudonim().equals(graczLokalny.dajPseudonim())) {

                Iterator<Zaklad> iteratorZ = zakladyGracza.iterator();
                while (iteratorZ.hasNext()) {
                    Zaklad z = (Zaklad) iteratorZ.next();
                    Pole pole = z.dajPole();
                    Pole poleGry = ((PanelSrodkowy) PanelSrodkowy.dajInstancje()).dajPole(pole);

                    if (graczLokalny.dajPseudonim().equals(graczMP.dajPseudonim()) || !czyPoleZajetePrzezGracza(graczLokalny, pole))
                    {
                        poleGry.dodajZeton(graczMP.dajKolorGracza(), graczLokalny.dajPseudonim().equals(graczMP.dajPseudonim()));     
                        
                    } else if (graczLokalny.dajPseudonim().equals(graczMP.dajPseudonim())) {
                        poleGry.setToolTipText("Kwota: " + wartoscZakladow(graczMP, pole) + "$");
                    }
                }
            }
          }
    }
    
}
