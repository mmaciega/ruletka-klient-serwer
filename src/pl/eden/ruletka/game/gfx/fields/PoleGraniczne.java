package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole graniczne w grze
 * @author Mateusz Macięga
 */
public class PoleGraniczne extends Pole {
    /** Wartości liczb granicznych */
    private final int[] pola;
    
    /**
     * Konstruktor domyślny
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param polaK Wartości liczb granicznych
     */
    public PoleGraniczne(String nazwa, int[] polaK) {
        super(nazwa, polaK.length, Pole.TypyPola.POLE_GRANICZNE);
        pola = new int[polaK.length];
        System.arraycopy(polaK, 0, this.pola, 0, polaK.length );
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        for(int p : pola) {
            if (p == wylosowanePole) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metoda zwracająca wartości liczb granicznych
     * @return Tablica wartości liczb granicznych
     */
    public int[] dajPola() {
        return pola;
    }
}
