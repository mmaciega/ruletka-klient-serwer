package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania o rozłączeniu rozgrywki przez jednego z graczy
 * @author Mateusz Macięga
 */
public class Pakiet01Disconnect extends Pakiet {
    /** Nazwa gracza, który opuscił rozgrywkę */
    private final String pseudonimGracza;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt 
     */
    public Pakiet01Disconnect(byte[] dane) {
        super(01);
        String wiadomosc = czytajDane(dane);
        
        this.pseudonimGracza = wiadomosc;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który rozłączyć się
     */
    public Pakiet01Disconnect(Gracz gracz) {
        super(01);
        this.pseudonimGracza = gracz.dajPseudonim();
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza, który rozłączył się, w postaci łancuchu
     */
    public Pakiet01Disconnect(String pseudonimGracza) {
        super(01);
        this.pseudonimGracza = pseudonimGracza;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu informującego o rozłączeniu gracza 
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet01Disconnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu informującego o rozłączeniu gracza 
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet01Disconnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("01" + pseudonimGracza + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza
     * @return Nazwa gracza
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
}
