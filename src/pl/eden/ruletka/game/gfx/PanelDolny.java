package pl.eden.ruletka.game.gfx;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.logic.ListaGraczyModel;
import pl.eden.ruletka.game.net.ObslugaPolecen;
import pl.eden.ruletka.game.net.packets.Pakiet;
import pl.eden.ruletka.game.net.packets.Pakiet03Message;
import pl.eden.ruletka.game.net.packets.Pakiet10ResetBets;
import pl.eden.ruletka.game.net.packets.Pakiet11FinishedBets;

/**
 * Klasa reprezentująca dolny panel z przyciskami i czatem.
 * Klasa ma zaimplementowany wzorzec projektowy Singleton.
 * @author Mateusz Macięga
 */
public class PanelDolny extends JPanel {
    /** Tło panelu */
    private Image tloObrazek;
    /** Panel z czatem */
    private final JPanel panelZCzatem = new PanelZCzatem();
    /** Panel z dwoma przyciskami - SPIN i RESET */
    private final JPanel panelZPrzyciskami = new PanelZPrzyciskami();

    /**
     * Konstruktor prywatny. Zastosowano wzorzec Singleton
     */
    private PanelDolny() {
        try 
        {
            tloObrazek = ImageIO.read(getClass().getResourceAsStream("/pasek_dol.png"));
        } 
        catch (IOException ex) { System.err.printf("Wystapil wyjatek " + ex.getMessage());}
        
        setPreferredSize(new Dimension(tloObrazek.getWidth(null), tloObrazek.getHeight(null)));
        setOpaque(false);
        setLayout(new BorderLayout(5,0));
        setBorder(new EmptyBorder(5, 8, 5, 8));
        
        setPreferredSize(new Dimension(120, 100));
 
        add(panelZCzatem, BorderLayout.CENTER);
        add(panelZPrzyciskami, BorderLayout.LINE_END);
    }
    
    /**
     * Metoda zwracająca instancję klasy
     * @return Instancja klasy PanelDolny
     */
    public static PanelDolny dajInstancje() {
        return PanelDolnyInstancja.INSTANCJA;
    }
    
    /**
     * Metoda statyczna tworząca instację klasy
     */
    private static class PanelDolnyInstancja {
        private static final PanelDolny INSTANCJA = new PanelDolny();
    }
    
    /**
     * Metoda ustawiająca listę graczy uczestniczących w rozgrywce
     * @param listaGraczy Instancja listy graczy
     */
    public void ustawListeCzatu(ListaGraczyModel listaGraczy) {
        ((PanelZCzatem) panelZCzatem).listGraczyCzat.setModel(listaGraczy);
    }
    
    /**
     * Metoda wprowadzająca komunikat do czatu
     * @param trescKomunikatu Treść komunikatu
     */
    public void wprowadzKomunikat(String trescKomunikatu) {
        ((PanelZCzatem) panelZCzatem).wprowadzKomunikat(trescKomunikatu);
    }
    
    /**
     * Metoda wprowadzająca wiadomość od gracza do czatu
     * @param odGracza Gracz, od którego przyszła wiadomość
     * @param wiadomosc Treść wiadomości
     */
    public void wprowadzKomunikat(Gracz odGracza, String wiadomosc) {
        ((PanelZCzatem) panelZCzatem).wprowadzKomunikat(odGracza, wiadomosc);
    }
    
    /**
     * Metoda wprowadzająca wiadomość od gracza do czatu
     * @param pakiet Pakiet otrzymany od serwera zawierający autora oraz treść wiadomości
     */
    public void wprowadzKomunikat(Pakiet03Message pakiet) {
        ((PanelZCzatem) panelZCzatem).wprowadzKomunikat(pakiet);
    }
    
    /**
     * Metoda zwracająca referencję do pola tekstowego czatu
     * @return Pole tekstowe czatu
     */
    public JTextField dajPoleTekstoweCzatu () {
        return ((PanelZCzatem) panelZCzatem).dajPoleTekstoweCzatu();
    }
    
    /**
     * Klasa reprezentująca panel z czatem gry
     */
    final class PanelZCzatem extends JPanel {
        /** Pole główne czatu wyświetlajace wiadomości od graczy i komunikaty serwerowe */
        private final JTextPane zawartoscCzatu = new JTextPane();
        /** Pole służące do wprowadzania wiadomości i poleceń serwerowych */
        private final JTextField poleTekstoweCzatu = new JTextField();
        /** Lista graczy uczestniczących w rozgrywce */
        private JList listGraczyCzat;
        
        /**
         * Konstruktor domyślny klasy
         */
        public PanelZCzatem() {
            setLayout(new BorderLayout());
            setOpaque(false);
            
            /////////////////////////////////////////////
            zawartoscCzatu.setContentType("text/plane"); // or any other styled content type
            zawartoscCzatu.setEditable(false);
            zawartoscCzatu.setOpaque(false);
            zawartoscCzatu.setForeground(Color.white);
            zawartoscCzatu.setBackground(new Color(0,0,0,0));
            zawartoscCzatu.setBorder(BorderFactory.createEmptyBorder());
            
            this.wprowadzKomunikat("Witaj w grze w ruletkę");
            
            JScrollPane przewijanyPanelZCzatem = new JScrollPane(zawartoscCzatu);
            przewijanyPanelZCzatem.setOpaque(false);
            przewijanyPanelZCzatem.setBorder(BorderFactory.createMatteBorder(2,2,1,2, Color.gray));
            przewijanyPanelZCzatem.setViewportView(zawartoscCzatu);
            przewijanyPanelZCzatem.getViewport().setOpaque(false);
            
           ///////////////////////////////////////////////////
                      
            poleTekstoweCzatu.setOpaque(false);
            poleTekstoweCzatu.setForeground(Color.white);
            poleTekstoweCzatu.setSelectedTextColor(Color.white);
            poleTekstoweCzatu.setCaretColor(Color.WHITE); 
            poleTekstoweCzatu.setSelectionColor(new Color (51,153,255));
            poleTekstoweCzatu.setBorder(BorderFactory.createMatteBorder(1,2,2,2, Color.gray));
            poleTekstoweCzatu.addActionListener(new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        wprowadzTekst(evt);
                    }
            });
   
            listGraczyCzat = new JList();
            listGraczyCzat.setForeground(Color.white);
            listGraczyCzat.setOpaque(false);
            listGraczyCzat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            listGraczyCzat.setLayoutOrientation(JList.VERTICAL);
            
            listGraczyCzat.setBorder(new EmptyBorder(0,5,0,5));
            listGraczyCzat.setCellRenderer(new ListaGraczyRenderWiersza());
            
            ((javax.swing.DefaultListCellRenderer)listGraczyCzat.getCellRenderer()).setOpaque(false); 

            JScrollPane listScroller = new JScrollPane(listGraczyCzat);
            listScroller.setViewportView(listGraczyCzat);
            listScroller.getViewport().setOpaque(false);
            listScroller.setOpaque(false);
            listScroller.setBorder(BorderFactory.createMatteBorder(2,0,1,2, Color.gray));
            
            add(przewijanyPanelZCzatem, BorderLayout.CENTER);
            add(listScroller, BorderLayout.LINE_END);
            add(poleTekstoweCzatu, BorderLayout.SOUTH);
        }
        
        /**
         * Klasa reprezentująca sposób renderowania wierszy w liście graczy
         */
        class ListaGraczyRenderWiersza extends DefaultListCellRenderer {
            /** Obrazek informujący o zakończeniu składania zakładów */
            Icon obrazekZakonczeniaZakladow;
            
            /**
             * Konstruktor domyślny klasy
             */
            public ListaGraczyRenderWiersza() {
                try {
                    //icona ze strony https://www.iconfinder.com/search/?q=iconset%3Asimplicio
                    obrazekZakonczeniaZakladow = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/gotowy2.png")));
                } catch (IOException ex) {
                        Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
            
            @Override
            public Component getListCellRendererComponent(JList list,Object value, int index, boolean selected, boolean expanded) {
                Component c = super.getListCellRendererComponent(list, value, index, false, false);

                if (value instanceof Gracz) {
                    Gracz gracz = (Gracz) value;
                    setText(gracz.dajPseudonim() + " - $" + gracz.dajPieniadze());
                    
                    setForeground(gracz.dajKolorGracza());
                    if(gracz.czyZakonczylZaklady()) {
                        setIcon(obrazekZakonczeniaZakladow);
                    } else {
                        setIcon(null);
                    }
                    setHorizontalTextPosition(JLabel.LEFT);
                }
               return c; 
            }      
        }
        
        /**
         * Metoda odpowiedzialna za wyświetlanie komunikatów serwerowych w oknie głównym czatu
         * @param trescKomunikatu Treść komunikatu serwera, który ma zostać wyświetlony
         */
        public void wprowadzKomunikat(String trescKomunikatu) {
            StyledDocument document = zawartoscCzatu.getStyledDocument();
            SimpleAttributeSet attributes = new SimpleAttributeSet();
            StyleConstants.setForeground(attributes, Color.white);
            
            trescKomunikatu =  "***** " + trescKomunikatu + " *****";
            
            try {
                document.insertString(document.getLength(),document.getLength() == 0 ?  trescKomunikatu : "\n" + trescKomunikatu, attributes);
            } catch (BadLocationException ex) {
                Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            zawartoscCzatu.setCaretPosition(zawartoscCzatu.getDocument().getLength());
            zawartoscCzatu.setDocument(document);
        }
        
        /**
         * Metoda odpowiedzialna za wyświetlenie wiadomości od gracza
         * @param odGracza Gracza, który przesyła wiadomość
         * @param wiadomosc Treść wiadomości
         */
        public void wprowadzKomunikat(Gracz odGracza, String wiadomosc) {
            StyledDocument document = zawartoscCzatu.getStyledDocument();
                    
            SimpleAttributeSet attributes = new SimpleAttributeSet();
            StyleConstants.setBold(attributes, true);
            StyleConstants.setForeground(attributes, odGracza.dajKolorGracza());
            try {
                document.insertString(document.getLength(),document.getLength() == 0 ?  odGracza.dajPseudonim() : "\n" + odGracza.dajPseudonim(), attributes);
            } catch (BadLocationException ex) {
                Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                wiadomosc = ": " + wiadomosc;
                document.insertString(document.getLength(), wiadomosc, null);
            } catch (BadLocationException ex) {
                Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            zawartoscCzatu.setCaretPosition(zawartoscCzatu.getDocument().getLength());
            zawartoscCzatu.setDocument(document);
        }
        
        /**
         * Metoda odpowiedzialna za wyświetlenie wiadomości od gracza na podstawie otrzymanego pakietu Pakiet03Message
         * @param pakiet Pakiet zawierający instancję gracza, który jest autorem oraz treść wiadomości
         */
        public void wprowadzKomunikat(Pakiet03Message pakiet) {
            Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
            int indeksGracza = gra.dajIndeksGraczaMP( ((Pakiet03Message) pakiet).dajPseudonimGracza());
            GraczMP gracz = (GraczMP) gra.dajListeGraczy().getElementAt(indeksGracza);
            
            StyledDocument document = zawartoscCzatu.getStyledDocument();
                    
            SimpleAttributeSet attributes = new SimpleAttributeSet();
            StyleConstants.setBold(attributes, true);
            StyleConstants.setForeground(attributes, gracz.dajKolorGracza());
            try {
                document.insertString(document.getLength(),document.getLength() == 0 ?  gracz.dajPseudonim() : "\n" + gracz.dajPseudonim(), attributes);
            } catch (BadLocationException ex) {
                Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                String wiadomosc = ": " + ((Pakiet03Message) pakiet).dajTresc();
                document.insertString(document.getLength(), wiadomosc, null);
            } catch (BadLocationException ex) {
                Logger.getLogger(PanelDolny.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            zawartoscCzatu.setCaretPosition(zawartoscCzatu.getDocument().getLength());
            zawartoscCzatu.setDocument(document);
        }
         
        /**
         * Metoda odpowiedzialna za obsługę zdarzenia wprowadzanie jakiejś treści do czatu
         * @param evt 
         */
        private void wprowadzTekst(ActionEvent evt) {
            String tresc = ((JTextField) evt.getSource()).getText().trim();

            if (tresc.length() != 0) {
                ((JTextField) evt.getSource()).setText("");

                Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));

                if(!tresc.contains("/")) {
                    Pakiet03Message pakiet = new Pakiet03Message(gra.dajGracza(), tresc);
                    pakiet.piszDane(gra.dajGniazdoKlienta());
                } else {
                    if(gra.dajSerwerGry() == null) {
                        wprowadzKomunikat("Nie masz uprawnień do konfigurowania serwerem");
                    } else {
                        ObslugaPolecen.obsluzPolecenie(gra, tresc);
                    }
                }
            }
        }
        
        /**
         * Metoda zwracająca pole tekstowe czatu
         * @return Pole tekstowe czatu
         */
        public JTextField dajPoleTekstoweCzatu() {
            return poleTekstoweCzatu;
        }
    }
    
    /**
     * Klasa reprezentująca panel z dwoma przyciskami (SPIN, RESET)
     */
    class PanelZPrzyciskami extends JPanel implements MouseListener {
        /** Przycisk SPIN */
        private final JLabel przyciskSpin;
        /** Przycisk RESET */
        private final JLabel przyciskReset;
        
        /**
         * Konstruktor domyślny
         */
        public PanelZPrzyciskami() {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setOpaque(false);
            
            przyciskSpin = new ElementGraficzny("spin_button_150.png");
            przyciskSpin.setAlignmentX(Component.CENTER_ALIGNMENT);
            przyciskSpin.addMouseMotionListener(ZmianaKursora.dajInstancje());
            przyciskSpin.addMouseListener(this);
            
            przyciskReset = new ElementGraficzny("reset_button.png");
            przyciskReset.setAlignmentX(Component.CENTER_ALIGNMENT);
            przyciskReset.addMouseMotionListener(ZmianaKursora.dajInstancje());
            przyciskReset.addMouseListener(this);

            add(przyciskSpin);
            add(Box.createRigidArea(new Dimension(0,10)));
            add(przyciskReset);
        }

        /**
         * Metoda obsługująca zdarzenie kliknięcia w przycisk
         * @param me 
         */
        @Override
        public void mouseClicked(MouseEvent me) {
            Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
            
            if(!gra.czyFlagaWait()) {
                JLabel przycisk = (JLabel) me.getSource();

                Pakiet pakiet = null;
                if(przycisk == przyciskReset) {
                    pakiet = new Pakiet10ResetBets(gra.dajGracza());
                    pakiet.piszDane(gra.dajGniazdoKlienta());
                } else if (przycisk == przyciskSpin) {
                    pakiet = new Pakiet11FinishedBets(gra.dajGracza());
                    pakiet.piszDane(gra.dajGniazdoKlienta());
                }
            }
        }

//<editor-fold defaultstate="collapsed" desc="inne nieobsłużone zdarzenia myszki">
        @Override
        public void mousePressed(MouseEvent me) {
        }
        
        @Override
        public void mouseReleased(MouseEvent me) {
        }
        
        @Override
        public void mouseEntered(MouseEvent me) {
        }
        
        @Override
        public void mouseExited(MouseEvent me) {
        }
//</editor-fold>
    }
}
