package pl.eden.ruletka.game.gfx;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import pl.eden.ruletka.game.Gra;

/**
 * Klasa reprezentująca pieniądze gracza
 * @author Mateusz Macięga
 */
public class PieniadzeGracza extends ElementGraficzny {
    /** Czcionka w jakiej zostaną wyświetlone pieniądze gracza */
    private Font czcionka = new Font("Times New Roman", Font.BOLD, 19);
    /** Ustawiamy margines w px jaki chcemy mieć po między ramką a kwota w ramce */
    private final int marginesZPrawej = 10;
   
    /**
     * Konstruktor domyślny
     */
    public PieniadzeGracza() {
        super("belka_cash.png");
        setForeground(Color.black);
    }
    
    /**
     * Metoda zarządzają pieniedzmi gracza (zmiana rozmiaru czcionki, żeby wartość pieniędzy gracza mieśćiła się w ramce)
     * @param g 
     */   
    @Override
    public void paintComponent(Graphics g) 
    { 
        super.paintComponent(g);
        Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
        String liczbaPieniedzy = gra.dajGracza().dajPieniadze() + "$";
        
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        FontMetrics metrykaCzcionki = getFontMetrics(czcionka);
        int dlugoscLancucha = metrykaCzcionki.stringWidth(liczbaPieniedzy);    
        while(dlugoscLancucha > (90-marginesZPrawej))
        {
            czcionka = czcionka.deriveFont((float) czcionka.getSize() - 1);

            metrykaCzcionki = getFontMetrics(czcionka);
            dlugoscLancucha = metrykaCzcionki.stringWidth(liczbaPieniedzy);
        }

        setFont(czcionka);
        g.drawString(liczbaPieniedzy, (getWidth()-marginesZPrawej) - dlugoscLancucha, (int) (getHeight()/2)  + (metrykaCzcionki.getHeight()/3));
    }
    
}
