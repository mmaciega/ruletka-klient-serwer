package pl.eden.ruletka.game.entities;

import java.awt.Color;
import java.net.InetAddress;

/**
 * Klasa reprezentująca gracza, który połączył się z serwerem
 * @author Mateusz Macięga
 */
public class GraczMP extends Gracz{
    /** Port, z którego gracz się połączył */
    private int port;
    /** Adres gracza */
    private InetAddress ipAdres;
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Pieniądze gracza
     * @param pseudonim Nazwa gracza
     * @param kolorGracza Kolor nazwy gracza
     * @param ipAdres Adres IP gracza
     * @param port Port gracza
     * @param czyZakonczylZaklady Informacja o tym czy zakonczył zakłady
     */
    public GraczMP(long pieniadzeStartowe, String pseudonim, Color kolorGracza, InetAddress ipAdres, int port, boolean czyZakonczylZaklady) {
        super(pieniadzeStartowe, pseudonim, kolorGracza, czyZakonczylZaklady);
        this.ipAdres = ipAdres;
        this.port = port;
    } 
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Pieniądze gracza
     * @param pseudonim Nazwa gracza
     * @param kolorGracza Kolor nazwy gracza
     * @param ipAdres Adres IP gracza
     * @param port Port gracza
     */
    public GraczMP(long pieniadzeStartowe, String pseudonim, Color kolorGracza, InetAddress ipAdres, int port) {
        super(pieniadzeStartowe, pseudonim, kolorGracza);
        this.ipAdres = ipAdres;
        this.port = port;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Pieniądze gracza
     * @param pseudonim Nazwa gracza
     * @param ipAdres Adres IP gracza
     * @param port Port gracza
     */
    public GraczMP(long pieniadzeStartowe, String pseudonim, InetAddress ipAdres, int port) {
        super(pieniadzeStartowe, pseudonim);
        this.ipAdres = ipAdres;
        this.port = port;
    }
    
    /**
     * Metoda zwracająca adres IP gracza
     * @return Adres IP gracza
     */
    public InetAddress dajAdres() {
        return ipAdres;
    }
    
    /**
     * Metoda ustawiająca adres IP gracza
     * @param ipAdres Nowy adres IP gracza
     */
    public void ustawAdres(InetAddress ipAdres) {
        this.ipAdres = ipAdres;
    }
    
    /**
     * Metoda zwracająca port gracza
     * @return Numer portu gracza
     */
    public int dajPort() {
        return port;
    }
    
    /**
     * Metoda ustawiająca port gracza
     * @param port Nowy port gracza
     */
    public void ustawPort(int port) {
        this.port = port;
    }
}
