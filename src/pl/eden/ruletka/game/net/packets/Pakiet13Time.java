package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o pozostalym czasie przeznaczonym na ruch (pakiet przesyłany jest automatycznie co 1 sekundę)
 * @author Mateusz Macięga
 */
public class Pakiet13Time extends Pakiet {
    /** Pozostały czas na ruch */
    private final long pozostalyCzasNaRuch;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet13Time(byte[] dane) {
        super(13);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pozostalyCzasNaRuch = Long.parseLong(wiadomosc[0]);
    }

    /**
     * Konstruktor sparametryzowany 
     * @param pozostalyCzasNaRuch Pozostały czas na ruch
     */
    public Pakiet13Time(long pozostalyCzasNaRuch) {
        super(13);
        this.pozostalyCzasNaRuch = pozostalyCzasNaRuch;
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o pozostalym czasie przeznaczonym na ruch 
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet13Time.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o pozostalym czasie przeznaczonym na ruch 
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet13Time.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("13" + pozostalyCzasNaRuch +  "K").getBytes();
    }
    
    /**
     * Metoda zwracająca pozostały czas na ruch
     * @return Pozostały czas na ruch
     */
    public long dajPozostalyCzasNaRuch() {
        return pozostalyCzasNaRuch;
    }
}
