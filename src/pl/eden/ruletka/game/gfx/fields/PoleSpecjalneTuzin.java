package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne typu tuzin
 * @author Mateusz Macięga
 */
public class PoleSpecjalneTuzin extends PoleSpecjalne {
    /** Typ wyliczeniowy dla pola specjalnego Tuzin */
    public static enum Tuziny {
        PIERWSZY_TUZIN, DRUGI_TUZIN, TRZECI_TUZIN;
    }
    
    /** Które pole specjalne typu Tuzin */
    private final Tuziny tuzin;

    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param tuzin Które pole specjalne typu Tuzin
     */
    public PoleSpecjalneTuzin(String nazwa, Tuziny tuzin) {
        super(nazwa, Pole.MnoznikiPola.DOZEN_BET, PoleSpecjalne.TypyPolaSpecjalnego.TUZIN);
        this.tuzin = tuzin;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        switch(tuzin)
        {
            case PIERWSZY_TUZIN:
                return wylosowanePole >= 0 && wylosowanePole <13;
            case DRUGI_TUZIN:
                return wylosowanePole >= 13 && wylosowanePole <25;
            case TRZECI_TUZIN:
                return wylosowanePole >= 25 && wylosowanePole <37;
            default:
                return false;
        }
    }
    
    /**
     * Metoda zwracająca typ wyliczeniowy dla pola specjalnego Tuzin
     * @return Typ wyliczeniowy dla pola specjalnego Tuzin
     */
    public Tuziny dajTuzin() {
        return tuzin;
    }
}
