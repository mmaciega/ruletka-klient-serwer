package pl.eden.ruletka.game.net;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.gfx.PanelDolny;
import pl.eden.ruletka.game.net.packets.Pakiet05KickPlayer;
import pl.eden.ruletka.game.net.packets.Pakiet06BanPlayer;
import pl.eden.ruletka.game.net.packets.Pakiet08Restart;

/**
 * Klasa odpowiedzialna za obsługę polece serwerowych  
 * @author Mateusz Macięga
 */
public class ObslugaPolecen {
    /**
     * Zdefiniowane polecenia możliwe do wykonania przez serwera. 
     */
    private enum TypyPolecen {
        INFO("/info", "Wyświetla listę wszystkich dostępnych poleceń"), 
        KICK("/kick nazwaGracza [powód]","Usuwa gracza o podanej nazwie z serwera (opcjonalnie można przekazać graczowi inforację dlaczego)."),
        BAN("/ban nazwaGracza [powód]", "Banuje adres IPgracza o podanej nazwie z serwera (opcjonalnie można przekazać graczowi inforację dlaczego)."),
        BANLIST("/banlist", "Wyświetla listę zbanowanych adresów IP z nazwą gracza zbanowanego."),
        UNBAN("/unban adresIP|nazwaGracza", "Usuwa adres IP z listy adresów zbanowanych. Możemy podać nazwę gracza, którego zbanowaliśmy lub adres IP."),
        STARTMONEY("/startmoney [nowaWartoscStartowa]","Wyświetla lub zmienia aktualną wartość pieniężną startową."),
        CHANGEMODE("/changemode [public|private]","Wyświetla lub ustawia tryb rozgrywki. Możliwe tryby to tryb publiczny oraz tryb prywatny. Tryb prywatny oznacza, że żeby się dostać na serwer należy znać hasło."),
        RESTART("/restart","Powoduje zresetowanie serwera gry. Wszyscy gracze rozpoczną nową gre z ustaloną kwotą początkową."),
        MAXCLIENTS("/maxclients [maksLiczbaKlientow]","Wyświetla lub zmienia maksymalną liczbę klientów."),
        BETTIME("/bettime [czasNaRuch]","Wyświetla lub zmienia czas na dokonanie zakładów. Wartość podaj w sekundach.");

        /** Łancuch z poprawną budową polecenia */
        private final String budowaPolecenia;
        /** Łancuch z opisem polecenia */
        private final String opisPolecenia;
        
        /**
         * Konstruktor sparametryzowany, prywatny
         * @param budowaPolecenia Łancuch z poprawną budową polecenia
         * @param opisPolecenia Łancuch z opisem polecenia
         */
        private TypyPolecen(String budowaPolecenia, String opisPolecenia) {
            this.budowaPolecenia = budowaPolecenia;
            this.opisPolecenia = opisPolecenia;
        }
        
        /**
         * Metoda zwracająca łancuch z poprawną budową polecenia
         * @return Łancuch z poprawną budową polecenia
         */
        public String dajBudowePolecenia() {
            return budowaPolecenia;
        }
        
        /**
         * Metoda zwracająca łancuch z opisem polecenia
         * @return Łancuch z opisem polecenia
         */
        public String dajOpisPolecenia() {
            return opisPolecenia;
        }
    }
    
    /**
     * Główna metoda statyczna służąca do obsługi poleceń serwerowych
     * @param gra Gra dla której dokonujemy zmian w konfiguracji serwera.
     * @param tresc Treść wpisanego polecenia. Treść tą będziemy parsować.
     */
    public static void obsluzPolecenie(Gra gra, String tresc) {
        ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat(gra.dajGracza(), tresc);
        tresc = tresc.substring(1);
        String[] trescTablica = tresc.split(" ");
        
        ObslugaPolecen.TypyPolecen typPolecenia;
        try {
            typPolecenia = ObslugaPolecen.TypyPolecen.valueOf(trescTablica[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nie ma takiego polecenia: " + trescTablica[0]);
            return;
        }

        //podejmowanie różnych działań w zależności od typu polecenia
        switch(typPolecenia) {
            case INFO:
                wyswietlPolecenia();
                break;
            case KICK:
                wyrzucGracza(trescTablica, gra);  
                break;
            case BAN:
                zbanujGracza(trescTablica, gra);
                break;
            case BANLIST:
                wyswietlListeZbanowanychIP(gra);
                break;
            case UNBAN:
                odbanujGracza(trescTablica, gra);
                break; 
            case STARTMONEY:
                ustawKwotePoczatkowa(trescTablica, gra);
                break;
            case CHANGEMODE:
                zmianaTrybu(trescTablica, gra);
                break;
            case RESTART:
                restartSerwera(trescTablica, gra);
                break;
            case MAXCLIENTS:
                ustawMaksKlientow(trescTablica, gra);
                break;
            case BETTIME:
                ustawCzasNaRuch(trescTablica, gra);
                break;
            default:
                break;
        } 
    }
    
    /**
     * Procedura wyświetlająca wszystkie możliwe polecenia wraz z prawidłowym formatem oraz opisem
     */
    private static void wyswietlPolecenia() {
                String wiadomosc = "Dostepne polecenia:\n";
                for(ObslugaPolecen.TypyPolecen p : ObslugaPolecen.TypyPolecen.values()){
                        wiadomosc += "- " + p + ", format: " + p.dajBudowePolecenia() + "\n  opis: " + p.dajOpisPolecenia() + "\n";
                }
                wiadomosc = wiadomosc.substring(0, wiadomosc.lastIndexOf("\n"));
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat(wiadomosc);    
    }
    
    
    /**
     * Procedura za pomocą, której usuwamy gracza z serwera
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, w której będziemy usuwać gracza
     */
    private static void wyrzucGracza(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.KICK.dajBudowePolecenia());
            return;
        }

        GraczMP gracz;
        try {
        gracz = gra.dajSerwerGry().dajPolaczeniGracze().get(gra.dajSerwerGry().dajIndeksGraczaMP(trescTablica[1]));
        } catch (IndexOutOfBoundsException e) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nie istnieje gracz o nazwie: " + trescTablica[1]);
            return;
        }

        if(gra.dajGracza().dajPseudonim().equals(gracz.dajPseudonim())) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nie można wyrzucić gracza, który jest serwerem");
            return;
        }

        String powodWyrzucenia = "";
        for(int i = 2; i < trescTablica.length; i++) {
            powodWyrzucenia += trescTablica[i] + " ";
        }
        
        Pakiet05KickPlayer pakiet;
        if(trescTablica.length >= 3) {
            pakiet = new Pakiet05KickPlayer(gracz, powodWyrzucenia.trim());
        } else {
            pakiet = new Pakiet05KickPlayer(gracz);
        }
        try {
            gra.dajSerwerGry().usunPolaczenie(pakiet);
        } catch (IOException ex) {
            Logger.getLogger(SerwerGry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Procedura za pomocą, której banujemy gracza
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, w której będziemy banować gracza
     */
    private static void zbanujGracza(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.BAN.dajBudowePolecenia());
            return;
        }
        
        GraczMP gracz;
        try {
        gracz = gra.dajSerwerGry().dajPolaczeniGracze().get(gra.dajIndeksGraczaMP(trescTablica[1]));
        } catch (IndexOutOfBoundsException e) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nie istnieje gracz o nazwie: " + trescTablica[1]);
            return;
        }
        
        if(gra.dajGracza().dajPseudonim().equals(gracz.dajPseudonim())) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nie można zbanować gracza, który jest serwerem");
            return;
        }
        
        String powodZbanowania = "";
        for(int i = 2; i < trescTablica.length; i++) {
            powodZbanowania += trescTablica[i] + " ";
        }
        
        Pakiet06BanPlayer pakiet;
        if(trescTablica.length >= 3) {
            pakiet = new Pakiet06BanPlayer(gracz, powodZbanowania.trim());
        } else {
            pakiet = new Pakiet06BanPlayer(gracz);
        }
        try {
            gra.dajSerwerGry().zbanujPolaczanie(pakiet);
        } catch (IOException ex) {
            Logger.getLogger(SerwerGry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Procedura za pomocą, której wyświetlamy listę zbanowanych adresów IP wraz z nazwą użytkownika.
     * @param gra Gra, z której będziemy wyświetlać listę banów
     */
    private static void wyswietlListeZbanowanychIP(Gra gra) {
        String wiadomosc;
        if(((List) gra.dajSerwerGry().dajZbanowaniGracze()).isEmpty()) {
            wiadomosc = "Brak zbanowanych adresów IP";
        } else {
            wiadomosc = "Zbanowane adresy IP:\n";
            for(GraczMP g : gra.dajSerwerGry().dajZbanowaniGracze()){
                wiadomosc += "- " + g.dajAdres().getHostName() + " (nazwa gracza: " + g.dajPseudonim() + ")\n";
            }
            wiadomosc = wiadomosc.substring(0, wiadomosc.lastIndexOf("\n"));
        }

        ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat(wiadomosc);
    }
    
    /**
     * Procedura za pomocą, której odbanowujemy gracza
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, w której będziemy odbanowywać gracza
     */
    private static void odbanujGracza(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length == 1 || trescTablica.length >2) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.UNBAN.dajBudowePolecenia());
            return;
        }
        
        boolean czyAdresZbanowany = gra.dajSerwerGry().czyAdresZbanowany(trescTablica[1]);
        if(czyAdresZbanowany) {
            int indeksGraczaZbanowanego = gra.dajSerwerGry().dajIndeksGraczaZbanowanego(trescTablica[1]);
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Odbanowano adres IP: " + gra.dajSerwerGry().dajZbanowaniGracze().get(indeksGraczaZbanowanego).dajAdres().getHostName());
            gra.dajSerwerGry().dajZbanowaniGracze().remove(indeksGraczaZbanowanego);
        } else {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Gracz lub adres IP nie zostali zbanowani");
        }
    }
    
    /**
     * Procedura za pomocą, której ustawiamy lub wyświetlamy kwotę początkową gracz. Zmiana kwoty przeprowadzana jest po zrestartowaniu serweru poleceniem /restart.
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, dla której będziemy ustawiać wartość początkową lub wyświetlać aktualną wartość.
     */
    private static void ustawKwotePoczatkowa(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length > 2) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.STARTMONEY.dajBudowePolecenia());
            return;
        }
        
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Obecna kwota startowa wynosi: " + gra.dajSerwerGry().dajPieniadzeStartowe() + "$");
        } else {
            try {
                int nowaWartoscStartowa = Integer.parseInt(trescTablica[1]);

                if(nowaWartoscStartowa <= 0) {
                    ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nowa kwota początkowa musi być wartością dodatnią"); 
                } else {
                    gra.dajSerwerGry().ustawTymczasPieniadzeStartowe(nowaWartoscStartowa);
                    ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Ustawiono nową kwotę startową na " + nowaWartoscStartowa + 
                            "$. Aby wprowadzić zmiany zresetuj serwer poleceniem /restart.");
                }
            } catch (NumberFormatException e) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nowa kwota startowa musi być liczbą");
            }
        }
    }
    
    /**
     * Procedura za pomocą, której wyświetlamy lub zmieniamy tryb gry. Możliwe tryby gry to public i private. W przypadku wybrania trybu private należy ustawić hasło dostępu do serwera.
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, dla której będziemy zmieniać tryb
     */
    private static void zmianaTrybu(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length > 2) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.CHANGEMODE.dajBudowePolecenia());
                return;
            }
        
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Aktualny tryb serwera to tryb " + (gra.dajSerwerGry().czyTrybPrywatny() ? "prywatny" : "publiczny"));
        }
        else {
            if(!trescTablica[1].equals("public") && !trescTablica[1].equals("private")) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Dostępne są dwa tryby: public, private.");
                return;
            }
            
            if(trescTablica[1].equals("public")) {
                gra.dajSerwerGry().ustawCzyTrybPrywatny(false);
                gra.dajSerwerGry().ustawHasloSerwera("");
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Zmieniono tryb serwera na tryb publiczy.");
            } else {
                JPanel panel = new JPanel();
                JLabel label = new JLabel("Wprowadz hasło:");
                JPasswordField pass = new JPasswordField(10);
                panel.add(label);
                panel.add(pass);
                String[] opcje = new String[]{"OK", "Cancel"};
                int opcja = JOptionPane.showOptionDialog(null, panel, "Ustawianie hasła dla trybu prywatnego",
                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                         null, opcje, opcje[1]);
                
                if(opcja == 0) {
                    gra.dajSerwerGry().ustawCzyTrybPrywatny(true);
                    gra.dajSerwerGry().ustawHasloSerwera(new String(pass.getPassword()));
                    ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Zmieniono tryb serwera na tryb prywatny.");
                }

            }
        } 
    }
    
    /**
     * Procedura za pomocą, której restartujemy serwer. Każdy gracz rozpoczyna grę od nowa z wartością startową pieniędzy.
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, którą bedziemy restartować
     */
    private static void restartSerwera(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length > 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.RESTART.dajBudowePolecenia());
            return;
        }
        
        gra.dajSerwerGry().ustawPieniadzeStartowe(gra.dajSerwerGry().dajTymczasPieniadzeStartowe());
        gra.dajSerwerGry().ustawCzasNaRuch(gra.dajSerwerGry().dajTymczasCzasNaRuch());
        
        for(Gracz g : gra.dajSerwerGry().dajPolaczeniGracze()) {
            g.ustawPieniadzeStartowe(gra.dajSerwerGry().dajPieniadzeStartowe());
        }
        
        Pakiet08Restart pakiet = new Pakiet08Restart(gra.dajSerwerGry().dajPieniadzeStartowe(), gra.dajSerwerGry().dajCzasNaRuch());
        pakiet.piszDane(gra.dajSerwerGry());
        
        gra.dajSerwerGry().dajMenadzerRozgrywki().ustawNowyCzas(gra.dajSerwerGry().dajCzasNaRuch());
    }
    
    /**
     * Procedura za pomocą, której wyświetlamy lub ustalamy maksymalną liczbę klientów. Wartość 0 oznacza liczbę nieograniczoną.
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, dla której będziemy zmieniać maksymalną liczbę klientów
     */
    private static void ustawMaksKlientow(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length > 2) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.MAXCLIENTS.dajBudowePolecenia());
            return;
        }
        
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Aktualna maksymalna liczba klientów " + (gra.dajSerwerGry().dajMaksKlientow() > 0 ? "wynosi " + gra.dajSerwerGry().dajMaksKlientow() : "jest nieograniczowa."));
        } else {
            try {
            int nowaMaksLiczbaKlientow = Integer.parseInt(trescTablica[1]);
            
            if(nowaMaksLiczbaKlientow < 0) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nowa maksymalna liczba klientów musi być wartością dodatnią lub 0 (liczba nieograniczowa)"); 
            } else {
                gra.dajSerwerGry().ustawMaksKlientow(nowaMaksLiczbaKlientow);
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Ustawiono maksymalną liczbę klientów na " + (nowaMaksLiczbaKlientow == 0 ? "nieograniczoną" : nowaMaksLiczbaKlientow));
            }
            } catch (NumberFormatException e) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Maksymalna liczba klientów musi być liczbą");
            }
        }
    }
    
    /**
     * Procedura za pomocą, której wyświetlamy lub ustalamy nowy czas na ruch (wartość w sekundach)
     * @param trescTablica Treść polecenia w postaci tablicy 
     * @param gra Gra, dla której będziemy zmieniać czas na ruch
     */
    private static void ustawCzasNaRuch(String[] trescTablica, Gra gra) {
        //walidacja danych
        if(trescTablica.length > 2) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Niepoprawny format polecenia. Prawidłowy format to " + TypyPolecen.BETTIME.dajBudowePolecenia());
            return;
        } 
        
        if(trescTablica.length == 1) {
            ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Obecny czas na ruch wynosi: " + gra.dajSerwerGry().dajCzasNaRuch()/60 + ":" + String.format("%02d", gra.dajSerwerGry().dajCzasNaRuch()%60));
        } else {
            try {
            int nowyCzasNaRuch = Integer.parseInt(trescTablica[1]);

            if(nowyCzasNaRuch <= 0) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nowy czas na ruch musi być wartością dodatnią"); 
            } else {
                gra.dajSerwerGry().ustawTymczasCzasNaRuch(nowyCzasNaRuch);
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Ustawiono nowy czas na ruch " + nowyCzasNaRuch + 
                        "s. Aby wprowadzić zmiany zresetuj serwer poleceniem /restart.");
            }
            } catch (NumberFormatException e) {
                ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Nowy czas na ruch musi być liczbą");
            }
        }
        
    }
}
