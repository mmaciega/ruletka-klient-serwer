package pl.eden.ruletka.game.logic;

import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.net.packets.Pakiet;
import pl.eden.ruletka.game.net.packets.Pakiet12Results;
import pl.eden.ruletka.game.net.packets.Pakiet13Time;

/**
 * Klasa reprezentująca menadżera rogrywki. Jego zadaniem jest sprawdzanie czy wszyscy gracze zakończyli stawianie zakładów lub czy minał czas przeznaczony na zakładay
 * @author Mateusz Macięga
 */
public class MenadzerRozgrywki {
    /** Timer zarządzający rozgrwyką */
    private final Timer timer;
    /** Timer odpowiedzialny za wrócenie do stanu początkowego czasu pozostałego na ruch */
    private Timer timerZresetowanie;
    /** Instancja gry */
    private final Gra gra;
    /** Czas jaki pozostał graczom na ruch */
    private long pozostalyCzasNaRuch;
    /** Początkowy czas na ruch */
    private long czasNaRuch;
    
    /**
     * Konstruktor sparametryzowany
     * @param gra Instacja gry
     */
    public MenadzerRozgrywki(Gra gra) {
        this.gra = gra;
        timer = new Timer("Wątek menadżera rozgrywki");
    }
    
    /**
     * Metoda odpowiedzialna za ustawienie czasu na ruch
     * @param czasNaRuch Czas na ruch
     */
    public void ustawCzasNaRuch(long czasNaRuch) {
        this.czasNaRuch = czasNaRuch;
        this.pozostalyCzasNaRuch = this.czasNaRuch;
    }
    
    /**
     * Metoda odpowiedzialna za uruchomienie timera, który co sekunde będzie odswieżał czas na ruch i sprawdzał czy należy już losować liczbę
     */
    public void uruchomZadanie() {
        timer.schedule(new Zadanie(), 1000, 1000);
    }
    
    /**
     * Metoda zwracająca pozostały czas na ruch
     * @return Pozostały czas na ruch
     */
    public long dajPozostalyCzasNaRuch() {
        return pozostalyCzasNaRuch;
    }

    /** 
     * Metoda odpowiedzialna za uruchomienie timera
     * @param czasNaRuch Czas na ruch 
     */
    public void ustawNowyCzas(long czasNaRuch) {
        timerZresetowanie = new Timer();
        timerZresetowanie.schedule(new ZadaniePoResetowaniu(czasNaRuch), 2000);
    }
    
    /**
     * Klasa reprezentująca timer odpowiedzialny za zarządzanie rozgrywką
     */
    class ZadaniePoResetowaniu extends TimerTask{
        private final long czasNaRuch;
       
        /**
         * Konstruktor sparametryzowany
         * @param czasNaRuch Czas na ruch
         */
        public ZadaniePoResetowaniu(long czasNaRuch) {
            this.czasNaRuch = czasNaRuch;
        }
        
        
        @Override
        public void run() {
            ustawCzasNaRuch(czasNaRuch);
            timerZresetowanie.cancel();
        }
    }
    
    class Zadanie extends TimerTask{
        @Override
        public void run(){
            if(!gra.dajSerwerGry().dajPolaczeniGracze().isEmpty()) {
                int liczbaGraczyKtorzyZakonczyli = 0;
                for(Gracz g : gra.dajSerwerGry().dajPolaczeniGracze()) {
                    if (g.czyZakonczylZaklady()) {
                        liczbaGraczyKtorzyZakonczyli++; 
                    } else {
                        break;
                    }
                }
                
                Pakiet pakiet = new Pakiet13Time(pozostalyCzasNaRuch);
                pakiet.piszDane(gra.dajSerwerGry());
                      
                
                if((liczbaGraczyKtorzyZakonczyli == gra.dajSerwerGry().dajPolaczeniGracze().size()) || (pozostalyCzasNaRuch == 0)) {
                    System.out.println("[SERWER] Wszyscy zakonczyl zaklady");
                    
                    Random ran = new Random();
                    int wylosowanaWartosc = ran.nextInt(37);
                    Map<String,Long[]> zwyciescyGracze = gra.dajMenadzerZakladow().sprawdzZaklady(wylosowanaWartosc);
                    
                    System.out.println("[SERWER] Wylosowano liczbe " + wylosowanaWartosc);
                    
                    for (Map.Entry<String, Long[]> pozycja : zwyciescyGracze.entrySet())
                    {
                        GraczMP gracz = (GraczMP) gra.dajSerwerGry().dajPolaczeniGracze().get(gra.dajSerwerGry().dajIndeksGraczaMP(pozycja.getKey()));
                        gracz.ustawPieniadzeStartowe(gracz.dajPieniadze() + pozycja.getValue()[0] + pozycja.getValue()[1]);
                        System.out.println("[SERWER] Dodałem graczowi: " + gracz.dajPseudonim() + " " + (pozycja.getValue()[0] + pozycja.getValue()[1]));
                    }
                    
                    for(Gracz g : gra.dajSerwerGry().dajPolaczeniGracze()) {
                        g.ustawCzyZakonczylZaklady(false);
                    }
                    
                    pakiet = new Pakiet12Results(zwyciescyGracze, wylosowanaWartosc);
                    pakiet.piszDane(gra.dajSerwerGry());                    
                    pozostalyCzasNaRuch = czasNaRuch;
                }
                
                pozostalyCzasNaRuch--;
            }
        }
    }
    
}
