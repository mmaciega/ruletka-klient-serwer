package pl.eden.ruletka.game.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractListModel;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;

/**
 * Klasa reprezentująca model listy graczy uczestniczących w rozgrywce
 * @author Mateusz Macięga
 */
public class ListaGraczyModel extends AbstractListModel implements Iterable<Gracz>{
    /** Lista graczy uczestniczących w grze */
    private List<Gracz> listaGraczy;

    /**
     * Konstruktor domyślny
     */
    public ListaGraczyModel() {
        listaGraczy = new ArrayList<>();
    }

    /**
     * Metoda odpowiedzialna za ustawienie nowej listy graczy
     * @param listaGraczy Lista graczy
     */
    public void ustawNowaListe(List<Gracz> listaGraczy) {
        this.listaGraczy = listaGraczy;
        fireContentsChanged(this, 0, getSize());
    }

    /**
     * Metoda zwracająca liczbę graczy na liście
     * @return Liczba graczy na liście
     */
    @Override
    public int getSize() {
        return listaGraczy.size();
    }

    /**
     * Metoda zwracająca gracza znajdującego się na pozycji przekazanego jako argument
     * @param i Pozycja gracza na liście
     * @return Instancja klasy Gracz znajdują się na i-tej pozycji na liście graczy
     */
    @Override
    public Object getElementAt(int i) {
        return listaGraczy.get(i);
    }

    /**
     * Metoda odpowiedzialna za dodanie obiektu klasy Gracz do listy graczy
     * @param element Instancja klasy gracz, która ma zostać dodana do listy graczy
     */
    public void addElement(Object element) {
        if (listaGraczy.add((Gracz) element)) {
            fireIntervalAdded(this, listaGraczy.indexOf((Gracz) element), listaGraczy.indexOf((Gracz) element));
        }
    }

    /**
     * Metoda odpowiedzialna za przekopiowanie wszystkich graczy z listy przekazanej jako argument
     * @param list Lista graczy, którą chcemy przekopiować
     */
    public void addAll(List<Gracz> list) {
        listaGraczy.addAll(list);
        fireContentsChanged(this, 0, getSize());
    }

    /**
     * Metoda służąca do wyczyszczenia listy graczy
     */
    public void clear() {
        listaGraczy.clear();
        fireContentsChanged(this, 0, getSize());
      }  
    
    /**
     * Metoda zwracająca pierwszy element na liście graczy
     * @return Pierwszy element listy graczy
     */
    public Object firstElement() {
        return listaGraczy.get(0);
    }

    /**
     * Metoda zwracająca ostatni element na liście graczy
     * @return Ostatni element listy graczy
     */
    public Object lastElement() {
        return listaGraczy.get(listaGraczy.size()-1);
    }
    
    /**
     * Metoda zwracająca iterator listy graczy
     * @return Iterator listy graczy
     */
    @Override
    public Iterator iterator() {
        return listaGraczy.iterator();
    }

    /**
     * Metoda odpowiedzialna za zmianę wartości pieniężnej danego gracza na liście graczy
     * @param element Instancja gracza, któremu chcemy zmienić wartośc pieniedzy
     * @param pieniadze Wartość pieniędzy, na która chcemy zmienić graczowi
     */
    public void changeElement(Gracz element, long pieniadze) {
        Gracz gracz = listaGraczy.get(listaGraczy.indexOf(element));
        gracz.ustawPieniadzeStartowe(pieniadze);
        fireContentsChanged(this, listaGraczy.indexOf(element), listaGraczy.indexOf(element));
    }
    
    /**
     * 
     * @param element
     * @param b 
     */
    public void changeFinishedState(Gracz element, boolean b) {
        Gracz gracz = listaGraczy.get(listaGraczy.indexOf(element));
        gracz.ustawCzyZakonczylZaklady(b);
        fireContentsChanged(this, listaGraczy.indexOf(element), listaGraczy.indexOf(element)); 
    }
    
    public void graczZakonczylZaklady(Gracz element) {
        Gracz gracz = listaGraczy.get(listaGraczy.indexOf(element));
        gracz.przelaczCzyZakonczylZaklady();
        fireContentsChanged(this, listaGraczy.indexOf(element), listaGraczy.indexOf(element));
    }
    
    /**
     * Metoda odpowiedzialna za usunięcie gracza z listy graczy znajdującego się na pozycji przekazanej jako argument
     * @param indeks Indeks gracza, którego chcemy usunąć
     */
    public void remove(int indeks) {
        listaGraczy.remove(indeks);
        fireIntervalRemoved(this, indeks, indeks);
    }
    
    /**
     * Metoda odpowiedzialna za usuniecie gracza z listy graczy zgodnego z obiektem klasy Gracz przekazanym jako argument
     * @param element Obiekt klasy gracza
     * @return Informacja o tym czy istniał gracz zgodny z podanym obiektem jako argument
     */
    public boolean removeElement(Gracz element) {
        Gracz graczDoUsuniecia = (Gracz) element;

        int indeks = 0;
        Iterator<Gracz> g = listaGraczy.iterator();
        while (g.hasNext()) {
           Gracz gracz = g.next();

           if(graczDoUsuniecia.dajPseudonim().equals(gracz.dajPseudonim()))
           {
                g.remove();
                fireIntervalRemoved(this, indeks, indeks);
                return true;
           }
           ++indeks;
        }

        return false;
    } 

    /**
     * Metoda odpowiedzialna za usunięcie gracza z listy graczy o podanej nazwie
     * @param pseudonim Nazwa gracza, którego chcemy usunąć
     * @return Inforamcja o tym czy został usunięty gracz o podanej nazwie
     */
    public boolean removeElement(String pseudonim) {
        int indeks = 0;
        Iterator<Gracz> g = listaGraczy.iterator();
        while (g.hasNext()) {
           Gracz gracz = g.next();

           if(gracz.dajPseudonim().equals(pseudonim))
           {
                g.remove();
                fireIntervalRemoved(this, indeks, indeks);
                return true;
           }
           ++indeks;
        }

        return false;
    }
    
    /**
     * Metoda zwracająca indeks na jakim znajduje się gracz o podanej nazwie 
     * @param pseudonim Nazwa gracza, którego indeks poszukujemy
     * @return Indes na jakim znajduje się gracz o podanej nazwie
     */
    public int dajIndeksGraczaMP(String pseudonim) {
        int index = 0;
        for (Gracz g : listaGraczy) {
            if (g instanceof GraczMP && ((GraczMP) g).dajPseudonim().equals(pseudonim)) {
                break;
            }
            index++;
        }
        return index;
    }
} 