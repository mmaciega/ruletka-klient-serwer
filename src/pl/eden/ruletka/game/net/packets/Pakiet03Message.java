package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do przesłania wiadomości czatowej gracza
 * @author Mateusz Macięga
 */
public class Pakiet03Message extends Pakiet {
    /** Nazwa gracza, który przesyła wiadomość */
    private final String pseudonimGracza;
    /** Treść wiadomości */
    private String tresc;
  
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet03Message(byte[] dane) {
        super(03);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.tresc = wiadomosc[1].substring(1);
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który przesyła wiadomość
     * @param tresc Treść wiadomości
     */
    public Pakiet03Message(Gracz gracz, String tresc) {
        super(03);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.tresc = tresc;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza, który przesyła wiadomość, w postaci łancuchu
     * @param tresc 
     */
    public Pakiet03Message(String pseudonimGracza, String tresc) {
        super(03);
        this.pseudonimGracza = pseudonimGracza;
        this.tresc = tresc;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z wiadomością od gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet03Message.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z wiadomością od gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet03Message.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
       return ("03" + pseudonimGracza + ",T" + tresc + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza
     * @return Nazwa gracza
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
    
    /**
     * Metoda zwracająca treść wiadomości
     * @return Treść wiadomości
     */
    public String dajTresc() {
        return tresc;
    }
    
}
