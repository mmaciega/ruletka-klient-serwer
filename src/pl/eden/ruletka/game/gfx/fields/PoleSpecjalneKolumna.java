package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalnego typu kolumna
 * @author Mateusz Macięga
 */
public class PoleSpecjalneKolumna extends PoleSpecjalne {
    /** Numer kolumny */
    private final int numerKolumny;
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param numerKolumny Numer kolumny 
     */
    public PoleSpecjalneKolumna(String nazwa, int numerKolumny) {
        super(nazwa, Pole.MnoznikiPola.SIX_LINE_BET, PoleSpecjalne.TypyPolaSpecjalnego.KOLUMNA);
        this.numerKolumny = numerKolumny;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        return ((1 + ((this.numerKolumny-1)*3)) == wylosowanePole) || ((2 + ((this.numerKolumny-1)*3)) == wylosowanePole) ||
                ((3 + ((this.numerKolumny-1)*3)) == wylosowanePole);
    }
    
    /**
     * Metoda zwracająca numer kolumny
     * @return Numer kolumny
     */
    public int dajNumerKolumny() {
        return numerKolumny;
    }
    
}
