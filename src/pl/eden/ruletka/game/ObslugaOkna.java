package pl.eden.ruletka.game;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.gfx.PanelDolny;

/**
 * Klasa odpowiedzialna za zarządzanie zdarzeniami związanymi z oknem np. zamknięcie okna gry
 * @author Mateusz Macięga
 */
public class ObslugaOkna implements WindowListener {
    /** Instancja gry */
    private final Gra gra;
    
    /**
     * Konstruktor domyślny klasy
     * @param gra Okno gry, dla którego klasa ma być odpowiedzialna za obsłużenie zdarzeń
     */
    public ObslugaOkna(Gra gra) {
        this.gra = gra;
        gra.addWindowListener(this);
    }
    

    /**
     * Metoda odpowiedzialna za obsługę zdarzenia otwarcia okna z grą
     * @param we Zdarzenie okna
     */
    @Override
    public void windowOpened(WindowEvent we) {    
        PanelDolny.dajInstancje().dajPoleTekstoweCzatu().requestFocus();    //przekazanie focusu polu tekstowemu czatu, można od razu wprowadzać komunikaty
    }

    /**
     * Metoda odpowiedzialna za obsługę zdarzenia zamknięcia okna z grą
     * @param we Zdarzenie okna
     */
    @Override
    public void windowClosing(WindowEvent we) {
        try {
            // zamakymamy gniazdo klienta gry
            if(gra.dajGniazdoKlienta() != null) {
                gra.dajGniazdoKlienta().dajGniazdo().close();
            }
            
            // zamykamy wszystkie połączenia z serwerem
            if(gra.dajSerwerGry() != null) {
                Iterator selectedKeys = gra.dajSerwerGry().dajSelector().keys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) selectedKeys.next();
                    
                    if(key.isValid() && key.channel() instanceof SocketChannel) {
                        SocketChannel sch=(SocketChannel) key.channel();
                        sch.close();
                    }
                }
                
                //zamykamy gniazdo serwera
                gra.dajSerwerGry().dajChannel().close();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ObslugaOkna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//<editor-fold defaultstate="collapsed" desc="inne nieobłużone zdarzenia okna">
    @Override
    public void windowClosed(WindowEvent we) {    }
    
    @Override
    public void windowIconified(WindowEvent we) {
    }
    
    @Override
    public void windowDeiconified(WindowEvent we) {
    }
    
    @Override
    public void windowActivated(WindowEvent we) {
    }
    
    @Override
    public void windowDeactivated(WindowEvent we) {
    }
//</editor-fold>
    
}
