package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o zablokowaniu adresu IP gracza
 * @author Mateusz Macięga
 */
public class Pakiet06BanPlayer extends Pakiet {
    /** Nazwa gracza, który korzystał ze zbanowanego adresu IP */
    private final String pseudonimGracza;
    /** Powód zbanowania gracza */
    private final String powodZbanowania;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet06BanPlayer(byte[] dane) {
        super(06);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.powodZbanowania = wiadomosc[1].substring(1);
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który został zablokowany
     * @param powodZbanowania  Powód zablokowania gracza 
     */
    public Pakiet06BanPlayer(Gracz gracz, String powodZbanowania) {
        super(06);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.powodZbanowania = powodZbanowania;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który został zablokowany
     */
    public Pakiet06BanPlayer(Gracz gracz) {
        super(06);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.powodZbanowania = "";
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza, który został zablokowany
     * @param powodZbanowania Powód zablokowania gracza
     */
    public Pakiet06BanPlayer(String pseudonimGracza, String powodZbanowania) {
        super(06);
        this.pseudonimGracza = pseudonimGracza;
        this.powodZbanowania = powodZbanowania;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza, który został zablokowany
     */
    public Pakiet06BanPlayer(String pseudonimGracza) {
        super(06);
        this.pseudonimGracza = pseudonimGracza;
        this.powodZbanowania = "";
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o zablokowaniu gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet06BanPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o zablokowaniu gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet06BanPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("06" + pseudonimGracza + ",R" + powodZbanowania + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza zablokowanego
     * @return Nazwa gracza zablokowanego
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
    
    /**
     * Metoda zwracajaca powód zablokowania gracza
     * @return Powód zablokowania gracza 
     */
    public String dajPowodZbanowania() {
        return powodZbanowania;
    }
}
