package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne w grze
 * @author Mateusz Macięga
 */
public abstract class PoleSpecjalne extends Pole {
    /** Typ wyliczeniowy zawierający typy pól specjalnych  */
    public static enum TypyPolaSpecjalnego {
        KOLOR, KOLUMNA, PARZYSTOSC, POLOWKA, TUZIN, WIERSZ;
    }
    
    /** Typ pola specjalnego */
    private final TypyPolaSpecjalnego typPolaSpecjalnego;
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param mnoznikWygranej Mnożnik wygranej pola
     * @param typPolaSpecjalnego1 Typ pola specjalnego
     */
    public PoleSpecjalne(String nazwa, MnoznikiPola mnoznikWygranej, TypyPolaSpecjalnego typPolaSpecjalnego1) {
        super(nazwa, mnoznikWygranej, Pole.TypyPola.POLE_SPECJALNE);
        this.typPolaSpecjalnego = typPolaSpecjalnego1;
    }
    
    /**
     * Metoda zwracająca typ pola specjalnego
     * @return Typ pola specjalnego
     */
    public TypyPolaSpecjalnego dajTypPolaSpecjalnego() {
        return typPolaSpecjalnego;
    }
}
