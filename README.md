#  Ruletka (klient-serwer) #

Aplikacja napisana w Javie umożliwiająca wieloosobową rozgrywkę w ruletkę. Aplikacja
stworzona jest w architekturze klient - serwer. Klient komunikuje się z serwerem, który
kontroluje przebieg rozgrywki oraz dokonuje wymiany komunikatów pomiędzy serwerem
a klientami uczestniczącymi w grze. Aplikacja przystosowana jest do pracy na komputerach
stacjonarnych i przenośnych. Gra powstała w ramach pracy inżynierskiej.


Konfiguracja serwera odbywa się za pomocą zdefiniowanych poleceń wpisywanych
w odpowiednim polu. 


## Budowa aplikacji ##

Serwer gry jest serwerem nasłuchującym co oznacza, że działa w tym samym
procesie co klient gry. Jeden z graczy uczestniczących w rozgrywce jest równocześnie
klientem i serwerem gry.

### Klient gry ###

Klient gry wysyła i odbiera komunikaty od serwera, z którym jest połączony.
Odebrane komunikaty są przetwarzane przez klienta. Klient gry zajmuje się również
renderowaniem interfejsu graficznego aplikacji.

### Serwer gry ###

Głównym zadaniem serwera jest organizacja i zarządzanie rozgrywką. Serwer
decyduje m.in. o rozpoczęciu i zakończeniu rozgrywki. Zadaniem serwera jest również
przekazywanie informacji do wszystkich klientów gry. W przypadku wystąpienia jakiegoś
zdarzenia podczas rozgrywki, zadaniem serwera jest poinformowanie wszystkich klientów
o jego wystąpieniu. Wszystkie informacje, m.in. o postawionych zakładach i wartościach
pieniężnych graczy, przechowywane są na serwerze gry.

## Protokół komunikacyjny gry ##

Na potrzeby aplikacji stworzony został protokół komunikacyjny. Komunikacja
odbywa się za pomocą pakietów przesyłanych w postaci bajtów pomiędzy klientami
a serwerem. Informacje na temat rozgrywki przesyłane są za pomocą tego protokołu.

Każdy pakiet posiada taką samą budowę. Pierwsza dwa bajty określają typ pakietu.
Typem pakietu jest dwucyfrowa liczba całkowita. Na następnych bajtach znajdują się bajty
danych. Liczba bajtów oraz format danych zależny jest od typu pakietu. Każdy pakiet
zakończony jest literą „K”.

## Interfejs graficzny gry ##

![interfejs2.jpg](https://bitbucket.org/repo/ggGXLM/images/2329768756-interfejs2.jpg)
![interfejs3.jpg](https://bitbucket.org/repo/ggGXLM/images/3283470352-interfejs3.jpg)