package pl.eden.ruletka.game.gfx;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Mateusz Macięga
 */
public class ElementGraficzny extends JLabel {

    /**
     * Nazwa pliku wraz z rozszerzeniem do grafiki dla elementu
     */
    private final String nazwaPliku;
    /**
     * Obrazek elementu
     */
    private ImageIcon obrazek;

    public ElementGraficzny(String nazwa) {
        nazwaPliku = nazwa;

        if (nazwa != null) {
            try {
                Image obraz;
                obraz = ImageIO.read(getClass().getResourceAsStream("/" + nazwaPliku));
                obrazek = new ImageIcon(obraz);
                setIcon(obrazek);
                setVerticalAlignment(SwingConstants.CENTER);
                setHorizontalAlignment(SwingConstants.CENTER);
            } catch (IOException ex) {
                System.out.println("Nie ma takiego pliku");
            }
        } else {
            obrazek = null;
        }
    }

    /**
     * Metoda wykorzystywana do skalowania elementu. Wykorzystywana w przypadku
     * zmiany żetonu, gdzie wybrany żeton jest większy od pozostałych.
     *
     * @param skala Skala do zmiany rozmiaru elementu.
     */
    public void skalujProporcjonalnie(double skala) {
        BufferedImage zmienionaIkona = new BufferedImage((int) (skala * obrazek.getIconWidth()), (int) (skala * obrazek.getIconHeight()),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = zmienionaIkona.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.drawImage(obrazek.getImage(), 0, 0, (int) (skala * obrazek.getIconWidth()), (int) (skala * obrazek.getIconHeight()), null);
        g2.dispose();

        setIcon(new ImageIcon((Image) zmienionaIkona));
    }

    /**
     * Metoda zwracajaca obrazek elementu
     *
     * @return Obrazek (tło) elementu
     */
    public ImageIcon dajObrazek() {
        return obrazek;
    }

    /**
     * Metoda zwracająca nazwę pliku z grafika
     *
     * @return Nazwa pliku z grafiką
     */
    public String dajNazwaPliku() {
        return nazwaPliku;
    }
}
