package pl.eden.ruletka.game.net.packets;

import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania już połączonych klientów o dołączeniu do rozgrywki nowego gracza
 * @author Mateusz Macięga
 */
public class Pakiet02Newplayer extends Pakiet {
    /** Nazwa nowego grzacza */
    private final String pseudonimGracza;
    /** Kolor gracza */
    private final Color kolorGracza;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet02Newplayer(byte[] dane) {
        super(02);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.kolorGracza = new Color(Integer.parseInt(wiadomosc[1].substring(1)), Integer.parseInt(wiadomosc[2].substring(1)), Integer.parseInt(wiadomosc[3].substring(1)));
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza, który dołączył do rozgrywki
     */
    public Pakiet02Newplayer(Gracz gracz) {
        super(02);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.kolorGracza = gracz.dajKolorGracza();
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza, który dołączył do rozgrywki, w postaci łancuchu
     */
    public Pakiet02Newplayer(String pseudonimGracza) {
        super(02);
        this.pseudonimGracza = pseudonimGracza;
        this.kolorGracza = Color.black;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu informującego o dołączeniu gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet02Newplayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu informującego o dołączeniu gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet02Newplayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("02" + pseudonimGracza + ",R" + kolorGracza.getRed()+ ",G" + kolorGracza.getGreen() + ",B" + kolorGracza.getBlue() + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę graza
     * @return Nazwa gracza 
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }

    /**
     * Metoda zwracająca kolor nazwy gracza
     * @return Kolor nazwa gracza
     */
    public Color dajKolorGracza() {
        return kolorGracza;
    }
    
}
