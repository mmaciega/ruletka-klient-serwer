package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentujące pole numeryczne w grze
 * @author Mateusz Macięga
 */
public class PoleNumeryczne extends Pole {
    /** Typ wyliczeniowy zawierający dostępny kolory liczb z przypasowanymi do nich liczbami  */
    public static enum KoloryPol {
        BLAD(new int[]{-1}),
        ZADEN(new int[]{0}), 
        CZARNE(new int[]{2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36}),
        CZERWONE(new int[]{1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35});
        
        /** Tabela z liczbami dla danego koloru */
        private final int[] pola;
        
        /**
         * Konstruktor sparametryzowany, prywatny 
         * @param pola Tabela z liczbami dla koloru
         */
        private KoloryPol(int[] pola) {
            this.pola = new int[pola.length];
            System.arraycopy(pola, 0, this.pola, 0, pola.length );
        }
        
        /**
         * 
         * @param pole
         * @return 
         */
        public boolean sprawdzPole(int pole) {
            for(int p : pola) {
                if(p == pole) {
                    return true;
                }
            }
            
        return false;
        }   
    }
    
    /**
     * Metoda statyczna sprawdzajaca jakiego koloru jest liczba przekazana jako argument
     * @param wartosc Liczba, której kolor chcemy sprawdzić
     * @return Kolor pola
     */
    public static KoloryPol znajdzKolorPola(int wartosc) {
        for (KoloryPol k : KoloryPol.values()) {
            if(k.sprawdzPole(wartosc)) {
                return k;
            }
        }
        return PoleNumeryczne.KoloryPol.BLAD;
    }
    
    int wartosc;    
    
    /**
     * Konstruktor sparametryzowany
     * @param wartosc Wartość liczbowa pola
     * @param nazwa Ścieżka do pliku z grafiką pola
     */
    public PoleNumeryczne(int wartosc, String nazwa) {
        super(nazwa, Pole.MnoznikiPola.STRAIGHT_UP_BET, Pole.TypyPola.POLE_NUMERYCZNE);
        this.wartosc = wartosc;
    }
    
    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        return wartosc == wylosowanePole;
    }
    
    /**
     * Metoda zwracająca wartość liczbową pola
     * @return Wartość liczbowa pola
     */
    public int dajWartosc() {
        return wartosc;
    }
    
}
