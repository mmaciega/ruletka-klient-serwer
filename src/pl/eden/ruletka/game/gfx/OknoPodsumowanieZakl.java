package pl.eden.ruletka.game.gfx;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.gfx.fields.PoleNumeryczne;
import pl.eden.ruletka.game.logic.NowaGra;

/**
 * Klasa reprezentująca okno podsumowujące losowanie. Okno informuje o wylosowanej wartości i kolorze oraz wygranej gracza.
 * @author Mateusz Macięga
 */
public class OknoPodsumowanieZakl extends JPanel{
    private final JPanel wynikLosowania = new WynikLosowania();
    private final JPanel wygrana = new Wygrana();
    private final JPanel zamkniecie = new Zamkniecie();
    
    /**
     * Konstruktor domyślny
     */
    public OknoPodsumowanieZakl() {
        setOpaque(false); 
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 1;
        c.insets = new Insets(7,0,0,0);
        
        add(wynikLosowania);
        add(wygrana,c);
        
        c.gridy = 2;
        add(zamkniecie, c);
    }
    
    /**
     * Metoda ustawiająca wylosowane pole, które ma zostać wyświetlone
     * @param wylosowanePole Wylosowane pole przez koło ruletki
     */
    public void ustawWylosowanePole(int wylosowanePole) {
        ((WynikLosowania)wynikLosowania).ustawWylosowaniePole(wylosowanePole);
    }
    
    /**
     * Metoda ustawiającą wartość wygranej gracza w oknie
     * @param wartoscWygranej Wartość wygranej gracza
     */
    public void ustawWartoscWygranej(long wartoscWygranej) {
        ((Wygrana)wygrana).ustawWartoscWygranej(wartoscWygranej);
    }
    
    
    /**
     * Metoda zarządzają kurtyną (wyświetlenie półprzeźroczystego czarnego kwadratu w panelu głównym)
     * @param g 
     */ 
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(0, 0, 0, 160));
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    } 
    
    class WynikLosowania extends JPanel {
        /** Tło panelu. */
        private Image tloObrazek;
        /** Wartość wylosowanego pola */
        private int wylosowanePole;
        
        public WynikLosowania()
        {
            try 
            {
                tloObrazek = ImageIO.read(getClass().getResourceAsStream("/oknoPodsumowujace/nnumber.png"));
            } 
            catch (IOException ex) {
                System.out.println("Nie znaleziono pliku");
            }

            setPreferredSize(new Dimension(tloObrazek.getWidth(null), tloObrazek.getHeight(null)));
            setOpaque(false);
        }
        
        private void ustawWylosowaniePole(int wylosowanePole) {
            this.wylosowanePole = wylosowanePole;
        }
        
        /**
         * Metoda zarządzają panelem okna (rysowanie wylosowanego pola)
         * @param g 
         */        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;

            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);         

            g2d.drawImage(tloObrazek, 0, 0, null);
            
            PoleNumeryczne.KoloryPol typ =  PoleNumeryczne.znajdzKolorPola(wylosowanePole);
            switch(typ) {
                case CZARNE:
                    g2d.setColor(Color.black);
                    break;
                case CZERWONE:
                    g2d.setColor(Color.red);
                    break;
                    
                case ZADEN:
                    g2d.setColor(Color.white);
                    break;
            }
            
            g2d.setFont(new Font("Times New Roman", Font.BOLD, 73));
            if(wylosowanePole >9)
            {
                g2d.drawString(String.valueOf(wylosowanePole),(int) (tloObrazek.getWidth(null)/3) + 2,(int) (tloObrazek.getHeight(null)/2) + 43);
            }
            else
            {
                g2d.drawString(String.valueOf(wylosowanePole),(int) (tloObrazek.getWidth(null)/3) + 21,(int) (tloObrazek.getHeight(null)/2) + 43);
            }
        }
    }
    
    /**
     * Klasa reprezentująca panel, który wyświetla wygraną gracza w danym losowaniu.
     */
    class Wygrana extends JPanel {
        /** Obrazek panelu */
        private Image tloObrazek;
        /** Wartość wygranej gracza */
        private long wygrana;
        /** Czcionka, w której zostanie wyświetlowa wartość wygranej */
        private Font domyslnaCzcionka;
        
        public Wygrana()
        {
            domyslnaCzcionka = new Font("Times New Roman", Font.BOLD, 22);

            try 
            {
                tloObrazek = ImageIO.read(getClass().getResourceAsStream("/oknoPodsumowujace/you_won.png"));
            } 
            catch (IOException ex) {}

            setPreferredSize(new Dimension(tloObrazek.getWidth(null), tloObrazek.getHeight(null)));
            setOpaque(false);
        }
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D) g;

            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);    

            g2d.drawImage(tloObrazek, 0, 0, null);  
            
            if(wygrana !=0) {
                g2d.setColor(new Color(23,86,43));
            } else {
                g2d.setColor(Color.black);
            }
            
            String lancuch = wygrana + "$";

            FontMetrics metrykaCzcionki = getFontMetrics(domyslnaCzcionka);
            int dlugoscLancucha = metrykaCzcionki.stringWidth(lancuch);            

            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            g2d.setFont(domyslnaCzcionka);
            g.drawString(lancuch, (int) (getWidth()-20) - dlugoscLancucha ,(int) (getHeight()/2)  + (metrykaCzcionki.getHeight()/3));
        } 

        private void ustawWartoscWygranej(long wartoscWygranej) {
            this.wygrana = wartoscWygranej;
        }
    }
    
    class Zamkniecie extends JPanel implements MouseListener
    {
        /** Przycisk OK */
        private final JLabel przyciskOK;
        
        public Zamkniecie()
        {
            przyciskOK = new ElementGraficzny("oknoPodsumowujace/ok_button.png");
            przyciskOK.addMouseListener(this);
            przyciskOK.addMouseMotionListener(ZmianaKursora.dajInstancje());

            setLayout(new FlowLayout(FlowLayout.CENTER));
            setOpaque(false);
            add(przyciskOK);
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            getRootPane().getGlassPane().setVisible(false);
            ((OknoPodsumowanieZakl)getParent()).ustawWartoscWygranej(0L);
            
            Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
            gra.ustawFlagaWait(false);
            
            if(gra.dajGracza().dajPieniadze() == 0) {
                NowaGra nowaGra = new NowaGra(gra);
            }
        }

//<editor-fold defaultstate="collapsed" desc="inne nieobsłużone zdarzenia">
        @Override
        public void mousePressed(MouseEvent me) {
        }
        
        @Override
        public void mouseReleased(MouseEvent me) {
        }
        
        @Override
        public void mouseEntered(MouseEvent me) {
        }
        
        @Override
        public void mouseExited(MouseEvent me) {
        }
//</editor-fold>
        
    }

}
