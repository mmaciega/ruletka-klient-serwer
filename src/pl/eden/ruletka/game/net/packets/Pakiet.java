package pl.eden.ruletka.game.net.packets;

import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa abstrakcyjna reprezentująca pakiet protokołu komunikacyjnego
 * @author Mateusz Macięga
 */
public abstract class Pakiet {
    /** 
     * Typ wyliczeniowy zawierający typy dostępnych pakietów
     */
    public static enum TypyPakietu {
        INVALID(-1), LOGIN(0), DISCONNECT(1), NEWPLAYER(2), MESSAGE(3), CONFIGURATION(4), 
        KICKPLAYER(5), BANPLAYER(6), LOGINDENIED(7), RESTART(8), NEWBET(9), RESETBETS(10), FINISHEDBETS(11), RESULTS(12), TIME(13),
        NEWGAME(14);

        private final int pakietID;

        private TypyPakietu(int pakietID) {
            this.pakietID = pakietID;
        }

        public int dajId() {
            return pakietID;
        }
    }
    
    /** Identyfikator pakietu */
    public byte pakietID;

    /**
     * Konstrutktor sparametryzowany
     * @param pakietID Identyfikator pakietu
     */
    public Pakiet(int pakietID) {
        this.pakietID = (byte) pakietID;
    }
    
    /**
     * Metoda zwracająca nazwę typu pakietu na podstawie jego identyfikatora
     * @param id Identyfikator pakietu
     * @return Nazwa typu pakietu
     */
    public static TypyPakietu znajdzTypPakietu(int id) {
        for (TypyPakietu p : TypyPakietu.values()) {
            if (p.dajId() == id) {
                return p;
            }
        }
        return TypyPakietu.INVALID;
    }
    
    /**
     * Metoda zwracająca nazwę typu pakietu na podstawie jego identyfikatora
     * @param pakietID Identyfikator pakietu w formie łanuchu
     * @return Nazwa typu pakietu
     */
    public static TypyPakietu znajdzTypPakietu(String pakietID) {
        try {
            return znajdzTypPakietu(Integer.parseInt(pakietID));
        } catch (NumberFormatException e) {
            return TypyPakietu.INVALID;
        }
    }
    
    /**
     * Abstarkcyjna metoda odpowiedzialna za wysłanie pakietu od klienta do serwera
     * @param klient Gniazdo klienta gry
     */
    public abstract void piszDane(KlientGry klient);

    /**
     * Abstrakcyjna metoda odpowiedzialna za wysłanie pakietu od serwera do wszystkich klientów
     * @param serwer Gniazdo serwera gry
     */
    public abstract void piszDane(SerwerGry serwer);
    
    /**
     * Metoda zwracająca jedynie dane pakietu w formie łańcuchu
     * @param dane Pakiet w postaci tablicy bajtów
     * @return Łańcuch zawierający dane pakietu przekazanego jako argument
     */
    public String czytajDane(byte[] dane) {
        String wiadomosc = new String(dane).trim();
        return wiadomosc.substring(2,wiadomosc.length()-1);
    }
    
    /**
     * Metoda abstrakcyjna zwracająca w postaci tablicy bajtów pakiet
     * @return Pakiet w postaci tablicy bajtów
     */
    public abstract byte[] dajDane();
}
