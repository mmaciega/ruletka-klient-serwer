package pl.eden.ruletka.game.gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JLabel;

/**
 * Klasa reprezentująca zmiane kursora po najechaniu na przyciski i żetony. 
 * @author Mateusz Macięga
 */
public class ZmianaKursora implements MouseMotionListener {
    /**
     * Konstruktor prywatny. Zastosowano wzorzec Singleton
     */
    private ZmianaKursora() {
    }
    
    /**
     * Metoda zwracająca instancję klasy
     * @return Instancja klasy ZmianaKursora
     */
    public static ZmianaKursora dajInstancje() {
        return ZmianaKursoraInstancja.INSTANCJA;
    }
    
    /**
     * Metoda statyczna tworząca instację klasy
     */
    private static class ZmianaKursoraInstancja {
        private static final ZmianaKursora INSTANCJA = new ZmianaKursora();
    }

    /**
     * Metoda obsługująca zdarzenie najechania na przycisk lub żeton. Zmiana kursora na rączke.
     * @param e 
     */
     @Override
     public void mouseMoved(MouseEvent e) 
     {
        final int x = e.getX();
        final int y = e.getY();
        final JLabel zrodlo = (JLabel) e.getSource();

        final Rectangle cellBounds = new Rectangle(new Dimension((int) zrodlo.getPreferredSize().getWidth(), (int) zrodlo.getPreferredSize().getHeight()));
        if (cellBounds.contains(x, y)) 
        {
                zrodlo.setCursor(new Cursor(Cursor.HAND_CURSOR));
        } 
        else 
        {
                zrodlo.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
     }
//<editor-fold defaultstate="collapsed" desc="zdarzenie nieobsłużone">
     
     @Override
     public void mouseDragged(MouseEvent me) {
     }
//</editor-fold>
}
