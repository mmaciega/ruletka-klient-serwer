package pl.eden.ruletka.game.logic;

import java.util.Timer;
import java.util.TimerTask;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.gfx.PanelDolny;
import pl.eden.ruletka.game.gfx.PanelGorny;
import pl.eden.ruletka.game.gfx.PanelSrodkowy;

/**
 * Klasa odpowiedzialna za zresetowanie serwera gry
 * @author Mateusz Macięga
 */
public class ResetGry {
    /** Timer odpowiedzialna za reset gry */
    private final Timer timer;
    /** Instancja gry */
    private final Gra gra;
    /** Liczba sekund do zresetowania */
    private int nrPetli = 3;
    
    /**
     * Konstruktor sparametryzowany
     * @param gra Instancja gry
     */
    public ResetGry(Gra gra) {
        timer = new Timer();
        this.gra = gra;
    }
    
    /**
     * Metoda odpowiedzialna za uruchomienie wątku resetującego seerwer
     */
    public void uruchomZadanie() {
        timer.schedule(new Zadanie(), 0, 1000);
    }
    
    /**
     * Klasa zawierająca instrukcje pozwalające na zresetowanie serwera gry
     */
    class Zadanie extends TimerTask{
        @Override
        public void run(){
            PanelDolny.dajInstancje().wprowadzKomunikat("Serwer zostanie zrestartowany za " + nrPetli--);
            
            if(nrPetli == 0) {
                for(Gracz g : gra.dajListeGraczy()) {
                    gra.zmodyfikujPieniadzeGracza(g, gra.dajGniazdoKlienta().dajPieniadzeStartowe());
                    gra.dajMenadzerZakladow().usunZakladyGracza((GraczMP) g);
                    gra.zmodyfikujZakonczenieZaklGracza(g,false);
                }

                ((PanelSrodkowy) PanelSrodkowy.dajInstancje()).wyczyscWszystkiePola();

                gra.dajGracza().ustawPieniadzeStartowe(gra.dajGniazdoKlienta().dajPieniadzeStartowe());
                PanelGorny.dajInstancje().repaint();
                PanelDolny.dajInstancje().wprowadzKomunikat("Serwer został zrestartowany. Usunięto zakłady, ustawiono wartość początkową pieniędzy");
                timer.cancel();
            }
        }
    }
    
}
