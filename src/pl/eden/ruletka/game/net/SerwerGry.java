package pl.eden.ruletka.game.net;

import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.logic.MenadzerRozgrywki;
import pl.eden.ruletka.game.net.packets.Pakiet;
import pl.eden.ruletka.game.net.packets.Pakiet.TypyPakietu;
import pl.eden.ruletka.game.net.packets.Pakiet00Login;
import pl.eden.ruletka.game.net.packets.Pakiet01Disconnect;
import pl.eden.ruletka.game.net.packets.Pakiet02Newplayer;
import pl.eden.ruletka.game.net.packets.Pakiet03Message;
import pl.eden.ruletka.game.net.packets.Pakiet04Configuration;
import pl.eden.ruletka.game.net.packets.Pakiet05KickPlayer;
import pl.eden.ruletka.game.net.packets.Pakiet06BanPlayer;
import pl.eden.ruletka.game.net.packets.Pakiet07LoginDenied;
import pl.eden.ruletka.game.net.packets.Pakiet09NewBet;
import pl.eden.ruletka.game.net.packets.Pakiet10ResetBets;
import pl.eden.ruletka.game.net.packets.Pakiet11FinishedBets;
import pl.eden.ruletka.game.net.packets.Pakiet14NewGame;

/**
 * Klasa reprezentująca serwer gry
 * @author Mateusz Macięga
 */
public class SerwerGry extends Thread {
    /** Numer portu na jakim uruchomiony jest serwe */
    private static int numerPortu;

    /** Pienidzące startowe rzogrywki */
    private long pieniadzeStartowe = 1500;
    /** Tymczasowe pieniądze startowe. Pole ustawiane po modyfikacji startowych pieniędzy odpowiednim poleceniem */
    private long tymczasPieniadzeStartowe = pieniadzeStartowe;

    /** Informacja o tym czy serwer jest prywatny czy publiczny */
    private boolean czyTrybPrywatny = false;
    /** Hasło dostepu serwera (dla serwera prywatnego) */
    private String hasloSerwera = "";
    /** Maksymalna liczba klientów. Zero oznacza brak limitu */
    private int maksKlientow = 0;
    /** Czas na ruch */
    private long czasNaRuch = 3 * 60; //[s] - czas w sekundach
    /** Tymczasowy czas na ruch. Pole ustawiane po modyfikacji czas na ruch odpowiednim poleceniem */
    private long tymczasCzasNaRuch = czasNaRuch;

    /** Tablica hashująca zawierająca oczekujące dane dla konkretnego gniazda */
    private final Map<SocketChannel, List<ByteBuffer>> oczekujaceDane = new HashMap<>();

    /** Lista oczekujących zmian dla danego gniazda */
    private final List<ZadanieZmiany> oczekujaceZmiany = new LinkedList<>();

    /** Selector do obsługi wielu gniazd sieciowych za pomocą jednego wątku */
    private Selector selector;
    /** Gniazdo serwera gry */
    private ServerSocketChannel channel;
    /** Lista połączonych graczy */
    private final List<GraczMP> polaczeniGracze = new ArrayList<>();
    /** Lista zablokowanych adresów IP */
    private final List<GraczMP> zbanowaniGracze = new ArrayList<>();
    /** Instancja gry */
    private final Gra gra;
    /** Instancja menadżera rozgrywki */
    private final MenadzerRozgrywki menadzerRozgrywki;

    /**
     * Konstruktor sparametryzowany
     * @param numerPortu Numer porty na jakim uruchominy zostanie serwer
     * @param gra Instancja gry
     */
    public SerwerGry(int numerPortu, Gra gra) {
        super("Watek Serwera");
        SerwerGry.numerPortu = numerPortu;
        this.gra = gra;

        inicjalizacjaGniazda();
        inicjalizacjaSelektora();

        menadzerRozgrywki = new MenadzerRozgrywki(gra);
        menadzerRozgrywki.ustawCzasNaRuch(czasNaRuch);
        menadzerRozgrywki.uruchomZadanie();
    }

    /**
     * Metoda odpowiedzialna za konfigurację gniazda serwera
     */
    private void inicjalizacjaGniazda() {
        try {
            channel = ServerSocketChannel.open();
            dajChannel().configureBlocking(false);
            dajChannel().socket().bind(new InetSocketAddress(numerPortu));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gra, "Serwer już jest uruchomiony na tym adresie IP i porcie", "Błąd serwera", JOptionPane.ERROR_MESSAGE);
            gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
        }
    }

    /**
     * Metoda odpowiedzialna za konfiguracją selectora 
     */
    private void inicjalizacjaSelektora() {
        try {
            selector = Selector.open();
            dajChannel().register(dajSelector(), SelectionKey.OP_ACCEPT);
        } catch (IOException ex) {
            Logger.getLogger(SerwerGry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda odpowiedzialna za wysłanie danych na podany adres IP i port
     * @param ipAddress Adres IP, na który chcemy przesłać dane
     * @param port Port, na który chcemy przesłać dane
     * @param data Dane do przesłania
     * @throws IOException 
     */
    private void wyslij(InetAddress ipAddress, int port, byte[] data) throws IOException {
        SocketChannel gniazdo = dajGniazdo(ipAddress, port);

        if (gniazdo != null) {
            wyslij(gniazdo, data);
        } else {
            System.out.println("[SERWER] Nie istnieje gniazdo dla [" + ipAddress.getHostName() + ":" + port + "]");
        }
    }

    /**
     * Metoda odpowiedzialna za wysłanie danych do danego gniazda
     * @param gniazdo Gniazdo sieciowe
     * @param data Dane do wysłania
     * @throws IOException 
     */
    private void wyslij(SocketChannel gniazdo, byte[] data) throws IOException {
        synchronized (this.oczekujaceZmiany) {
            // Indicate we want the interest ops set changed
            this.oczekujaceZmiany.add(new ZadanieZmiany(gniazdo, ZadanieZmiany.CHANGEOPS, SelectionKey.OP_WRITE));
            // And queue the data we want written
            synchronized (this.oczekujaceDane) {
                List queue = (List) this.oczekujaceDane.get(gniazdo);
                if (queue == null) {
                    queue = new ArrayList();
                    this.oczekujaceDane.put(gniazdo, queue);
                }

                queue.add(ByteBuffer.wrap(data));
            }
        }

        this.dajSelector().wakeup();
    }

    /**
     * Metoda odpowiedzialna za obsłużenie próby połączenia się klienta z serwerem gry
     * @param klucz Klucz selectora na którym wystąpiła próba połączenia
     * @throws IOException 
     */
    private void obsluzAkceptacje(SelectionKey klucz) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) klucz.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        String address = (new StringBuilder(socketChannel.socket().getInetAddress().toString())).append(":").append(socketChannel.socket().getPort()).toString();
        System.out.println("[SERWER] Zgloszenie klienta: " + address);
        socketChannel.configureBlocking(false);

        socketChannel.register(dajSelector(), SelectionKey.OP_READ, address);
    }

    /**
     * Metoda odpowiedzialna za obsłużenie odczytu danych
     * @param klucz Klucz selectora, który wymaga odczytu danych
     * @throws IOException 
     */
    private void obsluzOdczyt(SelectionKey klucz) throws IOException {
        SocketChannel socketChannel = (SocketChannel) klucz.channel();

        ByteBuffer buforOdczytu = ByteBuffer.allocate(1024);
        buforOdczytu.clear();

        int iloscBajtow;

        try {
            iloscBajtow = socketChannel.read(buforOdczytu);
        } catch (IOException e) {
            // nagle rozlaczenie ze strony klienta
            klucz.cancel();
            socketChannel.close();
            return;
        }

        InetSocketAddress adres = (InetSocketAddress) socketChannel.getRemoteAddress();
        if (iloscBajtow == -1) {
            //prawidlowe zamkniecie polaczenie ze strony klienta

            System.out.println("Prawidlowe rozlaczenie - " + adres.getAddress().getHostName() + ":" + adres.getPort());
            klucz.cancel();
            socketChannel.close();

            GraczMP gracz = null;
            for (GraczMP g : dajPolaczeniGracze()) {
                if (g.dajAdres().getHostName().equals(adres.getAddress().getHostName()) && g.dajPort() == adres.getPort()) {
                    gracz = g;
                    break;
                }
            }

            if (gracz != null) {
                Pakiet01Disconnect pakiet = new Pakiet01Disconnect(gracz);
                Pakiet10ResetBets pakietReset = new Pakiet10ResetBets(gracz);
                usunPolaczenie(pakietReset, pakiet);
            }
        } else {
            System.out.println("[SERVER] Otrzymano polecenie: " + new String(buforOdczytu.array()));
            parsujPakiet(buforOdczytu.array(), socketChannel);
        }
    }

    /** 
     * Metoda odpowiedzialna za obsługę wielu gniazd sieciowych za pomocą jednego wątku
     */
    @Override
    public void run() {

        try {
            System.out.println("[SERWER} Serwer uruchomiony na porcie " + SerwerGry.numerPortu);

            while (true) {
                try {
                    // Przetworzenie oczekujacych zmian w kluczach selektora
                    synchronized (this.oczekujaceZmiany) {
                        Iterator changes = this.oczekujaceZmiany.iterator();
                        while (changes.hasNext()) {
                            ZadanieZmiany change = (ZadanieZmiany) changes.next();
                            switch (change.dajType()) {
                                case ZadanieZmiany.CHANGEOPS:
                                    SelectionKey key = change.dajSocket().keyFor(this.dajSelector());
                                    key.interestOps(change.dajOps());
                            }
                        }
                        this.oczekujaceZmiany.clear();
                    }

                    this.dajSelector().select();
                    Iterator selectedKeys = this.dajSelector().selectedKeys().iterator();

                    while (selectedKeys.hasNext()) {
                        SelectionKey key = (SelectionKey) selectedKeys.next();
                        selectedKeys.remove();

                        if (!key.isValid()) {
                            continue;
                        }

                        if (key.isAcceptable()) {
                            obsluzAkceptacje(key);
                        } else if (key.isReadable()) {
                            obsluzOdczyt(key);
                        } else if (key.isWritable()) {
                            obsluzZapis(key);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                System.out.println("Zamykam serwer");
                dajChannel().close();
            } catch (IOException ex) {
                Logger.getLogger(SerwerGry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Metoda służąca do obłużenia zapisu danych do gniazda
     * @param key Klucz selectora, który wymaga przesłania danych
     * @throws IOException 
     */
    private void obsluzZapis(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        synchronized (this.oczekujaceDane) {
            List queue = (List) this.oczekujaceDane.get(socketChannel);

            // Write until there's not more data ...
            while (!queue.isEmpty()) {
                ByteBuffer buf = (ByteBuffer) queue.get(0);
                socketChannel.write(buf);
                if (buf.remaining() > 0) {
                    // ... or the socket's buffer fills up
                    break;
                }
                queue.remove(0);
            }

            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    /**
     * Metoda odpowiedzialna za przesłanie danych do wszystkich połączonych klientów
     * @param dane Dane do przesłania do klientów
     * @throws IOException 
     */
    public void wyslijDaneDoWszystkichKlientow(byte[] dane) throws IOException {
        for (SelectionKey key : dajSelector().keys()) {
            if (key.isValid() && key.channel() instanceof SocketChannel) {
                SocketChannel socket = (SocketChannel) key.channel();
                wyslij(socket, dane);
            }
        }
    }

    /**
     * Metoda odpowiedzialna za parsowanie otrzymanych danych
     * @param data Otrzymane dane
     * @param socketChannel Gniazdo, z którego otrzymano dane
     * @throws IOException 
     */
    private void parsujPakiet(byte[] data, SocketChannel socketChannel) throws IOException {
        String wiadomosc = new String(data).trim();
        TypyPakietu typ = Pakiet.znajdzTypPakietu(wiadomosc.substring(0, 2));
        Pakiet pakiet;
        GraczMP gracz;

        switch (typ) {
            default:
            case INVALID:
                break;
                
            case LOGIN:
                pakiet = new Pakiet00Login(data);

                InetSocketAddress adres = null;
                boolean czyZbanowany = false;
                boolean czyPseudonimZajety = false;
                boolean czyZleHaslo = false;
                boolean czyBrakMiejsc = false;

                try {
                    adres = (InetSocketAddress) socketChannel.getRemoteAddress();
                    System.out.println("[SERWER] [" + adres.getAddress().getHostName() + ":" + adres.getPort() + "] " + ((Pakiet00Login) pakiet).dajPseudonimGracza() + " połączył się");
                    czyZbanowany = czyAdresZbanowany(adres.getAddress());
                    czyPseudonimZajety = czyPseudonimZajety(((Pakiet00Login) pakiet).dajPseudonimGracza());
                    czyZleHaslo = czyZleHaslo(((Pakiet00Login) pakiet).dajHasloSerwera());
                    czyBrakMiejsc = czyBrakMiejsc();
                } catch (IOException ex) {
                    Logger.getLogger(SerwerGry.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (!czyZbanowany && !czyPseudonimZajety && !czyZleHaslo && !czyBrakMiejsc) {
                    gracz = new GraczMP(dajPieniadzeStartowe(), ((Pakiet00Login) pakiet).dajPseudonimGracza(), ((Pakiet00Login) pakiet).dajKolorGracza(), adres.getAddress(), adres.getPort());
                    dodajPolaczenie(gracz, pakiet);
                } else {
                    pakiet = new Pakiet07LoginDenied(czyZbanowany, czyPseudonimZajety, czyZleHaslo, czyBrakMiejsc);
                    wyslij(adres.getAddress(), adres.getPort(), pakiet.dajDane());
                    System.out.println("[SERWER] Adres zbanowany " + adres.getAddress().getHostName() + ", login zajety,serwer wymaga hasła dostępu lub serwer pełny");
                }
                break;
                
            case MESSAGE:
                pakiet = new Pakiet03Message(data);
                pakiet.piszDane(this);
                break;
                
            case NEWBET:
                pakiet = new Pakiet09NewBet(data);
                gracz = (GraczMP) dajPolaczeniGracze().get(dajIndeksGraczaMP(((Pakiet09NewBet) pakiet).dajPseudonimGracza()));
                gracz.ustawPieniadzeStartowe(gracz.dajPieniadze() - ((Pakiet09NewBet) pakiet).dajWartoscZetonu());
                pakiet.piszDane(this);
                break;
                
            case RESETBETS:
                pakiet = new Pakiet10ResetBets(data);
                gracz = (GraczMP) dajPolaczeniGracze().get(dajIndeksGraczaMP(((Pakiet10ResetBets) pakiet).dajPseudonimGracza()));
                int wartoscZakladow = gra.dajMenadzerZakladow().wartoscZakladow(gracz);
                gracz.ustawPieniadzeStartowe(gracz.dajPieniadze() + wartoscZakladow);
                pakiet.piszDane(this);
                break;
                
            case FINISHEDBETS:
                pakiet = new Pakiet11FinishedBets(data);
                gracz = (GraczMP) dajPolaczeniGracze().get(dajIndeksGraczaMP(((Pakiet11FinishedBets) pakiet).dajPseudonimGracza()));
                gracz.ustawCzyZakonczylZaklady(true);
                pakiet.piszDane(this);
                break;
                
            case NEWGAME:
                pakiet = new Pakiet14NewGame(data);
                gracz = (GraczMP) dajPolaczeniGracze().get(dajIndeksGraczaMP(((Pakiet14NewGame) pakiet).dajPseudonimGracza()));
                gracz.ustawPieniadzeStartowe(dajPieniadzeStartowe());
                pakiet.piszDane(this);
                break;
        }

    }

    /**
     * Metoda sprawdzjąca czy dany adres IP jest zablokowany
     * @param address Adres IP do sprawdzenia
     * @return Informacja o tym czy dany adres znajduje się na liście adresów zbanowanych
     */
    public boolean czyAdresZbanowany(InetAddress address) {
        for (GraczMP gracz : dajZbanowaniGracze()) {
            if (gracz.dajAdres().getHostName().equals(address.getHostName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda sprawdzjąca czy dany adres IP jest zablokowany lub gracz korzystał ze zbanowanego adresu
     * @param adresLubNazwaGracza Adres IP lub nazwa gracza w formie łańcuchu 
     * @return Informacja o tym czy dany adres znajduje się na liście adresów zbanowanych
     */
    public boolean czyAdresZbanowany(String adresLubNazwaGracza) {
        for (GraczMP gracz : dajZbanowaniGracze()) {
            if (gracz.dajAdres().getHostName().equals(adresLubNazwaGracza) || gracz.dajPseudonim().equals(adresLubNazwaGracza)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda sprawdzający czy dana nazwa gracza jest już w użyciu
     * @param pseudonim Nazwa gracza
     * @return Informacja o tym czy dana nazwa gracza jest już w użyciu
     */
    public boolean czyPseudonimZajety(String pseudonim) {
        for (GraczMP gracz : dajPolaczeniGracze()) {
            if (gracz.dajPseudonim().equals(pseudonim)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda sprawdzający czy dane hasło dostępu jest hasłem poprawnych
     * @param hasloSerwera Hasło dostępu
     * @return Informacja o tym czy dane hasło dostępu jest prawidłowe
     */
    public boolean czyZleHaslo(String hasloSerwera) {
        if (czyTrybPrywatny()) {
            return !this.hasloSerwera.equals(hasloSerwera);
        } else {
            return false;
        }
    }

    /**
     * Metoda odpowiedzialna za dodanie nowego połączenia do serwera gry
     * @param gracz Gracza, który dołaczyć do rozgrywki
     * @param pakiet Instancja otrzymanego pakietu 
     * @throws IOException 
     */
    private void dodajPolaczenie(GraczMP gracz, Pakiet pakiet) throws IOException {
        boolean juzPolaczony = false;

        for (GraczMP g : this.dajPolaczeniGracze()) {
            if (gracz.dajPseudonim().equalsIgnoreCase(g.dajPseudonim())) {
                if (g.dajAdres() == null) {
                    g.ustawAdres(gracz.dajAdres());
                }

                if (g.dajPort() == -1) {
                    g.ustawPort(gracz.dajPort());
                }

                juzPolaczony = true;
            } else {
                System.out.println("[SERWER] Dajemy znac polaczonemu klientow o nowym graczu: " + g.dajAdres().getHostName() + ":" + g.dajPort());
                // daj znać aktualnie połączonemu graczowi, że mamy nowego gracza
                Pakiet pakiet2 = new Pakiet02Newplayer(gracz);
                wyslij(g.dajAdres(), g.dajPort(), pakiet2.dajDane());

                //daj znać nowemu graczowi, że istnieje już inny połączony gracz
                pakiet2 = new Pakiet00Login(g);
                wyslij(gracz.dajAdres(), gracz.dajPort(), pakiet2.dajDane());
            }
        }

        if (!juzPolaczony) {
            this.dajPolaczeniGracze().add(gracz);
            System.out.print("[SERWER] Zmodyfikowano liste polaczonych graczy. Lista: ");

            for (GraczMP g : dajPolaczeniGracze()) {
                System.out.print(g.dajPseudonim() + " ");
            }
            System.out.println();
        }

        Pakiet04Configuration pakietConf = new Pakiet04Configuration(dajPieniadzeStartowe(), menadzerRozgrywki.dajPozostalyCzasNaRuch(), gra.dajMenadzerZakladow());
        System.out.println(pakietConf.dajMenadzerZakladow().toString());
        wyslij(gracz.dajAdres(), gracz.dajPort(), pakietConf.dajDane());
    }

    /**
     * Metoda odpowiedzialna za usunięcie połączenia w momencie opuszczenia rozgrywki przez klienta
     * @param pakietReset  
     * @param pakiet
     * @throws IOException 
     */
    private void usunPolaczenie(Pakiet10ResetBets pakietReset, Pakiet01Disconnect pakiet) throws IOException {
        this.dajPolaczeniGracze().remove(dajIndeksGraczaMP(pakiet.dajPseudonimGracza()));
        pakietReset.piszDane(this);
        pakiet.piszDane(this);
    }

    /**
     * Metoda odpowiedzialna za usunięcie połączenia w momencie wyrzucenia gracza z serwera
     * @param pakiet Pakiet wyrzucający gracza
     * @throws IOException 
     */
    public void usunPolaczenie(Pakiet05KickPlayer pakiet) throws IOException {
        this.dajPolaczeniGracze().remove(dajIndeksGraczaMP(pakiet.dajPseudonimGracza()));
        pakiet.piszDane(this);
    }

    /**
     * Metoda odpowiedzialna za zablokowanie adres IP
     * @param pakiet Pakiet blokujący adres IP
     * @throws IOException 
     */
    public void zbanujPolaczanie(Pakiet06BanPlayer pakiet) throws IOException {
        this.dajZbanowaniGracze().add(dajPolaczeniGracze().get(dajIndeksGraczaMP(pakiet.dajPseudonimGracza())));
        this.dajPolaczeniGracze().remove(dajIndeksGraczaMP(pakiet.dajPseudonimGracza()));
        pakiet.piszDane(this);
    }

    /**
     * Metoda zwracająca indeks gracza o podanej nazwie
     * @param pseudonim Nazwa gracza
     * @return Pozycja na liście połącoznych graczy, na której znajduje się gracz o podanej nazwie
     */
    public int dajIndeksGraczaMP(String pseudonim) {
        int index = 0;
        for (GraczMP g : this.dajPolaczeniGracze()) {
            if (g.dajPseudonim().equals(pseudonim)) {
                break;
            }
            index++;
        }
        return index;
    }

    /**
     * Metoda zwracająca indeks gracza zablokowanego o podanym adresie IP
     * @param adresLubNazwaGracza Adres IP lub nazwa gracza
     * @return Pozycja na liście zablokowanych adresów IP, na której znajduje się podany adres IP
     */
    public int dajIndeksGraczaZbanowanego(String adresLubNazwaGracza) {
        int index = 0;
        for (GraczMP g : this.dajZbanowaniGracze()) {
            if (g.dajPseudonim().equals(adresLubNazwaGracza) || g.dajAdres().getHostName().equals(adresLubNazwaGracza)) {
                break;
            }
            index++;
        }
        return index;
    }

    /**
     * Metoda zwracająca gniazdo dla podanego adresu IP i portu
     * @param ipAdres Adres IP
     * @param port Port
     * @return Gniazdo dla podanego adresu IP i portu
     * @throws IOException 
     */
    private SocketChannel dajGniazdo(InetAddress ipAdres, int port) throws IOException {
        for (SelectionKey key : dajSelector().keys()) {
            if (key.isValid() && key.channel() instanceof SocketChannel) {
                SocketChannel socket = (SocketChannel) key.channel();
                InetSocketAddress adres = (InetSocketAddress) socket.getRemoteAddress();

                if (adres.getAddress().getHostName().equalsIgnoreCase(ipAdres.getHostName()) && adres.getPort() == port) {
                    return socket;
                }
            }
        }

        return null;
    }

    /**
     * Metoda sprawdzająca czy istnieją jeszcze wolne miejsca w rozgrywce
     * @return Inforamcja o tym czy istnieją jeszcze wolne miejsca w rozgrywce
     */
    private boolean czyBrakMiejsc() {
        return dajPolaczeniGracze().size() >= maksKlientow && maksKlientow != 0;
    }

    /**
     * Metoda zwracająca pieniądze startowe serwera
     * @return Pieniądze startowe serwera
     */
    public long dajPieniadzeStartowe() {
        return pieniadzeStartowe;
    }

    /**
     * Metoda zwracająca tymczasowe pieniądze startowe serwera
     * @return Tymczasowe pieniądze startowe serwera
     */
    public long dajTymczasPieniadzeStartowe() {
        return tymczasPieniadzeStartowe;
    }

    /**
     * Metoda ustawiająca pieniądze startowe serwera
     * @param pieniadzeStartowe Pieniądze startowe serwera
     */
    public void ustawPieniadzeStartowe(long pieniadzeStartowe) {
        this.pieniadzeStartowe = pieniadzeStartowe;
    }

    /**
     * Metoda ustawiająca tymczasowe pieniądze startowe serwera
     * @param tymczasPieniadzeStartowe Tymczasowe pieniądze startowe serwera
     */
    public void ustawTymczasPieniadzeStartowe(long tymczasPieniadzeStartowe) {
        this.tymczasPieniadzeStartowe = tymczasPieniadzeStartowe;
    }

    /**
     * Metoda sprawdzjąca czy serwer działa w trybie prywatnym
     * @return Informacja o tym czy serwer działa w trybie prywatnym (true, false)
     */
    public boolean czyTrybPrywatny() {
        return czyTrybPrywatny;
    }

    /**
     * Metoda ustawiająca czy serwer ma działą w trybie prywatnym czy publicznym
     * @param czyTrybPrywatny Wartość logiczna true jeśli ma pracować w trybie prywatnym, false jest w publicznym
     */
    public void ustawCzyTrybPrywatny(boolean czyTrybPrywatny) {
        this.czyTrybPrywatny = czyTrybPrywatny;
    }

    /**
     * Metoda zwracająca czas przeznaczony na ruch
     * @return Czas przeznaczony na ruch
     */
    public long dajCzasNaRuch() {
        return czasNaRuch;
    }

    /**
     * Metoda ustawiająca czas przeznaczony na ruch
     * @param czasNaRuch Czas przeznaczony na ruch
     */
    public void ustawCzasNaRuch(long czasNaRuch) {
        this.czasNaRuch = czasNaRuch;
    }

    /**
     * Metoda zwracająca tymczasowy czas na ruch
     * @return Tymczasowy czas na ruch
     */
    public long dajTymczasCzasNaRuch() {
        return tymczasCzasNaRuch;
    }

    /**
     * Metoda ustawiająca tymczasowy czas na ruch
     * @param tymczasCzasNaRuch Tymczasowy czas na ruch 
     */
    public void ustawTymczasCzasNaRuch(long tymczasCzasNaRuch) {
        this.tymczasCzasNaRuch = tymczasCzasNaRuch;
    }

    /**
     * Metoda ustawiająca hasło dostepu do serwera gry
     * @param hasloSerwera Hasło dostepu do serwera gry 
     */
    public void ustawHasloSerwera(String hasloSerwera) {
        this.hasloSerwera = hasloSerwera;
    }

    /**
     * Metoda zwracająca maksymalną liczbę klientów na serwerze gry
     * @return Maksymalna liczba klientów
     */
    public int dajMaksKlientow() {
        return maksKlientow;
    }
    
    /**
     * Metoda ustawiająca maksymalną liczbe klientów na serwerze gry
     * @param maksKlientow Maksymalna liczba klientów
     */
    public void ustawMaksKlientow(int maksKlientow) {
        this.maksKlientow = maksKlientow;
    }

    /**
     * Metoda zwracająca selector serwera
     * @return Selector serwera
     */
    public Selector dajSelector() {
        return selector;
    }

    /**
     * Metoda zwracająca gniazdo serwera gry
     * @return Gniazdo serwera gry
     */
    public ServerSocketChannel dajChannel() {
        return channel;
    }

    /**
     * Metoda zwracająca listę połączonych graczy
     * @return Lista połączonych graczy
     */
    public List<GraczMP> dajPolaczeniGracze() {
        return polaczeniGracze;
    }

    /**
     * Metoda zwracająca listę zablokowanych adresów IP
     * @return Lista zablokowanych adresów IP
     */
    public List<GraczMP> dajZbanowaniGracze() {
        return zbanowaniGracze;
    }
    
    /** 
     * Metoda zwracająca menadżera rozgrywki
     * @return Menadżer rozgrywki
     */
    public MenadzerRozgrywki dajMenadzerRozgrywki() {
        return menadzerRozgrywki;
    }
}
