package pl.eden.ruletka.game.entities;

import java.awt.Color;
import java.io.Serializable;
import java.util.Random;

/**
 * Klasa reprezentująca gracza lokalnego
 * @author Mateusz Macięga
 */
public class Gracz implements Serializable {
    /** Aktualne pieniadze gracza */
    private long pieniadze;
    /** Pseudonim gracza */
    private String pseudonim;
    /** Kolor gracza */
    private final Color kolorGracza;
    /** Wartosc aktualnie wybranego żetonu. */
    private int wartoscWybranegoZetonu;
    /** Infomacja o tym czy gracz zakończył zakłady */
    private boolean czyZakonczylZaklady;
    
    
    /** 
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Początkowa kwota gracza
     * @param pseudonim Nazwa gracza
     * @param kolorGracza Kolor gracza
     */     
    public Gracz(long pieniadzeStartowe, String pseudonim, Color kolorGracza)
    {
        pieniadze = pieniadzeStartowe;
        this.pseudonim = pseudonim;
        this.kolorGracza = kolorGracza;
        czyZakonczylZaklady = false;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Początkowa kwota gracza
     * @param pseudonim Nazwa gracza
     * @param kolorGracza Kolor gracza
     * @param czyZakonczylZaklady Informacja o tym czy zakonczył zakłady
     */
    public Gracz(long pieniadzeStartowe, String pseudonim, Color kolorGracza, boolean czyZakonczylZaklady)
    {
        pieniadze = pieniadzeStartowe;
        this.pseudonim = pseudonim;
        this.kolorGracza = kolorGracza;
        this.czyZakonczylZaklady = czyZakonczylZaklady;
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Początkowa kwota gracza
     * @param pseudonim Nazwa gracza
     */
    public Gracz(long pieniadzeStartowe, String pseudonim)
    {
        pieniadze = pieniadzeStartowe;
        this.pseudonim = pseudonim;
        
        Random random = new Random();
        this.kolorGracza = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
        czyZakonczylZaklady = false;
    }
    
    /**
     * Metoda zwracająca pieniądze gracza
     * @return Wartość pieniędzy gracza
     */
    public long dajPieniadze() {
        return pieniadze;
    }
    
    /**
     * Metoda zwracająca nazwę gracza
     * @return Nazwa gracza
     */
    public String dajPseudonim() {
        return pseudonim;
    }
    
    /**
     * Metoda zwracająca wartość wybranego żetonu przez gracza
     * @return Wartość wybranego żetonu
     */
    public int dajWartoscWybranegoZetonu() {
        return wartoscWybranegoZetonu;
    }
    
    /**
     * Metoda ustawiająca nazwę graczowi
     * @param pseudonim Nowa nazwa gracza
     */
    public void ustawPseudonim(String pseudonim) {
        this.pseudonim = pseudonim;
    }
    
    /**
     * Metoda zwracająca kolor gracza
     * @return Kolor gracza
     */
    public Color dajKolorGracza() {
        return kolorGracza;
    }
    
    /**
     * Metoda przełączająca status informujący o tym czy gracza zakończył stawianie zakładów
     */
    public void przelaczCzyZakonczylZaklady() {
        czyZakonczylZaklady = !czyZakonczylZaklady;
    }
    
    /**
     * Metoda ustawiająca status informujący o zakończeniu stawiania zakładow przez gracza
     * @param b Wartość logiczną na jaką chcemy zmienić status
     */
    public void ustawCzyZakonczylZaklady(boolean b) {
        czyZakonczylZaklady = b;
    }
    
    /**
     * Metoda zwracająca informacje o tym czy gracz zakończył zakłady
     * @return Wartość logiczna statusu
     */
    public boolean czyZakonczylZaklady() {
        return czyZakonczylZaklady;
    }
    
    /**
     * Metoda zwracająca nazwę gracza
     * @return Nazwa gracza
     */
    @Override
    public String toString() {
        return pseudonim;
    }
    
    /**
     * Metoda ustawiająca aktualnie wybrany żeton. Wywoływana w momencie kliknięcia w żeton.
     * @param warZet Wartość wybranego żetonu.
     */
    public void ustawWartoscWybranegoZetonu(int warZet)
    {
        wartoscWybranegoZetonu = warZet;
    }
    
    public void ustawPieniadzeStartowe(long pieniadzeStartowe) {
        this.pieniadze = pieniadzeStartowe;
    }
}
