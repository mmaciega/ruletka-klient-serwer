package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.gfx.fields.Pole;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o złożeniu nowego zakładu przez gracza
 * @author Mateusz Macięga
 */
public class Pakiet09NewBet extends Pakiet {
    /** Nazwa gracza składadające zakład */
    private final  String pseudonimGracza;
    /** Wartość żetonu zakładu */
    private final  int wartoscZetonu;
    /** Instancja klasy pole na które został złożony zakład */
    private final  Pole pole;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet09NewBet(byte[] dane) {
        super(9);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.wartoscZetonu = Integer.valueOf(wiadomosc[3].substring(1));
        this.pole = Pole.stworzPole(wiadomosc[1].substring(1), wiadomosc[2].substring(1));
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pseudonimGracza Nazwa gracza składadające zakład
     * @param wartoscZetonu Wartość żetonu zakładu
     * @param pole Instancja klasy pole na które został złożony zakład
     */
    public Pakiet09NewBet(String pseudonimGracza, int wartoscZetonu, Pole pole) {
        super(9);
        this.pseudonimGracza = pseudonimGracza;
        this.wartoscZetonu = wartoscZetonu;
        this.pole = pole;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o złożeniu zakładu przez gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet09NewBet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o złożeniu zakładu przez gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet09NewBet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {       
        return ("09" + pseudonimGracza + "," + pole + ",V" + wartoscZetonu + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza składające zakład
     * @return Nazwa gracza składające zakład
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
    
    /**
     * Metoda zwracająca instancję klasy pole na które został złożony zakład
     * @return Instancja klasy pole na które został złożony zakład
     */
    public Pole dajPole() {
        return pole;
    }
    
    /**
     * Metoda zwracająca wartość żetonu zakładu
     * @return Wartość żetonu zakładu
     */
    public int dajWartoscZetonu() {
        return wartoscZetonu;
    }
}
