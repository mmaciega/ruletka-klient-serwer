package pl.eden.ruletka.game.net;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.entities.GraczMP;
import pl.eden.ruletka.game.gfx.OknoPodsumowanieZakl;
import pl.eden.ruletka.game.gfx.PanelDolny;
import pl.eden.ruletka.game.gfx.PanelGorny;
import pl.eden.ruletka.game.gfx.PanelSrodkowy;
import pl.eden.ruletka.game.gfx.fields.Pole;
import pl.eden.ruletka.game.logic.ResetGry;
import pl.eden.ruletka.game.net.packets.*;

/**
 * Klasa reprezentująca klienta gry
 * @author Mateusz Macięga
 */
public class KlientGry extends Thread {
    /** Numer portu na jaki będziemy próbować łączyć się z serwerem */
    private static int numerPortu;
    /** Adres serwera, z którym się łączymy */
    private InetAddress ipAdres;
    /** Selector do obsługi połaczenia sieciowego */
    private Selector selector; 
    /** Gniazdo klienta */
    private volatile SocketChannel gniazdo;
    /**Instancja gry */
    private final Gra gra;
    
    /** Pieniądze startowe rozgrywki */
    private long pieniadzeStartowe;
    /** Czas na ruch rozgrwyki */
    private long czasNaRuch;
    
    /** Tablica hashująca zawierająca oczekujące dane dla konkretnego gniazda */
    private final Map<SocketChannel,List<ByteBuffer>> oczekujaceDane = new HashMap<SocketChannel,List<ByteBuffer>>();
    
    /** Lista oczekujących zmian dla danego gniazda */
    private final List oczekujaceZmiany = new LinkedList();    
    
    /**
     * Konstruktor sprametryzowany
     * @param numerPortu Numer portu na jaki będziemy próbować łączyć się z serwerem
     * @param ipAdres Adres IP serwera
     * @param gra Instancja gry
     */
    public KlientGry(int numerPortu, String ipAdres, Gra gra) {
        super("Watek Klienta");
        KlientGry.numerPortu = numerPortu;
        this.gra = gra;
        try {
            this.ipAdres = InetAddress.getByName(ipAdres);
        } catch (UnknownHostException ex) {
            Logger.getLogger(KlientGry.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("[KLIENT] Uruchamiam klienta ...");
        inicjalizacjaGniazda();
        inicjalizacjaSelektora();
    }
    
    /**
     * Metoda odpowiedzialna za konfiguracją gniazda
     */
    private void inicjalizacjaGniazda() {
        try {
            gniazdo = SocketChannel.open();
            dajGniazdo().configureBlocking(false);
            dajGniazdo().connect(new InetSocketAddress(ipAdres, KlientGry.numerPortu));
            
        } catch (IOException ex) {
            Logger.getLogger(KlientGry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Metoda odpowiedzialna za konfigurację selectora
     */
    private void inicjalizacjaSelektora() {
        try {
            selector = Selector.open();
            dajGniazdo().register(selector, SelectionKey.OP_CONNECT);
        } catch (ClosedChannelException ex) {
            Logger.getLogger(KlientGry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KlientGry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Metoda odpowiedzialna za dodanie danych przekazanych jako argument do listy danych, które mają zostać wysłane 
     * @param data Dane jakie mają zostać przesłane
     * @throws IOException 
     */
    public void wyslij(byte[] data) throws IOException {	
        synchronized (this.oczekujaceZmiany) {
            // Indicate we want the interest ops set changed
            this.oczekujaceZmiany.add(new ZadanieZmiany(dajGniazdo(), ZadanieZmiany.CHANGEOPS, SelectionKey.OP_WRITE));

            // And queue the data we want written
            synchronized (this.oczekujaceDane) {
                    List queue = (List) this.oczekujaceDane.get(dajGniazdo());
                    if (queue == null) {
                            queue = new ArrayList();
                            this.oczekujaceDane.put(dajGniazdo(), queue);
                    }

                    System.out.println("[KLIENT] Dodaje do bufora: " + new String(data));
                    queue.add(ByteBuffer.wrap(data));
            }
        }

        this.selector.wakeup();
    }
    
    /**
     * Metoda obsługująca wysyłanie i odbieranie danych dla gniazda klienta
     */
    @Override
    public void run() 
    {
            while(true)
            {
                try {
                    // Przetworzenie oczekujacych zmian w kluczach selektora
                    synchronized (this.oczekujaceZmiany) {
                            Iterator changes = this.oczekujaceZmiany.iterator();
                            while (changes.hasNext()) {
                                    ZadanieZmiany change = (ZadanieZmiany) changes.next();
                                    switch (change.dajType()) {
                                    case ZadanieZmiany.CHANGEOPS:
                                            SelectionKey key = change.dajSocket().keyFor(this.selector);
                                            key.interestOps(change.dajOps());
                                    }
                            }
                            this.oczekujaceZmiany.clear();
                    }

                    this.selector.select();
                    Iterator selectedKeys = this.selector.selectedKeys().iterator();

                    while (selectedKeys.hasNext()) {
                        SelectionKey key = (SelectionKey) selectedKeys.next();
                        selectedKeys.remove();

                        if (!key.isValid())
                        {
                            continue;
                        }

                        if(key.isConnectable()) {
                            this.obsluzLaczenie(key);
                        }
                        else if (key.isWritable()) {
                            this.obsluzZapis(key);
                        }
                        else if (key.isReadable()) {
                            this.obsluzOdczyt(key);
                        }
                    }
                } catch (Exception e) {
                            e.printStackTrace();
                }
            }
    }   


    /**
     * Metoda odpowiedzialna za obsłużenie próby połączenia z serwerem
     * @param key Klucz selectora który próbuje obsłużyc łączenie
     * @throws IOException 
     */
    private void obsluzLaczenie(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        try {
                socketChannel.finishConnect();
        } catch (IOException e) {
                System.out.println("Nie udało się połączyć z serwerem");
                JOptionPane.showMessageDialog(gra, "Nie udało się połączyć z serwerem", "Brak połączenia", JOptionPane.ERROR_MESSAGE);
                gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
        }

        System.out.println("[KLIENT] Udalo sie nawiazac polaczenie z serwerem " + ipAdres.getHostName() + ":" + numerPortu);
        Pakiet00Login loginPakiet = new Pakiet00Login(gra.dajGracza());
        loginPakiet.piszDane(this);
        key.interestOps(SelectionKey.OP_WRITE);
    }

    /**
     * Metoda odpowiedzialna za zapisanie danych do gniazda
     * @param key Klucz selectora, do którego chcemy zapisaćdane
     */
    private void obsluzZapis(SelectionKey key) {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        synchronized (this.oczekujaceDane) {
                List queue = (List) this.oczekujaceDane.get(socketChannel);

                // Write until there's not more data ...
                while (!queue.isEmpty()) {
                    try {
                        ByteBuffer buf = (ByteBuffer) queue.get(0);
                        System.out.println("[KLIENT] Wysylam polecenie: " + new String(buf.array()));
                        socketChannel.write(buf);
                        if (buf.remaining() > 0) {
                            break;
                        }
                        queue.remove(0);
                    } catch (IOException ex) {
                        Logger.getLogger(KlientGry.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (queue.isEmpty()) {
                        key.interestOps(SelectionKey.OP_READ);
                }
        }    
    }

    /**
     * Metoda odpowiedzialna za obsłużenie odczytu danych
     * @param key Klucz selectora, który otrzymał dane do odczytu
     * @throws IOException 
     */
    private void obsluzOdczyt(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        ByteBuffer buforOdczytu = ByteBuffer.allocate(1024);
        
        // Clear out our read buffer so it's ready for new data
        buforOdczytu.clear();

        // Attempt to read off the channel
        int numRead = -1;
        try {
                numRead = socketChannel.read(buforOdczytu);
        } catch (IOException e) {
                // nagle rozlaczenie ze strony klienta
                key.cancel();
                socketChannel.close();
                
                JOptionPane.showMessageDialog(gra, "Nagle utracono połączenie z serwerem", "Nagłe rozłączenie", JOptionPane.ERROR_MESSAGE);
                System.out.println("[KLIENT] Nagła utrata polaczenia z serwerem");
                
                //zamykamy aplikację
                gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
        }

        if (numRead == -1) {
                //prawidlowe zamkniecie polaczenie ze strony klienta
                socketChannel.close();
                key.cancel();
                
                JOptionPane.showMessageDialog(gra, "Serwer został wyłączony. Rozłączono.", "Rozłączenie", JOptionPane.ERROR_MESSAGE);
                System.out.println("[KLIENT] Utrata polaczenia z serwerem");

                //zamykamy aplikację
                gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
        }

        parsujPakiet(buforOdczytu.array(), socketChannel);
    }

    /**
     * Metoda odpowiedzialna za parsowanie otrzymanych pakietów
     * @param data Otrzymane dane
     * @param socketChannel Gniazdo, które otrzymało dane
     * @throws IOException 
     */
    private void parsujPakiet(byte[] data, SocketChannel socketChannel) throws IOException {
        String wiadomosc = new String(data).trim();
        String polecenia[] = wiadomosc.split("K");
        
        for (String polecenie : polecenia) {
            polecenie+="K";
            data = polecenie.getBytes();
            
            Pakiet.TypyPakietu typ = Pakiet.znajdzTypPakietu(polecenie.substring(0, 2));
            Pakiet pakiet;
            InetSocketAddress adres;
            GraczMP gracz;
            
            if(typ != Pakiet.TypyPakietu.TIME) {
                System.out.println("[KLIENT] Otrzymalem polecenie: " + new String(data));
            }
            
            switch(typ) {
                default:
                case INVALID:
                    break;
                    
                case LOGIN:
                    pakiet = new Pakiet00Login(data);
                    adres = (InetSocketAddress) socketChannel.getRemoteAddress();
                    System.out.println("[KLIENT] [" + adres.getAddress().getHostName()+ ":" + adres.getPort() +"] " + ((Pakiet00Login)pakiet).dajPseudonimGracza() + " dołączył do gry");
                        
                    gracz = new GraczMP(((Pakiet00Login)pakiet).dajPieniadzeGracza(), ((Pakiet00Login)pakiet).dajPseudonimGracza(), 
                            ((Pakiet00Login)pakiet).dajKolorGracza(), adres.getAddress(), adres.getPort(), ((Pakiet00Login)pakiet).czyZakonczylZaklady());
                    gra.dodajGracza(gracz);

                    break;
                    
                case DISCONNECT:
                    pakiet = new Pakiet01Disconnect(data);
                    System.out.println("[KLIENT] Rozlaczyl sie gracz " + ((Pakiet01Disconnect)pakiet).dajPseudonimGracza());
                    gra.usunGracza(((Pakiet01Disconnect)pakiet).dajPseudonimGracza());
                    
                    String komunikat = "Opuścił grę gracz o pseudonimie " + ((Pakiet01Disconnect)pakiet).dajPseudonimGracza();
                    PanelDolny.dajInstancje().wprowadzKomunikat(komunikat);
                    break;
                    
                case NEWPLAYER:
                    pakiet = new Pakiet02Newplayer(data);
                    adres = (InetSocketAddress) socketChannel.getRemoteAddress();
                    
                    gracz = new GraczMP(dajPieniadzeStartowe(), ((Pakiet02Newplayer)pakiet).dajPseudonimGracza(), ((Pakiet02Newplayer)pakiet).dajKolorGracza(), adres.getAddress(), adres.getPort());
                    gra.dodajGracza(gracz);
                    PanelDolny.dajInstancje().wprowadzKomunikat("Dołączył gracz o pseudonimie " + gracz.dajPseudonim());
                    break;
                    
                case MESSAGE:
                    pakiet = new Pakiet03Message(data);
                    PanelDolny.dajInstancje().wprowadzKomunikat((Pakiet03Message) pakiet);
                    break;
                    
                case CONFIGURATION:
                    pakiet = new Pakiet04Configuration(data, gra.dajListeGraczy());
                    
                    gra.ustawMenadzerZakladow(((Pakiet04Configuration)pakiet).dajMenadzerZakladow());
                    gra.dajMenadzerZakladow().aktualizujWszystkiePola(gra.dajGracza());
                    
                    pieniadzeStartowe = ((Pakiet04Configuration)pakiet).dajPieniadzeStartowe();
                    gra.dajGracza().ustawPieniadzeStartowe(dajPieniadzeStartowe());
                    gra.dodajGracza(gra.dajGracza());
                    gra.setTitle(Gra.NAZWA + " [Nick: " + gra.dajGracza().dajPseudonim() + "]" + ( gra.dajSerwerGry() != null ? " - serwer gry" : "" ));
                    
                    gra.setVisible(true);
                    ((PanelDolny) PanelDolny.dajInstancje()).revalidate();
                    ((PanelGorny) PanelGorny.dajInstancje()).ustawCzasNaRuch(((Pakiet04Configuration)pakiet).dajCzasNaRuch());
                    break;
                    
                case KICKPLAYER:
                    pakiet = new Pakiet05KickPlayer(data);
                    String powodWyrzucenia = ((Pakiet05KickPlayer)pakiet).dajPowodWyrzucenia();

                    if( ((Pakiet05KickPlayer)pakiet).dajPseudonimGracza().equals(gra.dajGracza().dajPseudonim()) ) {
                        System.out.println("[KLIENT] Zostałeś wyrzucony z gry");
                        JOptionPane.showMessageDialog(gra, "Zostałeś wyrzucony z gry.\nPowód: " 
                                + ( powodWyrzucenia.equals("") ? "niezdefinowano" : powodWyrzucenia), 
                                "Zostałeś wyrzucony z gry", JOptionPane.INFORMATION_MESSAGE);
                        gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
                    } else {
                        System.out.println("[KLIENT] Gracz " + ((Pakiet05KickPlayer)pakiet).dajPseudonimGracza() + "został wyrzucony.");
                        gra.usunGracza(((Pakiet05KickPlayer)pakiet).dajPseudonimGracza());
                        PanelDolny.dajInstancje().wprowadzKomunikat("Gracz o pseudonimie " + ((Pakiet05KickPlayer)pakiet).dajPseudonimGracza() 
                                + " został wyrzucony. Powód: "  + ( powodWyrzucenia.equals("") ? "niezdefinowano" : powodWyrzucenia));
                    }
                    break;
                    
                case BANPLAYER:
                    pakiet = new Pakiet06BanPlayer(data);
                    String powodZbanowania = ((Pakiet06BanPlayer)pakiet).dajPowodZbanowania();

                    if( ((Pakiet06BanPlayer)pakiet).dajPseudonimGracza().equals(gra.dajGracza().dajPseudonim()) ) {
                        System.out.println("[KLIENT] Zostałeś zbanowany");
                        JOptionPane.showMessageDialog(gra, "Zostałeś zbanowany.\nPowód: " 
                                + ( powodZbanowania.equals("") ? "niezdefinowano" : powodZbanowania), 
                                "Zostałeś zbanowany", JOptionPane.INFORMATION_MESSAGE);
                        gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
                    } else {
                        System.out.println("[KLIENT] Gracz " + ((Pakiet06BanPlayer)pakiet).dajPseudonimGracza() + "został zbanowany.");
                        gra.usunGracza(((Pakiet06BanPlayer)pakiet).dajPseudonimGracza());
                        PanelDolny.dajInstancje().wprowadzKomunikat("Gracz o pseudonimie " + ((Pakiet06BanPlayer)pakiet).dajPseudonimGracza() 
                                + " został zbanowany. Powód: "  + ( powodZbanowania.equals("") ? "niezdefinowano" : powodZbanowania));
                    }
                    break;
                    
                case LOGINDENIED:
                    pakiet = new Pakiet07LoginDenied(data);
                    
                    if (((Pakiet07LoginDenied)pakiet).czyBrakMiejsc())
                    {
                        JOptionPane.showMessageDialog(gra, "Serwer gry jest pełny.", "Serwer pełny", JOptionPane.INFORMATION_MESSAGE);
                        System.out.println("[KLIENT] Serwer gry jest pełny.");
                        //zamykamy aplikację
                        gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
                    }
                    
                    if (((Pakiet07LoginDenied)pakiet).czyZbanowany())
                    {
                        JOptionPane.showMessageDialog(gra, "Twój adres IP jest zbanowany. Odrzucono połączenie.", "Adres IP zbanoowany", JOptionPane.INFORMATION_MESSAGE);
                        System.out.println("[KLIENT] Adres IP klienta jest zbanowany.");
                        //zamykamy aplikację
                        gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
                    }
                    
                    String hasloSerwera = "";
                    if (((Pakiet07LoginDenied)pakiet).czyZleHaslo())
                    {
                        JPanel panel = new JPanel();
                        JLabel label = new JLabel("Wprowadz hasło serwera:");
                        JPasswordField pass = new JPasswordField(10);
                        panel.add(label);
                        panel.add(pass);
                        String[] opcje = new String[]{"OK", "Cancel"};
                        int opcja = JOptionPane.showOptionDialog(null, panel, "Serwer prywatny. Wymagane hasło. Podaj hasło: ",
                                 JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                                 null, opcje, opcje[1]);

                        if(opcja == 0) {
                            hasloSerwera = new String(pass.getPassword());
                        } else {
                            gra.dispatchEvent(new WindowEvent(gra, WindowEvent.WINDOW_CLOSING));
                        }
                    }
                    
                    if (((Pakiet07LoginDenied)pakiet).czyPseudonimZajety())
                    {
                        String pseudonim = null;
                        do {
                            pseudonim = JOptionPane.showInputDialog("Pseudonim jest zajęty poodaj inny (max 15 znakow)");
                        } while (pseudonim == null || pseudonim.isEmpty() || pseudonim.length() > 15);
                        
                        gra.dajGracza().ustawPseudonim(pseudonim);
                    }
                                         
                    pakiet = new Pakiet00Login(gra.dajGracza(), hasloSerwera);
                    pakiet.piszDane(this);
                    break;
                    
                case RESTART:
                    pakiet = new Pakiet08Restart(data);
                    pieniadzeStartowe = ((Pakiet08Restart) pakiet).dajPieniadzeStartowe();
                    czasNaRuch = ((Pakiet08Restart) pakiet).dajCzasNaRuch();
                    ResetGry reset = new ResetGry(gra);
                    reset.uruchomZadanie();
                    break;
                    
                case NEWBET:
                    pakiet = new Pakiet09NewBet(data);
                    gra.dajMenadzerZakladow().dodajZaklad((Pakiet09NewBet) pakiet);
                    
                    Pole pole = ((PanelSrodkowy) PanelSrodkowy.dajInstancje()).dajPole(((Pakiet09NewBet)pakiet).dajPole());
                    gracz = (GraczMP) gra.dajListeGraczy().getElementAt( gra.dajIndeksGraczaMP(((Pakiet09NewBet)pakiet).dajPseudonimGracza()) );
                    gra.zmodyfikujPieniadzeGracza(gracz, gracz.dajPieniadze() - ((Pakiet09NewBet)pakiet).dajWartoscZetonu());
                    PanelGorny.dajInstancje().repaint();

                    if (gra.dajGracza().dajPseudonim().equals(gracz.dajPseudonim()) || !gra.dajMenadzerZakladow().czyPoleZajetePrzezGracza(gra.dajGracza(), pole)) {
                        pole.dodajZeton(gracz.dajKolorGracza(), gracz.dajPseudonim().equals(gra.dajGracza().dajPseudonim()));
                    }
                    
                    if (gra.dajGracza().dajPseudonim().equals(gracz.dajPseudonim())) {
                        pole.setToolTipText("Kwota: " + gra.dajMenadzerZakladow().wartoscZakladow(gracz, pole) + "$");
                        
                        //dynamiczna aktualizacja 
                        Point locationOnScreen = MouseInfo.getPointerInfo().getLocation();
                        if(locationOnScreen != null) {
                        Point locationOnComponent = new Point(locationOnScreen);
                        SwingUtilities.convertPointFromScreen(locationOnComponent, pole);
                        if (pole.contains(locationOnComponent)) {
                            ToolTipManager.sharedInstance().mouseMoved(
                            new MouseEvent(pole, -1, System.currentTimeMillis(), 0, locationOnComponent.x, locationOnComponent.y,
                                    locationOnScreen.x, locationOnScreen.y, 0, false, 0));
                        }
                        }
                    }
                    break;
                    
                case RESETBETS:
                    pakiet = new Pakiet10ResetBets(data);
                    gracz = (GraczMP) gra.dajListeGraczy().getElementAt( gra.dajIndeksGraczaMP(((Pakiet10ResetBets)pakiet).dajPseudonimGracza()) );
                    int wartoscZakladow = gra.dajMenadzerZakladow().wartoscZakladow(gracz);
                    
                    gra.zmodyfikujPieniadzeGracza(gracz, gracz.dajPieniadze() + wartoscZakladow);
                    gra.dajMenadzerZakladow().usunZakladyGracza(gracz);
                    
                    if(gracz.dajPseudonim().equals(gra.dajGracza().dajPseudonim())) {
                        gra.dajGracza().ustawPieniadzeStartowe(gracz.dajPieniadze());
                        PanelGorny.dajInstancje().repaint();
                        ((PanelSrodkowy) PanelSrodkowy.dajInstancje()).wyczyscWszystkiePola();
                    } else {
                        ((PanelSrodkowy) PanelSrodkowy.dajInstancje()).wyczyscWszystkiePola(gra.dajGracza());
                    }
                    gra.dajMenadzerZakladow().aktualizujWszystkiePola(gra.dajGracza());
                    break;
                    
                case FINISHEDBETS:
                    pakiet = new Pakiet11FinishedBets(data); 
                    gracz = (GraczMP) gra.dajListeGraczy().getElementAt( gra.dajIndeksGraczaMP(((Pakiet11FinishedBets)pakiet).dajPseudonimGracza()) );
                    gra.zmodyfikujZakonczenieZaklGracza(gracz, true);
                    break;
                    
                case RESULTS:
                    pakiet = new Pakiet12Results(data);
                    Map<String,Long[]> zwyciescyGracze = ((Pakiet12Results)pakiet).dajZwyciescyGracze();
                    
                    String pseudonimGracza;
                    for (Map.Entry<String, Long[]> pozycja : zwyciescyGracze.entrySet())
                    {
                        pseudonimGracza = pozycja.getKey();
                        gracz = (GraczMP) gra.dajListeGraczy().getElementAt( gra.dajIndeksGraczaMP(pseudonimGracza));
                        gra.zmodyfikujPieniadzeGracza(gracz, gracz.dajPieniadze() + pozycja.getValue()[0] + pozycja.getValue()[1]);
                        
                        if(gracz.dajPseudonim().equals(gra.dajGracza().dajPseudonim())) {
                            ((OknoPodsumowanieZakl) gra.getGlassPane()).ustawWartoscWygranej(pozycja.getValue()[0]);
                            PanelGorny.dajInstancje().repaint();
                        }
                    }
                    
                    for(Gracz g : gra.dajListeGraczy()) {
                        gra.zmodyfikujZakonczenieZaklGracza(g, false);
                    }
                    
                    gra.dajMenadzerZakladow().usunWszystkieZaklady();
                    PanelSrodkowy.dajInstancje().wyczyscWszystkiePola(); 
                    
                    ((OknoPodsumowanieZakl) gra.getGlassPane()).ustawWylosowanePole(((Pakiet12Results)pakiet).dajWylosowanePole());
                    gra.getGlassPane().setVisible(false);
                    gra.getGlassPane().setVisible(true);
                    
                    PanelSrodkowy.dajInstancje().dodajWylosowanePole(((Pakiet12Results)pakiet).dajWylosowanePole());
                    PanelGorny.dajInstancje().zakrecRuletka();
                    
                    gra.ustawFlagaWait(true);
                    break;
                    
                case TIME:
                    pakiet = new Pakiet13Time(data);
                    PanelGorny.dajInstancje().ustawCzasNaRuch(((Pakiet13Time) pakiet).dajPozostalyCzasNaRuch());
                    PanelGorny.dajInstancje().repaint();
                    break;
                    
                case NEWGAME:
                    pakiet = new Pakiet14NewGame(data);
                    gracz = (GraczMP) gra.dajListeGraczy().getElementAt( gra.dajIndeksGraczaMP(((Pakiet14NewGame) pakiet).dajPseudonimGracza()));
                    gra.zmodyfikujPieniadzeGracza(gracz, pieniadzeStartowe);
                    break;
            }
        }
    }

    
    
    /**
     * Metoda zwracająca gniazdo klienta
     * @return Gniazdo klienta
     */
    public SocketChannel dajGniazdo() {
        return gniazdo;
    }

    /**
     * Metoda zwracająca pieniądze startowe rozgrywki
     * @return Pieniądze startowe rozgrywki
     */
    public long dajPieniadzeStartowe() {
        return pieniadzeStartowe;
    }
}
