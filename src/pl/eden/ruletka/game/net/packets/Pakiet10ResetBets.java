package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o usunięciu wszystkich zakładów przez gracza
 * @author Mateusz Macięga
 */
public class Pakiet10ResetBets extends Pakiet {
    /** Nazwa gracza usuwającego swoje zakłady */
    private final String pseudonimGracza;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet10ResetBets(byte[] dane) {
        super(10);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
    }

    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracza usuwającego swoje zakłady
     */
    public Pakiet10ResetBets(Gracz gracz) {
        super(10);
        this.pseudonimGracza = gracz.dajPseudonim();
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o usunięciu wszystkich zakładów przez gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet10ResetBets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o usunięciu wszystkich zakładów przez gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet10ResetBets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("10" + pseudonimGracza +  "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza usuwającego swoje zakłady
     * @return Nazwa gracza usuwającego swoje zakłady
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
}
