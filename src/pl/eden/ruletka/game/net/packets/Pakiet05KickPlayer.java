package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o wyrzuceniu gracza z rozgrywki
 * @author Mateusz Macięga
 */
public class Pakiet05KickPlayer extends Pakiet {
    /** Nazwa gracza, który został wyrzucony */
    private final String pseudonimGracza;
    /** Powód wyrzucenia gracza */
    private final String powodWyrzucenia;
    
    /**
     * Konstruktor sprametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet05KickPlayer(byte[] dane) {
        super(05);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
        this.powodWyrzucenia = wiadomosc[1].substring(1);
    }
    
    /**
     * Konstruktor sprametryzowany
     * @param gracz Instancja gracza wyrzuconego
     * @param powodWyrzucenia Powód wyrzucenia
     */
    public Pakiet05KickPlayer(Gracz gracz, String powodWyrzucenia) {
        super(05);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.powodWyrzucenia = powodWyrzucenia;
    }
    
    /**
     * Konstruktor sprametryzowany
     * @param gracz Instancja gracza wyrzuconego
     */
    public Pakiet05KickPlayer(Gracz gracz) {
        super(05);
        this.pseudonimGracza = gracz.dajPseudonim();
        this.powodWyrzucenia = "";
    }
    
    /**
     * Konstruktor sprametryzowany
     * @param pseudonimGracza Nazwa gracza wyrzuconego
     * @param powodWyrzucenia Powód wyrzucenia gracza
     */
    public Pakiet05KickPlayer(String pseudonimGracza, String powodWyrzucenia) {
        super(05);
        this.pseudonimGracza = pseudonimGracza;
        this.powodWyrzucenia = powodWyrzucenia;
    }
    
    /**
     * Konstruktor sprametryzowany
     * @param pseudonimGracza Nazwa gracza wyrzuconego
     */
    public Pakiet05KickPlayer(String pseudonimGracza) {
        super(05);
        this.pseudonimGracza = pseudonimGracza;
        this.powodWyrzucenia = "";
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o wyrzuceniu gracza z rozgrywki
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet05KickPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o wyrzuceniu gracza z rozgrywki
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet05KickPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("05" + pseudonimGracza + ",R" + powodWyrzucenia + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwę gracza wyrzuconego
     * @return Nazwa gracza wyrzuconego
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
    
    /**
     * Metoda zwracająca powód wyrzucenia gracza
     * @return Powód wyrzucenia gracza
     */
    public String dajPowodWyrzucenia() {
        return powodWyrzucenia;
    }
}
