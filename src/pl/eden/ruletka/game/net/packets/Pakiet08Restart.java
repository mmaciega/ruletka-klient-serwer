package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o rozpoczęciu rozgrywki od nowa
 * @author Mateusz Macięga
 */
public class Pakiet08Restart extends Pakiet {
    /** Wartość pieniędzy startowych */
    private long pieniadzeStartowe;
    /** Czas przeznaczony na ruch */
    private long czasNaRuch;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet08Restart(byte[] dane) {
        super(8);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        try {
        this.pieniadzeStartowe = Long.parseLong(wiadomosc[0].substring(1));
        this.czasNaRuch = Long.parseLong(wiadomosc[1].substring(1));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Konstruktor sparametryzowany
     * @param pieniadzeStartowe Pieniądze startowe rozgrywki
     * @param czasNaRuch Czas przeznaczony na ruch
     */
    public Pakiet08Restart(long pieniadzeStartowe, long czasNaRuch) {
        super(8);
        this.pieniadzeStartowe = pieniadzeStartowe;
        this.czasNaRuch = czasNaRuch;
    }

    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o rozpoczęciu rozgrywki od nowa
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet08Restart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o rozpoczęciu rozgrywki od nowa
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet08Restart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("08M" + pieniadzeStartowe + ",T" + czasNaRuch + "K").getBytes();
    }
    
    /**
     * Metoda zwracająca pieniądze startowe rozgrywki
     * @return Pieniądze startowe rozgrywki
     */
    public long dajPieniadzeStartowe() {
        return pieniadzeStartowe;
    }
    
    /**
     * Metoda zwracająca czas przeznaczony na ruch
     * @return Czas przeznaczony na ruch
     */
    public long dajCzasNaRuch() {
        return czasNaRuch;
    }
}
