package pl.eden.ruletka.game.net.packets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.net.KlientGry;
import pl.eden.ruletka.game.net.SerwerGry;

/**
 * Klasa reprezentująca pakiet służący do informowania klientów o zakończeniu składania zakładów przez gracza
 * @author Mateusz Macięga
 */
public class Pakiet11FinishedBets extends Pakiet {
    /** Nazwa gracza, który zakończył składanie zakładów */
    private final String pseudonimGracza;
    
    /**
     * Konstruktor sparametryzowany
     * @param dane Pakiet w formie tablicy bajtów, z którego ma powstać obiekt pakietu
     */
    public Pakiet11FinishedBets(byte[] dane) {
        super(11);
        String[] wiadomosc = czytajDane(dane).split(",");
        
        this.pseudonimGracza = wiadomosc[0];
    }

    /**
     * Konstruktor sparametryzowany
     * @param gracz Instancja gracz, który zakończył składanie zakładów
     */
    public Pakiet11FinishedBets(Gracz gracz) {
        super(11);
        this.pseudonimGracza = gracz.dajPseudonim();
    }
    
    /**
     * Metoda służaca do przesłania do serwera pakietu z informacją o zakończeniu składania zakładów przez gracza
     * @param klient Gniazdo klienta gry, z którego ma zostać wysłany pakiet
     */
    @Override
    public void piszDane(KlientGry klient) {
        try {
            klient.wyslij(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet11FinishedBets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda służaca do przesłania z serwera do wszystkich klientów pakietu z informacją o zakończeniu składania zakładów przez gracza
     * @param serwer Gniazdo serwera gry, z którego ma zostać przesłany pakiet
     */
    @Override
    public void piszDane(SerwerGry serwer) {
        try {
            serwer.wyslijDaneDoWszystkichKlientow(dajDane());
        } catch (IOException ex) {
            Logger.getLogger(Pakiet11FinishedBets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zwracająca dane pakietu w formie tabeli bajtów
     * @return Tabela bajtów pakietu
     */
    @Override
    public byte[] dajDane() {
        return ("11" + pseudonimGracza +  "K").getBytes();
    }
    
    /**
     * Metoda zwracająca nazwe gracza, który zakończył składanie zakładów
     * @return Nazwa gracza, który zakończył składanie zakładów
     */
    public String dajPseudonimGracza() {
        return pseudonimGracza;
    }
}
