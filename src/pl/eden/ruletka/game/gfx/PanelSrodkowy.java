package pl.eden.ruletka.game.gfx;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import static java.awt.GridBagConstraints.FIRST_LINE_START;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import pl.eden.ruletka.game.Gra;
import pl.eden.ruletka.game.entities.Gracz;
import pl.eden.ruletka.game.gfx.fields.Pole;
import pl.eden.ruletka.game.gfx.fields.PoleGraniczne;
import pl.eden.ruletka.game.gfx.fields.PoleNumeryczne;
import pl.eden.ruletka.game.gfx.fields.PoleSpecjalneKolor;
import pl.eden.ruletka.game.gfx.fields.PoleSpecjalneParzystosc;
import pl.eden.ruletka.game.gfx.fields.PoleSpecjalnePolowka;
import pl.eden.ruletka.game.gfx.fields.PoleSpecjalneTuzin;
import pl.eden.ruletka.game.gfx.fields.PoleSpecjalneWiersz;
import pl.eden.ruletka.game.net.packets.Pakiet09NewBet;

/**
 * Klasa reprezentująca panel środkowy z planszą z polami oraz historią gry
 * @author Mateusz Macięga
 */
public class PanelSrodkowy extends JPanel {
    /** Panel z polami do składania zakładów */
    private final JPanel panelZPolami = new PanelZPolami();
    /** Panel z sześcioma ostatatnimi wylosowanymi liczbami */
    private final JPanel panelZHistoria = new PanelZHistoria();
    
    /**
     * Konstruktor prywatny. Zastosowano wzorzec Singleton
     */
    private PanelSrodkowy() {
        setOpaque(false);
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0, 0, 0, 30));
                
        add(panelZPolami, BorderLayout.CENTER);
        add(panelZHistoria, BorderLayout.LINE_END);
    }
    
    /**
     * Metoda zwracająca instancję klasy
     * @return Instancja klasy PanelSrodkowy
     */
    public static PanelSrodkowy dajInstancje() {
        return PanelSrodkowyInstancja.INSTANCJA;
    }
    
    /**
     * Metoda statyczna tworząca instację klasy
     */
    private static class PanelSrodkowyInstancja {
        private static final PanelSrodkowy INSTANCJA = new PanelSrodkowy();
    }
    
    /**
     * Metoda zwracająca instancję pola na planszy zgodnego z polem przekazanym jako argument
     * @param pole Pole, którego instancję chcemy znaleźć na liście pól
     * @return 
     */
    public Pole dajPole(Pole pole) {
        for(Pole p : ((PanelZPolami)panelZPolami).listaPol) {
            if(p.equals(pole)) {
                return p;
            }
        }
        
        return null;
    }
    
    /**
     * Metoda odpowiedzialna za wyczyszczenie wszystkich pól z zakładów
     */
    public void wyczyscWszystkiePola() {
        for(Pole p : ((PanelZPolami)panelZPolami).listaPol) {
            p.wyczyscPole();
        }
    }
    
    /**
     * Metoda odpowiedzialna za wyczyszczenie wszystkich zakładów gracza przekazanego jako argument
     * @param gracz Gracz, którego zakłady usuwamy
     */
    public void wyczyscWszystkiePola(Gracz gracz) {
        Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
        for(Pole p : ((PanelZPolami)panelZPolami).listaPol) {
            if (!gra.dajMenadzerZakladow().czyPoleZajetePrzezGracza(gracz, p)) {
                p.wyczyscPole();
            }
        }
    }
    
    /**
     * Metoda odpowiedzialna za dodanie wylosowanej wartości do historii losowań
     * @param pole Wartość wylosowanego pola przez koło ruletki
     */
    public void dodajWylosowanePole(Integer pole) {
        ((PanelZHistoria)panelZHistoria).dodajWylosowanePole(pole);
    }
    
    /**
     * Klasa reprezentująca panel z planszą z pola do stawiania zakładów
     */
    class PanelZPolami extends JPanel implements MouseListener {
        /** Lista dostępnych pól */
        private List<Pole> listaPol = new ArrayList<>();
        
        /**
         * Konstruktor domyślny
         */
        public PanelZPolami() {
        setOpaque(false);
        setLayout(new GridBagLayout());
        
        umieszczeniePol();
        }
        
        /**
         * Metoda odpowiedzialna za obsługę zdarzenia kliknięcia w pole 
         * @param me 
         */
        @Override
        public void mouseClicked(MouseEvent me) {
            Pole pole = (Pole) me.getSource();
            Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
            
            if(!gra.czyFlagaWait()) {
                if(gra.dajGracza().dajWartoscWybranegoZetonu() != 0 && gra.dajGracza().dajPieniadze() - gra.dajGracza().dajWartoscWybranegoZetonu()  >= 0) {
                    Pakiet09NewBet pakiet = new Pakiet09NewBet(gra.dajGracza().dajPseudonim(), gra.dajGracza().dajWartoscWybranegoZetonu(), pole);
                    pakiet.piszDane(gra.dajGniazdoKlienta());
                } else if (gra.dajGracza().dajWartoscWybranegoZetonu() == 0) {
                    ((PanelDolny) PanelDolny.dajInstancje()).wprowadzKomunikat("Aby dodać zakład należy wybrać żeton");
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent me) {
        }

        @Override
        public void mouseReleased(MouseEvent me) {
        }

        /**
         * Metoda odpowiedzialna za obsługę zdarzenia najechania na pole. Najechanie na pole powoduje wyświetlenie półprzeźroczystej grafiki żetonu.
         * @param me 
         */
        @Override
        public void mouseEntered(MouseEvent me) {
            Gra gra = (Gra) ((JFrame) SwingUtilities.getWindowAncestor(this));
            if(!gra.czyFlagaWait()) { 
                JLabel pole = ((JLabel) me.getSource());

                BufferedImage obrazPolaZZetonami = new BufferedImage((int) (((Pole)pole).dajObrazekPolaZZetonami().getIconWidth()), (int) (((Pole)pole).dajObrazekPolaZZetonami().getIconHeight()), 
                        BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2D = obrazPolaZZetonami.createGraphics();  
                g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);         

                Image zeton;
                BufferedImage resizedImage = null;
                try {
                    zeton = ImageIO.read(getClass().getResourceAsStream("/stol2/zeton.png"));
                    resizedImage = new BufferedImage(zeton.getWidth(null), zeton.getHeight(null), TYPE_INT_ARGB);

                    Graphics2D g = resizedImage.createGraphics();
                    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
                    g.drawImage(zeton, 0, 0, zeton.getWidth(null), zeton.getHeight(null), null);
                    g.dispose();
                } catch (IOException ex) {
                    Logger.getLogger(PanelSrodkowy.class.getName()).log(Level.SEVERE, null, ex);
                }

                g2D.setColor(Color.black);
                g2D.drawImage(((Pole)pole).dajObrazekPolaZZetonami().getImage(), 0,0, null);
                zeton = Toolkit.getDefaultToolkit().createImage(resizedImage.getSource());

                g2D.drawImage(zeton, (int) ((((Pole)pole).dajObrazekPolaZZetonami().getImage().getWidth(null)/2) - ((zeton.getWidth(null) * 0.3))/2), (int) ((((Pole)pole).dajObrazekPolaZZetonami().getImage().getHeight(null)/2) - ((zeton.getHeight(null) * 0.3))/2), (int) (zeton.getWidth(null) * 0.3), (int) (zeton.getHeight(null) * 0.3),null);
                pole.setIcon(new ImageIcon((Image) obrazPolaZZetonami));
            }
        }

        /**
         * Metoda odpowiedzialna za obsługę zdarzenia wyjechania z pola. Powoduje usunięcie półprzeźroczystej grafiki żetonu z pola.
         * @param me 
         */
        @Override
        public void mouseExited(MouseEvent me) {
            JLabel pole = ((JLabel) me.getSource());
            pole.setIcon(((Pole)pole).dajObrazekPolaZZetonami());
        }

        /**
         * Metoda odpowiedzialna za umieszczenie pól na planszy
         */
        private void umieszczeniePol() {
            GridBagConstraints c = new GridBagConstraints();
            ElementGraficzny pole;

            //POLE 0                   
            c.gridx = 0;
            c.gridy = 0;
            c.gridheight = 7;
            pole = new PoleNumeryczne(0, "stol2/0_2.png");

            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            c.gridx = 1;
            c.gridheight = 1;
            int liczba = 3;
            for(int y = 1; y<=5; y+=2) {
                c.gridy=y;
                pole = new PoleGraniczne("stol2/pasek_pionowy.png", new int[]{0,liczba});
                pole.addMouseListener(this);
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                listaPol.add((Pole) pole);
                add(pole,c);

                --liczba;
            }

            c.gridy = 0;
            pole = new PoleGraniczne("stol2/naroznik1.png", new int[]{0,1,2,3});
            pole.addMouseListener(this);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            listaPol.add((Pole) pole);            
            add(pole,c); 

            c.gridy = 2;
            pole = new PoleGraniczne("stol2/naroznik3.png", new int[]{0,2,3});
            pole.addMouseListener(this);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            listaPol.add((Pole) pole);
            add(pole,c); 

            c.gridy = 4;
            pole = new PoleGraniczne("stol2/naroznik3.png", new int[]{0,1,2});
            pole.addMouseListener(this);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            listaPol.add((Pole) pole);
            add(pole,c); 

            c.gridy = 6;
            pole = new ElementGraficzny("stol2/naroznik2.png");
            add(pole,c); 

            //POLA NUMERYCZNE 1-36
            int x = 2;
            for(int i=1; i<37; i = i+3)
            {
                c.gridx = x;
                int wartosc = i;

                c.gridy = 6;
                pole = new ElementGraficzny("stol2/linia_pozioma.png");
                add(pole, c);
                
                c.gridy -= 2;
                pole = new PoleGraniczne("stol2/linia_pozioma.png", new int[]{wartosc, wartosc+1});
                pole.addMouseListener(this);
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                listaPol.add((Pole) pole);
                add(pole,c);
                
                c.gridy -= 2;
                pole = new PoleGraniczne("stol2/linia_pozioma.png", new int[]{wartosc+1, wartosc+2});
                pole.addMouseListener(this);
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                listaPol.add((Pole) pole);
                add(pole,c);
                
                c.gridy -= 2;
                pole = new PoleGraniczne("stol2/linia_pozioma.png", new int[]{wartosc, wartosc+1, wartosc+2});
                pole.addMouseListener(this);
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                listaPol.add((Pole) pole);
                add(pole,c);

                c.gridy = 6;
                c.gridx += 1;
                pole = new ElementGraficzny("stol2/naroznik2.png");
                add(pole,c);

                c.gridy = 5;
                c.gridx = x;
                pole = new PoleNumeryczne(wartosc, "stol2/" + String.valueOf(wartosc) + ".png");
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                pole.addMouseListener(this);
                listaPol.add((Pole) pole);
                add(pole,c);

                c.gridx += 1;
                pole = new PoleGraniczne("stol2/pasek_pionowy.png", new int[]{wartosc,wartosc+3});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);
                }
                
                add(pole,c);

                c.gridy = 4;
                c.gridx = x+1;
                pole = new PoleGraniczne("stol2/naroznik_krzyz.png", new int[]{wartosc, wartosc+1, wartosc+3, wartosc+4});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);
                }
                add(pole,c);

                c.gridy = 3;
                c.gridx = x;
                pole = new PoleNumeryczne(wartosc+1, "stol2/" + String.valueOf(wartosc+1) + ".png");
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                pole.addMouseListener(this);
                listaPol.add((Pole) pole);
                add(pole,c);

                c.gridx += 1;
                pole = new PoleGraniczne("stol2/pasek_pionowy.png", new int[]{(wartosc+1),(wartosc+1)+3});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);
                }
                add(pole,c);

                c.gridy = 2;
                c.gridx = x+1;
                pole = new PoleGraniczne("stol2/naroznik_krzyz.png", new int[]{wartosc+1, wartosc+2, wartosc+4, wartosc+5});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);
                }
                add(pole,c);

                c.gridy = 1;
                c.gridx = x;
                pole = new PoleNumeryczne(wartosc+2, "stol2/" + String.valueOf(wartosc+2) + ".png");
                pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                pole.addMouseListener(this);
                listaPol.add((Pole) pole);
                add(pole,c);

                c.gridx += 1;
                pole = new PoleGraniczne("stol2/pasek_pionowy.png", new int[]{(wartosc+2),(wartosc+2)+3});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);                    
                }
                add(pole,c);

                c.gridy = 0;
                c.gridx = x+1;
                pole = new PoleGraniczne("stol2/naroznik1.png", new int[]{wartosc,wartosc+1,wartosc+2,wartosc+3,wartosc+4,wartosc+5});
                if (i != 34) {
                    pole.addMouseListener(this);
                    pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
                    listaPol.add((Pole) pole);
                }
                add(pole,c);

                x+=2;
            }

            //POLA WIERSZE
            c.gridx = x;
            c.gridheight = 3;
            c.gridy = 0;
            c.anchor = FIRST_LINE_START;
            c.insets =  new Insets(0,-11,0,0);
            pole = new PoleSpecjalneWiersz("stol2/2do1_c.png", PoleSpecjalneWiersz.Wiersze.PIERWSZY_WIERSZ);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            c.gridy +=2;
            c.insets =  new Insets(10,-11,0,0);
            pole = new PoleSpecjalneWiersz("stol2/2do1_b.png", PoleSpecjalneWiersz.Wiersze.DRUGI_WIERSZ);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            c.gridy +=2;
            pole = new PoleSpecjalneWiersz("stol2/2do1_a.png", PoleSpecjalneWiersz.Wiersze.TRZECI_WIERSZ);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            //POLA: KOLUMNY
            c.gridx = 1;
            c.gridy = 7;
            c.gridwidth = 9;
            c.insets = new Insets(-11, 10, 0 ,0);
            pole = new PoleSpecjalneTuzin("stol2/1st12.png", PoleSpecjalneTuzin.Tuziny.PIERWSZY_TUZIN);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            c.gridx += c.gridwidth-1;
            pole = new PoleSpecjalneTuzin("stol2/2nd12.png", PoleSpecjalneTuzin.Tuziny.DRUGI_TUZIN);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            c.gridx += c.gridwidth-1;
            pole = new PoleSpecjalneTuzin("stol2/3rd12.png", PoleSpecjalneTuzin.Tuziny.TRZECI_TUZIN);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);

            //POLA: OSTATNI WIERSZ
            c.gridy = 10;
            c.insets = new Insets(0, 10, 0 ,0);
            c.gridx = 1;
            c.gridwidth = 5;
            String[] nazwaPlikow = {"1to18.png", "even.png", "red.png", "black.png", "odd.png", "19to36.png"};

            int numerPliku = 0;
            c.gridx = 1;

            pole = new PoleSpecjalnePolowka("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalnePolowka.Polowki.PIERWSZA_POLOWKA);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);   
            c.gridx += c.gridwidth-1;

            ++numerPliku;
            pole = new PoleSpecjalneParzystosc("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalneParzystosc.Parzystosc.PARZYSTE);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);
            c.gridx += c.gridwidth-1;

            ++numerPliku;
            pole = new PoleSpecjalneKolor("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalneKolor.Kolory.CZERWONY);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c); 
            c.gridx += c.gridwidth-1;

            ++numerPliku;
            pole = new PoleSpecjalneKolor("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalneKolor.Kolory.CZARNY);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);
            c.gridx += c.gridwidth-1;

            ++numerPliku;
            pole = new PoleSpecjalneParzystosc("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalneParzystosc.Parzystosc.NIEPARZYSTE);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);
            c.gridx += c.gridwidth-1;

            ++numerPliku;
            pole = new PoleSpecjalnePolowka("stol2/" + nazwaPlikow[numerPliku], PoleSpecjalnePolowka.Polowki.DRUGA_POLOWKA);
            pole.addMouseMotionListener(ZmianaKursora.dajInstancje());
            pole.addMouseListener(this);
            listaPol.add((Pole) pole);
            add(pole,c);
            c.gridx += c.gridwidth-1;
        }
    }
    
/**
 * Klasa reprezentująca historię, która wyświetla się po prawej stronie okna.
 */
    class PanelZHistoria extends JPanel{
        /** Tlo ramki z historią */
        private Image tloObrazek;
        /** Lista historii losowań. Przechowywane jest 6 ostatnich losowań. */
        private final List<Integer> historiaLosowan;
        
        private Image poleCzarne, poleCzerwone, poleBiale;

        /**
         * Konstruktor domyslny.
         */
        public PanelZHistoria()
        {
            try 
            {
                tloObrazek = ImageIO.read(getClass().getResourceAsStream("/belka_history2.png"));
                poleCzarne = ImageIO.read(getClass().getResourceAsStream("/pole_czarne.png"));
                poleCzerwone = ImageIO.read(getClass().getResourceAsStream("/pole_czerwone.png"));
                poleBiale = ImageIO.read(getClass().getResourceAsStream("/pole_biale.png"));
            } 
            catch (IOException ex) {
                System.out.println("Nie ma takiego pliku");
            }

            setLayout(new BorderLayout(0,0));
            setBorder(new EmptyBorder(10, 0, 0, 0));
            setOpaque(false);
            setPreferredSize(new Dimension(tloObrazek.getWidth(null), tloObrazek.getHeight(null)));
            
            historiaLosowan = new ArrayList<>();
        }
    
        /**
         * Metoda zarządzają panelem okna (rysowanie pól ostatnich losowań)
         * @param g 
         */    
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2D = (Graphics2D) g; 
            
            g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);         
            
            g2D.drawImage(tloObrazek, 0, 21, null);     
            
            g2D.setColor(Color.white);
            int wartosc_Y = 90, liczbaPol = 6, licznik = 1;

            for(Integer pole : historiaLosowan)
            {
                Image poleTlo = null;
                int x = 0, y = wartosc_Y-18;
                int xTekst, yTekst = wartosc_Y - 1;
                
                PoleNumeryczne.KoloryPol typ =  PoleNumeryczne.znajdzKolorPola(pole);
                switch(typ) {
                    case CZARNE:
                        poleTlo = poleCzarne;
                        x = 15;
                        break;
                    case CZERWONE:
                        poleTlo = poleCzerwone;
                        x = tloObrazek.getWidth(null) - poleCzerwone.getWidth(null) - 15;
                        break;
                    case ZADEN:
                        poleTlo = poleBiale;
                        x = 38;
                        break;
                }
                
                if (pole >9) {
                    xTekst = x + 10;
                } else {
                    xTekst = x + 14;
                }
                
                g2D.drawImage(poleTlo,x,y, null);
                g2D.setFont(new Font("Times New Roman", Font.BOLD, 22 ));
                g2D.drawString(String.valueOf(pole),xTekst, yTekst);

                wartosc_Y+=35;

                if(++licznik > liczbaPol) {
                    break;
                }
            } 
        }
        
        /**
         * Metoda odpowiedzialna za dodanie wylosowanego numeru do historii losowań
         * @param pole 
         */
        private void dodajWylosowanePole(Integer pole) {
            historiaLosowan.add(0, pole);
        }
    }
    
}
