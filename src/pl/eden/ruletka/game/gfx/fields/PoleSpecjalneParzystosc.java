package pl.eden.ruletka.game.gfx.fields;

/**
 * Klasa reprezentująca pole specjalne typu parzystość
 * @author Mateusz Macięga
 */
public class PoleSpecjalneParzystosc extends PoleSpecjalne {
    /** Typ wyliczeniowy dla pola specjalnego Even, Odd */
    public static enum Parzystosc { 
        NIEPARZYSTE, PARZYSTE;
    }
    
    /** Które pole specjalne typu Even, Odd */
    private final Parzystosc parzystosc;
    
    /**
     * Konstruktor sparametryzowany
     * @param nazwa Ścieżka do pliku z grafiką pola
     * @param parzystosc Które pole specjalne typu Even, Odd
     */
    public PoleSpecjalneParzystosc(String nazwa, Parzystosc parzystosc) {
        super(nazwa, Pole.MnoznikiPola.EVEN_ODD_BET, PoleSpecjalne.TypyPolaSpecjalnego.PARZYSTOSC);
        
        this.parzystosc = parzystosc;
    }

    /**
     * Metoda sprawdzająca czy zakłady postawione na to pole po wylosowaniu wartości są zwycięskie
     * @param wylosowanePole Wartość wylosowanej liczby przez koło ruletki
     * @return Wartość logiczna (true, false)
     */
    @Override
    public boolean sprawdzZaklad(int wylosowanePole) {
        switch(parzystosc)
        {
            case NIEPARZYSTE:
                return wylosowanePole % 2 != 0;
            case PARZYSTE:
                return wylosowanePole % 2 == 0;
            default:
                return false;
        }
    }
    
    /**
     * Metoda zwracająca typ wyliczeniowy dla pola specjalnego Even, Odd
     * @return Typ wyliczeniowy dla pola specjalnego Even, Odd
     */
    public Parzystosc dajParzystosc() {
        return parzystosc;
    }
}
